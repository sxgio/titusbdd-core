# Contributing to the TITUS BDD Framework Main Core of SXGIO.NET

First of all, thank you for taking the time to contribute! 👍

The following project is a set of guidelines for contributing to TITUSBDD-Core project. These are mostly guidelines, 
not strict rules. Use your best judgment, and feel free to propose changes to this document via pull requests. This 
document was inspired by the awesome Atom contribution guidelines.

## Table of contents

[I don't want to read this whole thing, I just have a question!!!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)

[How Can I Contribute?](#how-can-i-contribute)

* [Your First Code Contribution](#your-first-code-contribution)
* [GitFlow](#gitflow)
* [Commit message convention](#commit-message-convention)

## I don't want to read this whole thing, I just have a question

Note: Please don't file an issue to ask a question. You'll get faster results by using the following jabber channel:

We have an official **General Slack** channel where core maintainers can answer your questions in a more direct fashion. 
We encourage you to join to **General** on channel ##PLEASE-DEFINE-CHANNEL##
## How Can I Contribute

### Your First Code Contribution

Are you still unsure how you may contribute to the RSI ecosystem? You may start by looking through open issues or your 
own feature requests (if any). We are strong supporters of the inner source philosophy and we will help you as much 
as we can to add your code to the repository. If you are unsure about the way to do it, just 
[ask](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question).

#### GitFlow

##### Clone the main project

```{.sh}
git clone git@bitbucket.org:sxgio/titusbdd-core.git
git clone https://bitbucket.org/sxgio/titusbdd-core.git
```

##### Fork the project from Gitlab UI

##### Add a remote in your git

```{.sh}
git remote add personal https://bitbucket.org/your-user/titusbdd-core.git
```

You should have something like

```{.sh}
$ git remote -v
origin  https://bitbucket.org/sxgio/titusbdd-core.git (fetch)
origin  https://bitbucket.org/sxgio/titusbdd-core.git (push)
personal        https://bitbucket.org/your-user/titusbdd-core.git (fetch)
personal        https://bitbucket.org/your-user/titusbdd-core.git (push)

```

##### Create your branch

```{.sh}
git fetch origin
git rebase origin/master
git checkout -b dev/[your-user]/[branch-name]
```

( 2 first commands aims to ensure you are up to date with the master before creating a branch.)

##### Add and commit your changes

```{.sh}
git add .; git commit -m "doc: add whatever you did"
```

other change needs to be added...

```{.sh}
git add .: git commit -m "doc: add whatever other issue you fixed"
```

##### Rebase before to push your change and push a merge request

```{.sh}
git rebase -i $(git merge-base origin) dev/[your-user]/[branch-name]
```

and rewrite `pick` by `squash` for all extra commit so the first one must remain with the word `pick`.

For example:

```{.sh}
pick dp2ed3 doc: add whatever you did
squash 98dawj doc: add section local development
squash 098qdw doc: add commit convention
```

##### In the step of rebase, take the opportunity to write a well documented commit

For instance:

```{.sh}
fix(middleware): ensure Range headers adhere more closely to RFC 2616

Add one new dependency, use `range-parser` (Express dependency) to compute
range. It is more well-tested in the wild.

Fixes #2310
```

##### Then you are ready to push your code

```{.sh}
git fetch origin
git rebase origin/master
git push -f personal dev/[your-user]/[branch-name]
```

(The 2 first commands are there to sync with origin and fix potential conflicts)

##### Have a look in your fork in Gitlab

In my case, [here](http://172.22.9.28:8081/U029783/iron-framework-core.git)

Now, you are ready to create a merge request:
`From [your-user]/titusbdd-core:dev/[your-user]/[branch-name] into sxgio/titusbdd-core:master`

#### Commit message convention

We are following the standard message convention of git. 
Go [here](http://karma-runner.github.io/2.0/dev/git-commit-msg.html) to dig into all the specifications.

## License

&copy; 2022 SXGIO.NET
