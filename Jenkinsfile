pipeline {
    agent { label 'linux' }
    environment {
        VERSION = "0.1.0"
        ARTIFACTID = "titusbdd-core"
        PACKAGING = "jar"
    }
    options{
        buildDiscarder(logRotator(numToKeepStr: "5", daysToKeepStr: "5"))
    }
    parameters {
        booleanParam (
            defaultValue: true,
            description: 'Whether unit tests are mandatory',
            name : 'UNIT_TESTS_ARE_MANDATORY')
        booleanParam (
            defaultValue: false,
            description: 'Whether integration tests are mandatory',
            name : 'INTEGRATION_TESTS_ARE_MANDATORY')
        booleanParam (
            defaultValue: true,
            description: 'Whether quality gate is mandatory',
            name : 'QUALITY_GATE_IS_MANDATORY')
        booleanParam (
            defaultValue: false,
            description: 'Whether deploy documentation is mandatory',
            name : 'DEPLOY_IS_MANDATORY')
        booleanParam (
            defaultValue: true,
            description: 'Whether site documentation is mandatory',
            name : 'SITE_DOCUMENTATION_IS_MANDATORY')
    }
    tools {
        maven 'Default'
        jdk 'JDK 17'
    }
    stages {
        stage('Compile') {
            steps {
                echo 'Compiling...'
                sh "mvn compile"
            }
        }
        stage('Unit Tests') {
            when {
                 // Execute these steps just only if unit tests are mandatory
                 expression { return params.UNIT_TESTS_ARE_MANDATORY }
            }
            steps {
                echo 'Unit Tests...'
                sh "mvn test"
                echo 'Generating jacoco xml files...'
                sh "mvn jacoco:report"
            }
            post {
                success {
                     publishHTML (target: [
                          allowMissing: false,
                          alwaysLinkToLastBuild: false,
                          keepAll: true,
                          reportDir: 'target/site/jacoco',
                          reportFiles: 'index.html',
                          reportName: "Jacoco Coverage Report"
                     ])

                }
            }
        }
        stage('Quality analysis') {
            environment {
                SONAR_SOURCES = "src/main"
                SONAR_TESTS = "src/test"
                SONAR_JAVA_BINARIES = "target/classes"
                SONAR_EXCLUSIONS = "src/main/resources/**/*,src/main/java/net/sxgio/**/Waitable.java,src/main/java/net/sxgio/titusbdd/tests/acceptance/pages/*.java"
                SONAR_DEPENDENCY_CHECK = "target/dependency-check-report.html"
                SONAR_HOST_URL = "http://sxgio.servehttp.com:9000"
            }
            when {
                 // Execute these steps just only if unit tests are mandatory
                 expression { return params.UNIT_TESTS_ARE_MANDATORY }
            }
            steps {
                echo 'SonarQube Updates...'
                withSonarQubeEnv('SXGIO') {
                    sh "mvn verify sonar:sonar -DskipTests=true -Dsonar.sources=$SONAR_SOURCES -Dsonar.tests=$SONAR_TESTS -Dsonar.java.binaries=$SONAR_JAVA_BINARIES -Dsonar.coverage.exclusions=$SONAR_EXCLUSIONS -Dsonar.exclusions=$SONAR_EXCLUSIONS -Dsonar.dependencyCheck.htmlReportPath=$SONAR_DEPENDENCY_CHECK -Dsonar.host.url=$SONAR_HOST_URL"
                }
            }
            post {
                success {
                     publishHTML (target: [
                          allowMissing: false,
                          alwaysLinkToLastBuild: false,
                          keepAll: true,
                          reportDir: 'target',
                          reportFiles: 'dependency-check-report.html',
                          reportName: "Dependency Check Report"
                     ])
                }
            }
        }
        stage('Quality gate verification') {
            when {
                 // Execute these steps just only if unit tests are mandatory
                 expression { return params.UNIT_TESTS_ARE_MANDATORY && params.QUALITY_GATE_IS_MANDATORY }
            }
            steps {
                script {
                    Integer waitSeconds = 10
                    Integer timeOutMinutes = 10
                    Integer maxRetry = (timeOutMinutes * 60) / waitSeconds as Integer
                    for (Integer i = 0; i < maxRetry; i++) {
                        try {
                            timeout(time: waitSeconds, unit: 'SECONDS') {
                                def qg = waitForQualityGate()
                                if (qg.status != 'OK') {
                                    error "Sonar quality gate status: ${qg.status}"
                                } else {
                                    i = maxRetry
                                }
                            }
                        } catch (Throwable e) {
                            if (i == maxRetry - 1) {
                                throw e
                            }
                        }
                    }
                }
            }
        }
        stage('Deploy') {
           when {
                // Execute this step just only if deploy is mandatory
               expression { return params.DEPLOY_IS_MANDATORY }
           }
           steps {
               echo 'Deploying on Nexus...'
               sh "mvn deploy -DskipTests=true"
           }
        }
        stage('Site info') {
            when {
                 // Execute these steps just only if unit tests are mandatory
                 expression { return params.SITE_DOCUMENTATION_IS_MANDATORY }
            }
            steps {
                echo 'Generating Site Documentation...'
                sh "mvn site"
            }
            post {
                success {
                     publishHTML (target: [
                          allowMissing: false,
                          alwaysLinkToLastBuild: false,
                          keepAll: true,
                          reportDir: 'target/site/apidocs',
                          reportFiles: 'index.html',
                          reportName: "TITUS BDD Framework API Info"
                      ])
                }
            }
        }
    }
    post {
        always {
            script {
                MESSAGE_TO_SLACK = "Job: ${env.JOB_NAME}\n Build Report: ${env.BUILD_URL}\n"
            }
        }
        fixed {
            slackSend color: "warning", message: "${MESSAGE_TO_SLACK}Status: *Back to Normal*\n"
        }
        success {
            slackSend color: "good", message: "${MESSAGE_TO_SLACK} Status: *Build success*\n"
        }
        unsuccessful {
            slackSend color: "danger", message: "${MESSAGE_TO_SLACK} Status: *Build failed*\n"
        }
    }
}