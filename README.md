# Titus BDD Main Core

[![Repositorio Git](https://img.shields.io/badge/repository-url-green?style=flat&logo=git)](https://bitbucket.org/sxgio/titusbdd-core.git)
[![Versión de cucumber](https://img.shields.io/badge/cucumber%20version-7.15.0-brightgreen.svg?style=flat&logo=java)](https://cucumber.io/)
[![REST-Assured](https://img.shields.io/badge/Rest%20Assured-5.4.0-brightgreen.svg)](https://github.com/Naereen/badges)
![Selenium](https://img.shields.io/badge/-selenium-%43B02A?style=for-the-badge&logo=selenium&logoColor=white&version=4)
![Google Chrome](https://img.shields.io/badge/Google%20Chrome-4285F4?style=for-the-badge&logo=GoogleChrome&logoColor=white)
![Edge](https://img.shields.io/badge/Edge-0078D7?style=for-the-badge&logo=Microsoft-edge&logoColor=white)
![Firefox](https://img.shields.io/badge/Firefox-FF7139?style=for-the-badge&logo=Firefox-Browser&logoColor=white)
![Safari](https://img.shields.io/badge/Safari-000000?style=for-the-badge&logo=Safari&logoColor=white)
![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)
![iOS](https://img.shields.io/badge/iOS-000000?style=for-the-badge&logo=ios&logoColor=white)


## Introduction

**Titus BDD QA Framework Core** encompasses the main library of a framework for testing any type of
functional test.It provides a clean interface to separate the needs of business people (stakeholders) with the 
code needed to test new functionalities and the app stability. While stakeholders write their code in the **Gherkin** 
language and leave all their files inside the maven test resource folder, all the code needed is left 
in the *main* and *test* folders.

The project was created with maven. It contains 4 different ways to check an application:
 
1. Through scrapping by [JSOUP](https://jsoup.org/) y 
2. Through actions on devices or web with: 
    1. A mobile-driver with [Appium](http://appium.io/) o 
    2. A web-driver with [Selenium](http://www.seleniumhq.org/).
3. Through [Rest-Assured](https://rest-assured.io/) checking web-services

Within Selenium, the following browsers are supported:

1. **Chrome**
2. **Firefox** with Marionette
3. **Internet Explorer**, version 11 only
4. **Edge Chromium**
5. **Safari**
6. **Remote** (with Chrome, Firefox, Internet Explorer, Edge) with a GRID.

Within Appium, the following mobile device groups are supported:

1. **Android**: Android mobiles and tablets
2. **IOS**: Apple mobiles
3. **IPAD**: Apple tablets

It could be extended to any additional browser and/or new device by configuring the relevant configuration classes.

By default, **Remote** is the best option for testing on any environment other than
local. There is a relationship between the number of tests and their execution: the more tests
execute, the longer the system will take to process them. There are mechanisms to significantly reduce the
time of execution, but two ways are recommended:

1. All tests should be isolated for execution without side effects
2. There are 3 ways to run the tests in parallel:
    1. Through TestNG
    2. Using forking in the pom.xml configuration 
    3. Using Jenkins running each browser or device stage in parallel. 
 
By default, the system has the first option configured.

## Consideraciones funcionales

The different environments where the tests can be executed are defined by labels and are configurable in the yaml 
configuration file intended for each browser/test system and in the features files made in Gherkin. These are the labels
established at the beginning (can be customized):

1. **@local**: Entorno local. Solo para tareas de desarrollo. Se ejecuta sobre la máquina en local
2. **@dev**: Entorno de desarrollo. Desarrollo prueba las funcionalidades nuevas.
3. **@qa**: Entorno de certificación. QA certifica que las funcionalidades cumplen las especificaciones.
4. **@pre**: Entorno de pre-producción. Sistemas prueba que el sistema cumple el estándar de calidad y seguridad de la 
empresa
5. **@pro**: Entorno de producción.

Además de estas etiquetas, se pueden utilizar estas otras (o personalizar el sistema con unas propias):

1. **@javascript**: Marca que el test necesita javascript y se ejecutará mediante Selenium
2. **@appium**: Marca que el test se ejecutará para entorno móvil dentro del Grid de Selenium o mediante conexión con un servidor Appium
3. **@test**: Utilizada para realizar test individuales y que no pase el set de test al completo
4. **@api**: Se necesita para arrancar los test de Rest-Assured

Así, por ejemplo, en un proyecto derivado de la librería pueden existir los siguientes dos ficheros:

* Fichero feature para test (el test se etiqueta con @qa):

```{gherkin}
# language: es
@TEST-EXECUTION-AB @javascript @local @dev @qa @pre @pro
Característica: 99 - General:Estabilidad:Comprobar javascript en un página
Para comprobar una página con contenido javascript
Soy Juan, un posible cliente
Tengo que ser capaz de interactuar con la aplicación

@TEST-XY
Escenario: Comprobar javascript en la página principal
Dado estoy en la página principal
Entonces el código de respuesta es 200
Y puedo interactúar con la página web
```

Fichero de configuración del navegador para poder ejecutar en el entorno de QA (el nombre de la configuración identifica
el navegador, chrome, y el entorno a utilizar, QA):

* Extracto de configuración en un fichero yaml

````{yaml}
name: chrome-qa
baseKeyStore: ''
baseKeyStorePassword: ''
baseUrl: 'https://main-page-url-to-test'
driverClass: 'org.openqa.selenium.chrome.ChromeDriver'
driverFile: 'bin/chromedriver'
driverName: 'net.sxgio.titusbdd.tests.acceptance.drivers.ChromeBrowserConfiguration'
driverType: 'webdriver.chrome.driver'
headless: false 
nitriteConfiguration:
    active: true
    nitriteCollection: 'collection-name'
    nitritePath: 'database'
    nitritePassword: 'password'    
    nitriteUser: 'mongo-user'
````

Si realizamos la ejecución mediante el comando maven en el POC con un comando similar al siguiente:

````{bash}
mvn clean test -Dprofile=chrome-qa -Dtest.tag=@qa
````

El sistema leerá la configuración en chrome-qa, preparará las pruebas para el entorno qa y con el navegador chrome y 
ejecutará aquellos tests que lleven la etiqueta @qa. Si, en vez de ejecutar el comando anterior, usamos el 
siguiente: 

````{bash}
mvn clean test -Dprofile=chrome-qa -Dtest.tag=@TEST-XY 
````

el sistema limita la prueba en qa para chrome a única y exclusivamente aquellos test con la etiqueta @TEST-XY, en este 
caso, un único test.

Usando una coma (,), se pueden concatenar más de un test. Así, -Dtest.tag=@TEST-XY,@TEST-ZK ejecutaría los test nombrados como 
XY, ZK. Las posibilidades de etiquetar para agrupar los tests y ejecutarlos quedan abiertas y a elección del tester que 
realiza la prueba teniendo claro cómo pasar el listado de lo que se quiere probar.

### Database

Existe una base de datos OrientDb embebida y se utiliza para mantener los datos de prueba aislados de los ficheros
de definición de test. Dentro de los ficheros de definición de test no existirá información sensible.

Actualmente, solo existen consultas a la base de datos. La clase abstracta **OrientDbDtoAbstractImpl** lleva
implementados los siguientes métodos:

1. **Collection<EntityInterface> getContent(String text)**: Devuelve todos aquellos vértices de la entidad correspondiente
   que contiene un texto definido en cualesquiera de sus campos.
2. **EntityInterface getItem(String indexValue)**: Devuelve el primer vértice con el valor indexValue en cualesquiera de
   sus índices de búsqueda
3. **Collection<EntityInterface> getItemCollection(String indexValue)**: Devuelve todos aquellos vértices con el valor
   indexValue en cualquier índice de búsqueda
4. **getRawContent()**: Transforma el contenido buscado en un JSON de vuelta.

Partiendo de esta clase se generan las clases DTO que alimentan a cada una de las entidades. Todas ellas se pueden
encontrar en el siguiente paquete:  **net.sxgio.titusbdd.tests.acceptance.entities**. Se utiliza
[Gremlin Tinkerpop](https://tinkerpop.apache.org/gremlin.html) para el acceso a los datos.

Un ejemplo de cómo obtener un usuario concreto podrían ser las siguientes líneas de código:

```{.java}
 // RunMainTest.getFactory() contains the DI
 UserDto userDto = RunMainTest.getFactory().getInstance(UserDto.class);
 try {
     user = (User)userDto.getItem(identifier);
 } catch (Exception e) {
     logger.warning("Empty user from here. Exception caught "+ e.getClass().getName());
     ....
 } 
```

#### Ejecutar la base de datos dentro del proyecto

Son los proyectos que utilicen este los que deberían proporcionar la base de datos y ajustar la configuración
a la necesaria para utilizar los objetos que se proporcionan aquí.

## Configuraciones adicionales de navegadores

Si se necesita, en un proyecto, personalizar la estructura de los navegadores y cargar configuraciones en las opciones 
y/o preferencias del webdriver se puede realizar añadiendo la entrada browserCapabilities en el fichero de configuración 
yaml asociado. Un ejemplo sería el siguiente:

````{json}
browserCapabilities: "{\"property\":\"value\",\"property2\":\"value2\"}"
````

La entrada browserCapabilities define, en formato JSON, el par-valor de valores que se insertarán bien como 
preferencias, bien como opciones al webdriver para formar el navegador correspondiente con una configuración básica 
por defecto.

El JSON formado se decodifica y se convierte en un objeto que se inyectará al final de la configuración. Esto es, 
cualquier duplicación de la entrada prevalecerá el cambio que llegue mediante el fichero de configuración, ya que se 
definen al final del proceso.

Si se necesita, en un proyecto, que la ejecución en el navegador sea sin el navegador activo (headless) se puede 
realizar mediante la entrada headless en el fichero de configuración yaml asociado:

````{json}
headless: true
````

## Construcción y tests

Al ser una librería, este proyecto no se puede ejecutar. 

* Para instalarlo, en caso de no disponer de maven, se puede incluir en el directorio oculto _m2_ mediante el siguiente comando:

````{bash}
mvn clean install -P unit
````

* Para realizar los test unitarios con cobertura (Jacoco) utilizar:

````{bash}
mvn clean test -P unit
````

## API

Para acceder a la documentación del API ejecutar el siguiente comando:

````{bash}
mvn javadoc:javadoc
````

Si todo fue bien, se puede acceder a los resultados en el siguiente directorio:
* **target/site/apidocs/**


## Análisis SonarQube

En caso de necesitar un análisis de SonarQube se debería usar un comando como el siguiente:

````{bash}
mvn clean verify sonar:sonar -P unit 
-Dsonar.sources=src/main 
-Dsonar.tests=src/test 
-Dsonar.java.binaries=target/classes 
-Dsonar.exclusions=src/main/resources/**/* 
-Dsonar.dependencyCheck.htmlReportPath=target/dependency-check-report.html 
-Dsonar.host.url=http://[sonar-server]:9000 
-Dsonar.jacoco.reportPaths=target/jacoco.exec
````

## Contribuidores

* **Sergio Martin <mailto:info@sxgio.net>**
* **Daniel Salgado <mailto:daniel.salgado.p@gmail.com>**

## Para contribuir (en Inglés)

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)
](https://bitbucket.org/sxgio/titusbdd-core/src/develop/CONTRIBUTING.md)

## Licencia

&copy; 2023 SXGIO.NET
