# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Released]

## [0.1.3]

## Added - 2024-02-04

- feat:TIT-19:Make unit tests of pages

## [0.1.2]

## Added - 2023-07-06

- feat:TIT-8: Review class located at pages package and create the unit tests

## [0.1.1]

## Added - 2023-02-11

- fix:TIT-15:Fix version JDK 8 in jenkinsfile as the code is JDK 11

## [0.1.0]

## Changed - 2023-02-07

- fix:TIT-7:Change README.md to show last changes in the framework and how to use it. Correct minors errors and rewrite RestDriverTest

## [Unreleased]

## Added - 2023-02-10 

- feat:TIT-6:Make unit tests of all clases located at net.sxgio.titusbdd.tests.acceptance.entities

## Added - 2023-02-03

- fix:TIT-4:Refactoring version 0.1.0 to integrate the project into Jenkins

## Added - 2022-04-24

- feat:TIT-2:Scaffolding and 0.1.0 version available with mvn install

## Added - 2022-04-23

- Initial commit