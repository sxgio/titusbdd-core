package net.sxgio.titusbdd.tests.acceptance.entities;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * <strong>GlobalConfigurationEntityTest.java</strong><br>
 * <strong>GlobalConfigurationEntityTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release date: 3/2/24
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since File available since 3/2/24
 */
@Test(groups = {"units"})
public class GlobalConfigurationEntityTest extends AbstractConfigurationEntityTest {

    /**
     * Provider for getter and setters methods
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] getSettersAndGettersProvider() {
        // Params defined
        return new Object[][] {
                // Right params
                new Object[] {"setWebConfiguration", "getWebConfiguration", new WebDataEntity()},
                new Object[] {"setRestConfiguration", "getRestConfiguration", new RestDataEntity()},
        };
    }

    /**
     * Setup method.
     */
    @Override
    @BeforeMethod()
    public void setUp() {
        super.setUp();
        instance = new GlobalConfigurationEntity();
    }

    /**
     * Tester for getter and setters.
     *
     * @param setMethodName method to test as setter
     * @param getMethodName method to test as getter
     * @param methodParameter data to set and afterward to get
     */
    @Override
    @Test(dataProvider = "getSettersAndGettersProvider")
    public void testGettersAndSetters(String setMethodName, String getMethodName, Object methodParameter) {
        super.testGettersAndSetters(setMethodName,getMethodName, methodParameter);
    }
}
