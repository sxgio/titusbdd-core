package net.sxgio.titusbdd.tests.acceptance.test;

/**
 * <strong>GlobalContent.java</strong><br>
 * <strong>GlobalContent class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class GlobalContent
{
    /**
     * Activating text for cleaning up the test
     */
    public static final String ACTIVATING_TEST_TEXT = " Activating Test for %s";

    /**
     * Status text for cleaning up the test
     */
    public static final String CLEANING_STATUS_TEXT = "Cleaning up test for %s";

    public static final String DUMMY_STRING = "Dummy String Value";

}
