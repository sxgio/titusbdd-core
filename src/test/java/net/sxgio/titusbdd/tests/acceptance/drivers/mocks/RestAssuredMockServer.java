package net.sxgio.titusbdd.tests.acceptance.drivers.mocks;

import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.Parameter.param;
import static org.mockserver.model.ParameterBody.params;
import static org.mockserver.model.StringBody.exact;

import java.util.concurrent.TimeUnit;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpStatus;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.Header;

/**
 * <strong>RestAssuredMockServer.java</strong><br>
 * <strong>RestAssuredMockServer class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class RestAssuredMockServer {

  @Getter @Setter protected MockServerClient mockServer;

  /**
   * Constructor by default.
   *
   * @param host Host name
   * @param port Port number
   */
  public RestAssuredMockServer(String host, int port) {
    mockServer = new MockServerClient(host, port);
  }

  /** Mocking delete request. */
  public void createExpectationForDeleteRequest() {
    mockServer
        .when(request().withMethod("DELETE").withPath("/delete-resource"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'foo': 'bar', 'useless': 'sure', 'status': 'deleted' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }

  /** Mocking get request. */
  public void createExpectationsForGetRequest() {
    mockServer
        .when(request().withMethod("GET").withPath(" /not-available-for-sure"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_NOT_FOUND)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'error': '404', 'status': 'selected' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("GET")
                .withPath("/get-resource")
                .withHeader("Content-type", "application/json"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(
                    new Header("Content-Type", "application/json;charset=utf-8"),
                    new Header("Cache-Control", "public,max-age=86400"))
                .withBody("{ 'foo': 'bar', 'useless': 'sure', 'status': 'selected' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("GET")
                .withPath("/get-resource")
                .withQueryStringParameter("foo", "bar")
                .withQueryStringParameter("useless", "sure"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'foo': 'bar', 'useless': 'sure', 'status': 'selected' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(request().withMethod("GET").withPath("/"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(
                    new Header("Content-Type", "application/json;charset=utf-8"),
                    new Header("Cache-Control", "public,max-age=86400"))
                .withBody("{ 'foo': 'bar', 'useless': 'sure', 'status': 'selected' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }

  /** Mocking patch request. */
  public void createExpectationForOptionsRequest() {
    mockServer
        .when(request().withMethod("OPTIONS").withPath("/"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ username: 'foo', password: 'bar', status: 'patched' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }

  /** Mocking patch request. */
  public void createExpectationForPatchRequest() {
    mockServer
        .when(
            request()
                .withMethod("PATCH")
                .withPath("/patch-resource")
                .withHeader("Content-type", "application/json"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'patched' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("PATCH")
                .withPath("/patch-resource")
                .withBody(exact("{'username': 'foo', 'password': 'bar'}")),
            exactly(1))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'patched' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("PATCH")
                .withPath("/patch-resource")
                .withHeader("Content-type", "application/json")
                .withBody(exact("{'username': 'foo', 'password': 'bar'}")),
            exactly(1))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'patched' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(request().withMethod("PATCH").withPath("/patch-resource"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ username: 'foo', password: 'bar', status: 'patched' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }

  /** Mocking post request. */
  public void createExpectationForPostRequest() {
    mockServer
        .when(
            request()
                .withMethod("POST")
                .withPath("/post-resource")
                .withHeader("Content-type", "application/json"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'created' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("POST")
                .withPath("/post-resource")
                .withBody(params(param("username", "foo"), param("password", "bar"))))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'created' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("POST")
                .withPath("/post-resource")
                .withHeader("Content-type", "application/json")
                .withBody(params(param("username", "foo"), param("password", "bar"))))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'created' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("POST")
                .withPath("/post-resource")
                .withHeader("Content-type", "application/json")
                .withBody(exact("{'username': 'foo', 'password': 'bar'}")),
            exactly(1))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'created' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(request().withMethod("POST").withPath("/post-resource"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ username: 'foo', password: 'bar', status: 'created' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }

  /** Mocking put request. */
  public void createExpectationForPutRequest() {
    mockServer
        .when(
            request()
                .withMethod("PUT")
                .withPath("/put-resource")
                .withHeader("Content-type", "application/json"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'modified' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("PUT")
                .withPath("/put-resource")
                .withBody(exact("{'username': 'foo', 'password': 'bar'}")),
            exactly(1))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'modified' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(
            request()
                .withMethod("PUT")
                .withPath("/put-resource")
                .withHeader("Content-type", "application/json")
                .withBody(exact("{'username': 'foo', 'password': 'bar'}")),
            exactly(1))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ 'username': 'foo', 'password': 'bar', 'status': 'modified' }")
                .withDelay(TimeUnit.SECONDS, 1));
    mockServer
        .when(request().withMethod("PUT").withPath("/put-resource"))
        .respond(
            response()
                .withStatusCode(HttpStatus.SC_ACCEPTED)
                .withHeaders(
                    new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Cache-Control", "public, max-age=86400"))
                .withBody("{ username: 'foo', password: 'bar', status: 'modified' }")
                .withDelay(TimeUnit.SECONDS, 1));
  }
}
