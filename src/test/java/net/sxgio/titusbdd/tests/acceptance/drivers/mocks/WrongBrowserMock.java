package net.sxgio.titusbdd.tests.acceptance.drivers.mocks;

import net.sxgio.titusbdd.tests.acceptance.drivers.BrowserInterface;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.mockito.Mockito;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

/**
 * <strong>WrongBrowserMock.java</strong><br>
 * <strong>WrongBrowserMock class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class WrongBrowserMock implements BrowserInterface {
  /** Private constructor to raise NoSuchMethodException */
  private WrongBrowserMock() {
    // do nothing.
  }

  @Override
  public Capabilities getCapabilities() {
    return null;
  }

  @Override
  public WebDriver getDriver() throws DriverException {
    return Mockito.mock(WebDriver.class);
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public void setGlobalProperties(GlobalConfigurationEntity globalProperties) {
    // Do nothing
  }
}
