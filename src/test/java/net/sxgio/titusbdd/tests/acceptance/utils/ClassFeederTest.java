package net.sxgio.titusbdd.tests.acceptance.utils;

import net.sxgio.titusbdd.tests.acceptance.di.CustomPicoFactory;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * <strong>ClassFeederTest.java</strong><br>
 * <strong>ClassFeederTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class ClassFeederTest
{
    /**
     * Instance to test.
     */
    private ClassFeeder instance;

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(ClassFeederTest.class.getName());

    /**
     * Setup method
     */
    @BeforeTest
    public void setUp() {
        instance = new ClassFeeder();
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    }

    /**
     * Teardown method
     */
    @AfterTest
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
        // Shutting down instance
        instance = null;
    }

    /**
     * Tester for ClassFeeder setter and getter methods
     */
    @Test()
    public void testSetAndGetCustomPicoFactory() {
        instance.setFactory(null);
        assertNull(instance.getFactory());
        CustomPicoFactory factory = new CustomPicoFactory();
        instance.setFactory(factory);
        assertEquals(instance.getFactory(), factory);
    }

    /**
     * Provider for all methods of ClassFeeder
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] classContentProvider() {
        return new Object[][] {
            // Right arguments
            new Object[] {
                new HashSet<Class<?>>()
                {
                    {
                        add(GlobalConfigurationEntity.class);
                        add(SharedContent.class);
                    }
                }
            }
        };
    }

    /**
     * Tester for addClasses method
     *
     * @param classes Set of classes to add
     */
    @Test(dataProvider = "classContentProvider")
    public void testAddClasses(Set<Class<?>> classes) {
        instance.addClasses(classes);
        assertFalse(instance.getFactory().getClasses().isEmpty());
        classes.forEach(x -> {
            assertNotNull(instance.getFactory().getInstance(x));
        });
        // Factory already with those classes. Check everything is
        // working as expected
        instance.addClasses(classes);
        classes.forEach(x -> {
            assertNotNull(instance.getFactory().getInstance(x));
        });
    }

    /**
     * Tester for initFactory method
     *
     * @param classes Set of classes to add
     */
    @Test(dataProvider = "classContentProvider")
    public void testInitFactory(Set<Class<?>> classes) {
        instance.initFactory(classes);
        logger.info(instance.getFactory().getClasses().toString());
        assertFalse(instance.getFactory().getClasses().isEmpty());
        classes.forEach(x -> {
            assertNotNull(instance.getFactory().getInstance(x));
        });
    }

    /**
     * Tester for popClasses method
     *
     * @param classes Set of classes to add
     */
    @Test(dataProvider = "classContentProvider")
    public void testPopClasses(Set<Class<?>> classes) {
        instance.initFactory(classes);
        assertFalse(instance.getFactory().getClasses().isEmpty());
        instance.popClasses(classes);
        assertTrue(instance.getFactory().getClasses().isEmpty());
    }
}
