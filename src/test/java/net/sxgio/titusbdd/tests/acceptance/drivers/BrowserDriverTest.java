package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * <strong>BrowserDriverTest.java</strong><br>
 * <strong>BrowserDriverTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class BrowserDriverTest {
  /** Instance to test. */
  private BrowserDriver instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(BrowserDriverTest.class.getName());

  /** Webdriver Mock. */
  @Mock WebDriver driverMock;

  /** QaProperties Mock. */
  @Mock
  GlobalConfigurationEntity globalProperties;

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    instance = new BrowserDriver();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    // Initialize mocks
    WebDataEntity webData = new WebDataEntity();
    globalProperties = new GlobalConfigurationEntity();
    globalProperties.setWebData(webData);
    driverMock = Mockito.mock(WebDriver.class);
  }

  /**
   * Teardown method
   *
   * @throws Exception autocloseable exception
   */
  @AfterMethod()
  public void tearDown() throws Exception {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
    instance = null;
    globalProperties = null;
    driverMock = null;
  }

  /** Tester for getCurrentDriver method */
  @Test()
  public void testGetCurrentDriver() {
    try {
      // Setting up properties
      globalProperties.getWebData().setDriverFile("");
      globalProperties.getWebData().setDriverType("webdriver.chrome.driver");
      globalProperties.getWebData().setDriverName("net.sxgio.titusbdd.tests.acceptance.drivers.mocks.BrowserMock");
      // Injecting the mock
      instance.setGlobalProperties(globalProperties);
      // Nullify the seleniumDriver
      instance.setSeleniumDriver(null);
      // Getting currentDriver
      WebDriver driver = instance.getCurrentDriver();
      assertNotNull(driver);
      assertSame(driver, instance.getSeleniumDriver());
    } catch (Exception e) {
      logger.log(Level.SEVERE, e.getMessage());
      fail();
    }
  }

  /** Tester for close method */
  @Test(dependsOnMethods = "testGetCurrentDriver")
  public void testClose() {
    // Exception raised the sencond time
    for (int i = 0; i < 2; i++) {
      instance.close();
      assertNull(instance.getSeleniumDriver());
    }
  }

  /** Tester for getCurrentDriver method raising an exception */
  @Test()
  public void testGetCurrentDriverWitException() {
    try {
      // Setting up properties
      globalProperties.getWebData().setDriverFile("");
      globalProperties.getWebData().setDriverType("webdriver.chrome.driver");
      globalProperties.getWebData().setDriverName(
          "net.sxgio.titusbdd.tests.acceptance.drivers.mocks.WrongBrowserMock");
      // Injecting the mock
      instance.setGlobalProperties(globalProperties);
      // Nullify the seleniumDriver
      instance.setSeleniumDriver(null);
      // Getting currentDriver
      assertNull(instance.getCurrentDriver());
    } catch (Exception e) {
      logger.log(Level.SEVERE, e.getMessage());
      fail();
    }
  }

  /** Tester for getter and setter of seleniumDriver property */
  @Test()
  public void testGetAndSetSeleniumDriver() {
    instance.setSeleniumDriver(driverMock);
    assertNotNull(instance.getSeleniumDriver());
  }

  /** Tester for getter and setter of seleniumDriver property */
  @Test()
  public void testGetAndSetAppiumDriver() {
    instance.setAppiumDriver(Mockito.mock(AppiumDriver.class));
    assertNotNull(instance.getAppiumDriver());
  }
}
