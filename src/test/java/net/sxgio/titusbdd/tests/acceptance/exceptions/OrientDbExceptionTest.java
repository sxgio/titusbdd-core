package net.sxgio.titusbdd.tests.acceptance.exceptions;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

/**
 * <strong>OrientDbExceptionTest.java</strong><br>
 * <strong>OrientDbExceptionTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 07/02/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class OrientDbExceptionTest {
    /** Instance to test. */
    private OrientDbException instance;

    /** Common logger to know what is happening as the tests are being executed. */
    public static final Logger logger = Logger.getLogger(OrientDbExceptionTest.class.getName());

    /** Setup method. */
    @BeforeMethod()
    public void setUp() {
        logger.info(
            String.format(GlobalContent.ACTIVATING_TEST_TEXT, OrientDbException.class.getSimpleName()));
    }

    /** Teardown method. */
    @AfterMethod()
    public void tearDown() {
        logger.info(
            String.format(GlobalContent.CLEANING_STATUS_TEXT, OrientDbException.class.getSimpleName()));
        instance = null;
    }

    @Test()
    public void testGettersAndSetters() {
        final String message = "test";
        instance = new OrientDbException(message);
        assertEquals(instance.getMessage(), message);
        instance = null;
        instance = new OrientDbException(message, new Exception(message + message));
        assertEquals(instance.getMessage(), message);
    }
}
