package net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks;

import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Clickable;

/**
 * <strong>ClickableImpl.java</strong><br>
 * <strong>ClickableImpl class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class ClickableImpl implements Clickable {}
