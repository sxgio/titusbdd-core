package net.sxgio.titusbdd.tests.acceptance.db;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

/**
 * <strong>OrientDbServerStarterTest.java</strong><br>
 * <strong>OrientDbServerStarterTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class OrientDbServerStarterTest
{
    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(OrientDbServerStarterTest.class.getName());

    /**
     * Tester for main method
     */
    @Test()
    public void mainTest() {
        System.setProperty("orientdb.baseUrl", "/config/db.config");
        System.setProperty("orientdb.embedded.activation", "false");
        System.setProperty("orientdb.embedded.database", "remote:127.0.0.1/pro");
        System.setProperty("orientdb.embedded.password", "admin");
        System.setProperty("orientdb.embedded.user", "admin");
        System.setProperty("orientdb.resources", "src/test/resources");
        String[] args = null;
        OrientDbServerStarter.main(args);
        assertNotNull(OrientDbServerStarter.getOrientdb());
        OrientDbServerStarter.getOrientdb().getServer().shutdown();
        assertFalse(OrientDbServerStarter.getOrientdb().getServer().isActive());
    }

    /**
     * Setup method
     */
    @BeforeTest
    public void setUp() {
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, OrientDbServerStarter.class.getSimpleName()));
    }

    /**
     * Teardown method
     */
    @AfterTest
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, OrientDbServerStarter.class.getSimpleName()));
    }

    /**
     * Tester for main method with wrong parameters
     */
    @Test(dependsOnMethods = {"mainTest"})
    public void wrongMainTest() {
        String[] args = null;
        System.setProperty("orientdb.resources", "#########---######");
        System.setProperty("orientdb.baseUrl", "#########---######");
        OrientDbServerStarter.main(args);
        assertFalse(OrientDbServerStarter.getOrientdb().getServer().isActive());
    }
}