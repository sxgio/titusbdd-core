package net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks;

import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Accessible;

/**
 * <strong>AccessibleImpl.java</strong><br>
 * <strong>AccessibleImpl class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class AccessibleImpl implements Accessible {}
