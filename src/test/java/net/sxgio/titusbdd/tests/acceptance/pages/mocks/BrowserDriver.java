package net.sxgio.titusbdd.tests.acceptance.pages.mocks;

import lombok.Getter;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * <strong>BrowserDriver.java</strong><br>
 * <strong>BrowserDriver class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class BrowserDriver extends net.sxgio.titusbdd.tests.acceptance.drivers.BrowserDriver {

    @Getter @Mock WebDriver webDriver;

    /** WebElement Mock. */
    @Mock WebElement webElement;

    @Override
    public synchronized WebDriver getCurrentDriver() throws DriverException {
        MockitoAnnotations.openMocks(this);
        webElement = Mockito.mock(WebElement.class);
        when(webElement.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
        when(webElement.findElements(any())).thenReturn(Mockito.mock(List.class));
        doNothing().when(webElement).clear();
        when(webDriver.findElement(any())).thenReturn(webElement);
        return webDriver;
    }
}
