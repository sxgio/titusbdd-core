package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.restassured.RestAssured;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import net.sxgio.titusbdd.tests.acceptance.exceptions.RestException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.apache.http.HttpStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.*;

import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.EXCEPTION_CAUGHT_AS_EXPECTED_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.parseContent;
import static org.testng.Assert.*;

/**
 * <strong>RestDriverTest.java</strong><br>
 * <strong>RestDriverTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RestDriverIntegrationTest {

  /** Using with Mockito openMock */
  AutoCloseable closeable;

  /** general url **/
  public static final String URL_GENERAL = "https://httpbin.org";

  /** general url port **/
  public static final String URL_PORT = "443";

  /**
   * Rest-assured Headers mock.
   */
  @Mock
  Cookies cookies;

  /**
   * Instance to test.
   */
  private RestDriver instance;

  /**
   * Common logger to know what is happening as the tests are being executed.
   */
  public static final Logger logger = Logger.getLogger(RestDriverIntegrationTest.class.getName());

  /**
   * Setup method.
   *
   * @param urlGeneral url needed to connect to the rest interface
   * @param urlPort port needed to connect to the rest interface
   */
  @BeforeTest()
  @Parameters({"baseURI", "basePORT"})
  public void setUp(@Optional(URL_GENERAL) String urlGeneral, @Optional(URL_PORT)int urlPort) {
    instance = new RestDriver(urlGeneral, urlPort);
    // Initialize mocking system
    closeable = MockitoAnnotations.openMocks(this);
    logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, RestDriverIntegrationTest.class.getSimpleName()));
  }

  /** Teardown method.
   *
   * @throws Exception just in case something happened with AutoCloseable
   *
   */
  @AfterTest()
  public void tearDown() throws Exception {
    logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, RestDriverIntegrationTest.class.getSimpleName()));
    instance = null;
    RestAssured.baseURI = RestAssured.DEFAULT_URI;
    RestAssured.port = RestAssured.DEFAULT_PORT;
    closeable.close();
  }

  /**
   * Provider for connect method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] connectDataProvider() {
    return new Object[][]{
        // Right resources
        new Object[]{"/"},
    };
  }

  /**
   * Tester for connect method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(expectedExceptions = {RestException.class}, dataProvider = "connectDataProvider")
  public void connectTest(String resource) throws RestException {
    instance.connect(resource);
  }

  /**
   * Provider for delete method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] deleteDataProvider() {
    return new Object[][]{
        // Right resources
        new Object[]{"/delete", URL_GENERAL+"/delete", RestDriver.STATUS_CODE_OK},
    };
  }

  /**
   * Tester for delete method.
   *
   * @param resource url resource to test
   * @param textToFind text to find within the rest service response
   * @param statusToCheck expected response status
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "deleteDataProvider")
  public void deleteTest(String resource, String textToFind, int statusToCheck) throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.delete(resource);
    logger.info(response.getBody().asString());
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Provider for get method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] getDataProvider() {
    return new Object[][]{
        // Right resources
        new Object[]{"/get", URL_GENERAL+"/get", RestDriver.STATUS_CODE_OK},
    };
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "getDataProvider")
  public void getWithResourceTest(String resource, String textToFind, int statusToCheck) throws RestException {
    instance.setGetStatusCode(statusToCheck);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(resource);
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "getDataProvider")
  public void getWithResourceAndHeadersTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setGetStatusCode(statusToCheck);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(
        resource,
        Headers.headers(
            new Header("accept", "application/json")
        )
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "getDataProvider")
  public void getWithResourceAndHeadersAndParamsTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setGetStatusCode(statusToCheck);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(
        resource,
        Headers.headers(
            new Header("accept", "application/json")
        ),
        Map.of("key1", "value")
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "getDataProvider")
  public void getWithResourceAndParamsTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setGetStatusCode(statusToCheck);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(resource, Map.of("key1", "value"));
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Provider for connect method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] wrongResourceOnlyTestProvider() {
    return new Object[][]{
        // Wrong arguments
        new Object[]{null},
        new Object[]{"/not-available-for-sure"}
    };
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   *
   */
  @Test(dataProvider = "wrongResourceOnlyTestProvider")
  public void getWrongTest(String resource) {

    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = null;
    if (Objects.isNull(resource)) {
      instance.setGetStatusCode(RestDriver.STATUS_CODE_OK);
      response = instance.get(resource);
      // Exception expected here and the following line should never be read
      assertNull(response);
    } else {
      instance.setGetStatusCode(HttpStatus.SC_NOT_FOUND);
      response = instance.get(resource);
      assertEquals(response.getStatusCode(), instance.getStatusCode);
    }
  }

  /**
   * Tester for head method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(expectedExceptions = RestException.class, dataProvider = "connectDataProvider")
  public void headTest(String resource) throws RestException {
    instance.head(resource);
  }

  /**
   * Tester for options method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider = "connectDataProvider")
  public void optionsTest(String resource) throws RestException {
    instance.options(resource);
    assertEquals(instance.optionsStatusCode, HttpStatus.SC_OK);
    instance.options();
    assertEquals(instance.optionsStatusCode, HttpStatus.SC_OK);
  }

  /**
   * Provider for post method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] postDataProvider() {
    return new Object[][]{
        // Right resources
        new Object[]{"/post", URL_GENERAL+"/post", RestDriver.STATUS_CODE_OK},
    };
  }

  /**
   * Tester for post method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="postDataProvider")
  public void postWithResourceTest(String resource, String textToFind, int statusToCheck ) throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.post(resource);
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for post method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="postDataProvider")
  public void postWithResourceAndHeadersTest(String resource, String textToFind, int statusToCheck )
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.post(
        resource,
        Headers.headers(new Header("accept", "application/json"))
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for post method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="postDataProvider")
  public void postWithResourceAndHeadersAndParamsTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.post(
        resource,
        Headers.headers(new Header("accept", "application/json")),
        Map.of("key1", "value")
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for post method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="postDataProvider")
  public void postWithResourceAndHeadersAndPayloadTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.post(
        resource,
        Headers.headers(new Header("accept", "application/json")),
        ""
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for post method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="postDataProvider")
  public void postWithResourceAndParamsTest(String resource, String textToFind, int statusToCheck )
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.post(
        resource,
        Map.of("key1", "value")
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Provider for put method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] putDataProvider() {
    return new Object[][]{
        // Right resources
        new Object[]{"/put", URL_GENERAL+"/put", RestDriver.STATUS_CODE_OK},
    };
  }

  /**
   * Tester for put method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="putDataProvider")
  public void putWithResourceTest(String resource, String textToFind, int statusToCheck) throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.put(resource);
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for put method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="putDataProvider")
  public void putWithResourceAndHeadersTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.put(
        resource,
        Headers.headers(new Header("accept", "application/json"))
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for put method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="putDataProvider")
  public void putWithResourceAndHeadersAndPayloadTest(String resource, String textToFind, int statusToCheck)
      throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.put(
        resource,
        Headers.headers(new Header("accept", "application/json")),
        ""
    );
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for put method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param textToFind Text to find inside the response code
   * @param statusToCheck Status to verify after the request
   *
   * @throws RestException An error ocurred while connecting
   */
  @Test(dataProvider="putDataProvider")
  public void putWithResourceAndPayloadTest(String resource, String textToFind, int statusToCheck)
    throws RestException {
    instance.setPostStatusCode(statusToCheck);
    Response response = instance.put(resource,"");
    assertNotNull(response);
    assertEquals(response.getStatusCode(), statusToCheck);
    assertTrue(response.getBody().asString().contains(textToFind));
  }

  /**
   * Tester for trace method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(expectedExceptions = RestException.class, dataProvider = "connectDataProvider")
  public void traceTest(String resource) throws RestException {
    instance.trace(resource);
  }
}
