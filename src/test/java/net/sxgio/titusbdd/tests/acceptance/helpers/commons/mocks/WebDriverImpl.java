package net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.Logs;

/**
 * <strong>WebDriverImpl.java</strong><br>
 * <strong>WebDriverImpl class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class WebDriverImpl implements WebDriver, TakesScreenshot, JavascriptExecutor {

  /** options to be overriden by the mock. */
  private Options options;

  /** window to be overriden by the mock. */
  private Window window;

  /** dimenson to be overriden by the mock. */
  private Dimension dimension;

  @Override
  public void get(String url) {}

  @Override
  public String getCurrentUrl() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public List<WebElement> findElements(By by) {
    return null;
  }

  @Override
  public WebElement findElement(By by) {
    return null;
  }

  @Override
  public String getPageSource() {
    return null;
  }

  @Override
  public void close() {}

  @Override
  public void quit() {}

  @Override
  public Set<String> getWindowHandles() {
    return null;
  }

  @Override
  public String getWindowHandle() {
    return null;
  }

  @Override
  public TargetLocator switchTo() {
    return null;
  }

  @Override
  public Navigation navigate() {
    return null;
  }

  @Override
  public Options manage() {
    options =
        new Options() {
          @Override
          public void addCookie(Cookie cookie) {}

          @Override
          public void deleteCookieNamed(String name) {}

          @Override
          public void deleteCookie(Cookie cookie) {}

          @Override
          public void deleteAllCookies() {}

          @Override
          public Set<Cookie> getCookies() {
            return null;
          }

          @Override
          public Cookie getCookieNamed(String name) {
            return null;
          }

          @Override
          public Timeouts timeouts() {
            return null;
          }

          @Override
          public Window window() {
            window =
                new Window() {
                  @Override
                  public Dimension getSize() {
                    return new Dimension(500, 500);
                  }

                  @Override
                  public void setSize(Dimension targetSize) {}

                  @Override
                  public Point getPosition() {
                    return null;
                  }

                  @Override
                  public void setPosition(Point targetPosition) {}

                  @Override
                  public void maximize() {}

                  @Override
                  public void minimize() {}

                  @Override
                  public void fullscreen() {}
                };
            return window;
          }

          @Override
          public Logs logs() {
            return null;
          }
        };
    return options;
  }

  @Override
  public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
    byte[] bytes = null;
    try {
      final String imagePath = "src/test/resources/images/cucumber.jpg";
      File f = new File(imagePath);
      InputStream in = new FileInputStream(f);
      byte[] buff = new byte[8000];
      int bytesRead = 0;
      ByteArrayOutputStream bao = new ByteArrayOutputStream();
      while ((bytesRead = in.read(buff)) != -1) {
        bao.write(buff, 0, bytesRead);
      }
      in.close();
      bytes = bao.toByteArray();
      return (X) bytes;
    } catch (Exception e) {
      // ImageIO.read will return null with this hex-format image.
      bytes = new byte[] {0x00, 0x01};
    }
    return (X) bytes;
  }

  @Override
  public Object executeScript(String script, Object... args) {
    return new Object();
  }

  @Override
  public Object executeAsyncScript(String script, Object... args) {
    return null;
  }
}
