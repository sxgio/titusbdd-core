package net.sxgio.titusbdd.tests.acceptance.entities;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * <strong>NitriteConfigurationBeanTest.java</strong><br>
 * <strong>NitriteConfigurationBeanTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 07/02/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class MongoConfigurationEntityTest extends AbstractConfigurationEntityTest {

    /**
     * Provider for getter and setters methods.
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] getSettersAndGettersProvider() {
        // Params defined
        return new Object[][]{
            // Right params
            new Object[]{"setActive", "isActive", true},
            new Object[]{"setMongoDatabase", "getMongoDatabase", GlobalContent.DUMMY_STRING},
            new Object[]{"setMongoPassword", "getMongoPassword", GlobalContent.DUMMY_STRING},
            new Object[]{"setMongoPort", "getMongoPort", 123456},
            new Object[]{"setMongoServer", "getMongoServer", GlobalContent.DUMMY_STRING},
            new Object[]{"setMongoUser", "getMongoUser", GlobalContent.DUMMY_STRING}
        };
    }

    /**
     * Setup method.
     */
    @Override
    @BeforeMethod()
    public void setUp() {
        super.setUp();
        instance = new MongoConfigurationEntity();
    }

    /**
     * Tester for getter and setters.
     *
     * @param setMethodName method to test as setter
     * @param getMethodName method to test as getter
     * @param methodParameter data to set and afterward to get
     */
    @Override
    @Test(dataProvider = "getSettersAndGettersProvider")
    public void testGettersAndSetters(String setMethodName, String getMethodName, Object methodParameter) {
        super.testGettersAndSetters(setMethodName,getMethodName, methodParameter);
    }
}
