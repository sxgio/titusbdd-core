package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.ClickableImpl;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.WebDriverImpl;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * <strong>ClickableTest.java</strong><br>
 * <strong>ClickableTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class ClickableTest {

  /** Instance to test. */
  private ClickableImpl instance;

  /** Webdriver Mock. */
  @Mock WebDriverImpl webDriver;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(ClickableTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, ClickableTest.class.getSimpleName()));
    instance = new ClickableImpl();
    webDriver = Mockito.mock(WebDriverImpl.class);
    when(webDriver.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, ClickableTest.class.getSimpleName()));
    instance = null;
  }

  /** Tester for clickOnTheButton method. */
  @Test()
  public void testClickOnTheButton() {
    instance.clickOnTheButton("input0", webDriver);
  }

  /** Tester for clickOnTheButton method. */
  @Test()
  public void testClickOnTheButton2() {
    WebElement button = Mockito.mock(WebElement.class);
    instance.clickOnTheButton(button);
  }

  /** Tester for clickOnTheElement method. */
  @Test()
  public void testCickOnTheElement() {
    instance.clickOnTheElement("input0", webDriver);
  }

  /** Tester for clickOnTheElement method. */
  @Test()
  public void testClickOnTheElement2() {
    instance.clickOnTheElement(
        Mockito.mock(WebElement.class), Mockito.mock(JavascriptExecutor.class));
  }

  /** Tester for clickOnTheElement method. */
  @Test()
  public void testClickOnTheElement() {
    Actions actions = Mockito.mock(Actions.class);
    when(actions.click(any())).thenReturn(actions);
    instance.clickOnTheElement(Mockito.mock(WebElement.class), actions);
  }

  /** Tester for formatAndClickOnTheElement method. */
  @Test()
  public void testFormatAndClickOnTheElement() {
    instance.formatAndClickOnTheElement(webDriver, "", null);
  }
}
