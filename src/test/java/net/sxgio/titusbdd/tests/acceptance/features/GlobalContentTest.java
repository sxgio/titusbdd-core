package net.sxgio.titusbdd.tests.acceptance.features;

import static junit.framework.Assert.assertNotNull;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.testng.Assert.assertEquals;

import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * <strong>GlobalContentTest.java</strong><br>
 * <strong>GlobalContentTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class GlobalContentTest {

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(GlobalContentTest.class.getName());

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(String.format(CLEANING_STATUS_TEXT, GlobalContentTest.class.getSimpleName()));
  }

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(String.format(ACTIVATING_TEST_TEXT, GlobalContentTest.class.getSimpleName()));
  }

  /**
   * Provider for getter and setters methods
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] getStringToBeParserProvider() {
    // Params defined
    return new Object[][] {
      // Right params
      new Object[] {"change data %s", "as soon as possible", "change data as soon as possible"},
      new Object[] {"change number %d", 3, "change number 3"}
    };
  }

  @Test(dataProvider = "getStringToBeParserProvider")
  public void testParseContent(String toBeFormatted, Object actors, String whatExpected) {
    assertEquals(GlobalContent.parseContent(toBeFormatted, actors), whatExpected);
  }

  @Test()
  public void testPrintFullStack() {
    assertNotNull(GlobalContent.printFullStack(new DriverException("Test")));
  }
}
