package net.sxgio.titusbdd.tests.acceptance.features;

import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;
import static org.testng.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Logger;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.RestDataEntity;
import net.sxgio.titusbdd.tests.acceptance.features.mocks.TestContextImpl;
import net.sxgio.titusbdd.tests.acceptance.utils.SharedContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

/**
 * <strong>RunMainTestTest.java</strong><br>
 * <strong>RunMainTestTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RunMainTestTest {

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(RunMainTestTest.class.getName());

  /** Instance to test. */
  private RunMainTest instance;

  /** ITestContext Mock. */
  @Mock TestContextImpl testContext;

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(String.format(CLEANING_STATUS_TEXT, RunMainTestTest.class.getSimpleName()));
  }

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(String.format(ACTIVATING_TEST_TEXT, RunMainTestTest.class.getSimpleName()));
    instance = new RunMainTest();
    testContext = Mockito.mock(TestContextImpl.class);
    when(testContext.getCurrentXmlTest()).thenReturn(new XmlTest());
  }

  /**
   * Provider for getter and setters methods
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] getSettersAndGettersProvider() {
    // Params defined
    return new Object[][] {
      // Right params
      new Object[] {"setSharedContent", "getSharedContent", new SharedContent()},
      new Object[] {"setGlobalProperties", "getGlobalProperties", new GlobalConfigurationEntity()},
      new Object[] {"setOAuth2TokenEntity", "getOAuth2TokenEntity", new RestDataEntity()},
    };
  }

  @Test(dataProvider = "getSettersAndGettersProvider")
  public void testGettersAndSetters(
      String setMethodName, String getMethodName, Object methodParameter) {
    Arrays.stream(RunMainTest.class.getMethods())
        .filter(s -> s.getName().startsWith(setMethodName))
        .forEach(
            x -> {
              try {
                x.invoke(instance, methodParameter);
                assertNotNull(instance);
                logger.info("Method " + x.getName() + " called properly");
              } catch (IllegalAccessException
                  | IllegalArgumentException
                  | InvocationTargetException e) {
                final String message = "Exception raised with no control at all: " + e.getMessage();
                logger.severe(message);
                fail(message);
              }
            });

    Arrays.stream(instance.getClass().getMethods())
        .filter(s -> s.getName().startsWith(getMethodName))
        .forEach(
            x -> {
              try {
                Object retval = x.invoke(instance);
                assertNotNull(retval);
                assertSame(retval, methodParameter);
                logger.info("Method " + x.getName() + " called properly");
                logger.info(
                    "Parameter/s passed, "
                        + methodParameter
                        + ", and obtained value/s, "
                        + retval
                        + " are as expected");
              } catch (IllegalAccessException
                  | IllegalArgumentException
                  | InvocationTargetException e) {
                final String message = "Exception raised with no control at all: " + e.getMessage();
                logger.severe(message);
                fail(message);
              }
            });
  }

  @Test()
  public void testScenarios() {
    assertNotNull(instance.scenarios());
  }

  @Test(expectedExceptions = {NullPointerException.class})
  public void testTearDownClass() {
    // Finishing test. Property testNGCucumberRunner should be null
    instance.tearDownClass();
    // Verifying testNGCucumberRunner is null.
    // NullPointerException expected from this.testNGCucumberRunner.runScenario.
    instance.runScenario(null, null);
  }

  @Test(expectedExceptions = {IllegalStateException.class})
  public void testSetUpClass() {
    instance.setUpClass(testContext);
  }
}
