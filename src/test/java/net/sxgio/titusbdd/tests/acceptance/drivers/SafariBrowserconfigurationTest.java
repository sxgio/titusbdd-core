package net.sxgio.titusbdd.tests.acceptance.drivers;

import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * <strong>SafariBrowserconfigurationTest.java</strong><br>
 * <strong>SafariBrowserconfigurationTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class SafariBrowserconfigurationTest {

  /** Using with Mockito openMock */
  AutoCloseable closeable;

  /** Instance to test. */
  private SafariBrowserConfiguration instance;

  /** Common logger to know what is happening as the tests are being executed */
  public static final Logger logger =
      Logger.getLogger(SafariBrowserconfigurationTest.class.getName());

  /** Safari Driver */
  @Mock SafariDriver safariDriver;

  /** QaProperties */
  @Mock
  GlobalConfigurationEntity globalProperties;

  /** Web Data Entity. */
  @Mock
  WebDataEntity webDataEntity;

  /** Setup method */
  @BeforeMethod()
  public void setUp() {
    instance = new SafariBrowserConfiguration();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    // Initialize mocking system
    closeable = MockitoAnnotations.openMocks(this);
    when(webDataEntity.getBrowserCapabilities()).thenReturn("{\"foo\": \"true\"}");
    when(globalProperties.getWebData()).thenReturn(webDataEntity);
    instance.setGlobalProperties(globalProperties);
  }

  /** Teardown method.
   *
   * @throws Exception just in case something happened with AutoCloseable
   *
   */
  @AfterMethod()
  public void tearDown() throws Exception {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
    instance = null;
    closeable.close();
  }

  /** Tester for getCapabilities method */
  @Test()
  public void testGetCapabilities() {
    assertNull(instance.getCapabilities());
    instance.init();
    assertNotNull(instance.getCapabilities());
  }

  /** Tester for getDriver method */
  @Test()
  public void testGetDriver() {
    System.setProperty("webdriver.safari.driver", "bin/safaridriver.exe");
    instance.setSafariDriverInstance(safariDriver);
    SafariDriver test = (SafariDriver) instance.getDriver();
    assertNotNull(test);
    test.close();
  }

  /** Tester for getDriver method */
  @Test(expectedExceptions = {WebDriverException.class}, dependsOnMethods = "testInit")
  public void testGetDriverWithNullSafariInstance() {
    instance.setSafariDriverInstance(null);
    SafariDriver test = (SafariDriver) instance.getDriver();
  }

  /** Tester for init method */
  @Test()
  public void testInit() {
    instance.init();
    assertSame(instance.getOptions(), instance.getCapabilities());
  }

  /** Tester for get and set safari driver instance property */
  @Test()
  public void testSetAndGetSafariDriverInstance() {
    instance.setSafariDriverInstance(null);
    assertNull(instance.getSafariDriverInstance());
  }

  /** Tester for set and get options property */
  @Test()
  public void testSetAndGetOptions() {
    instance.setOptions(null);
    assertNull(instance.getOptions());
  }
}
