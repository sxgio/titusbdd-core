package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.utils.SharedContent;
import org.apache.commons.lang.NotImplementedException;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * <strong>NotFoundPageObjectJSOUPTest.java</strong><br>
 * <strong>NotFoundPageObjectJSOUPTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class NotFoundPageObjectJSOUPTest {
    /** Common logger to know what is happening as the tests are being executed. */
    public static final Logger logger = Logger.getLogger(NotFoundPageObjectJSOUPTest.class.getName());

    /** Instance to test. **/
    private NotFoundPageObjectJSOUP instance;

    /** Setup method. */
    @BeforeMethod()
    public void setUp() throws DriverException {
        logger.info(String.format(ACTIVATING_TEST_TEXT, NotFoundPageObjectJSOUP.class.getSimpleName()));
        instance = new NotFoundPageObjectJSOUP();
    }

    /** Teardown method. */
    @AfterMethod()
    public void tearDown() {
        logger.info(String.format(CLEANING_STATUS_TEXT, NotFoundPageObjectJSOUP.class.getSimpleName()));
        instance = null;
    }

    /** Tester for getDriver method. */
    @Test(expectedExceptions = NotImplementedException.class)
    public void getDriverTest() {
        instance.getDriver();
    }

    /**
     * Tester for loadHomePage method.
     *
     */
    @Test()
    public void loadUnknownPageTest() {
        GlobalConfigurationEntity globalProperties = Mockito.mock(GlobalConfigurationEntity.class);
        WebDataEntity webDataEntity = Mockito.mock(WebDataEntity.class);
        SharedContent sharedContent = Mockito.mock(SharedContent.class);
        when(webDataEntity.getBaseKeyStorePassword()).thenReturn("");
        when(webDataEntity.getBaseKeyStore()).thenReturn("");
        when(webDataEntity.getBaseUrl()).thenReturn("");
        when(globalProperties.getWebData()).thenReturn(webDataEntity);
        RunMainTest.setGlobalProperties(globalProperties);
        RunMainTest.setSharedContent(sharedContent);
        // Error raised. Malformed URL
        assertFalse(instance.loadUnknownPage());
        when(globalProperties.getWebData().getBaseUrl()).thenReturn("http://www.sxgio.net");
        RunMainTest.setGlobalProperties(globalProperties);
        // 404 obtained
        assertTrue(instance.loadUnknownPage());
    }
}
