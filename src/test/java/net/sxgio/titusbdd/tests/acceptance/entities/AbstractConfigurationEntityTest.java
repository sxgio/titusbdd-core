package net.sxgio.titusbdd.tests.acceptance.entities;

import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterMethod;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Logger;

import static org.testng.Assert.*;
import static org.testng.Assert.fail;

/**
 * <strong>AbstractConfigurationBeanTest.java</strong><br>
 * <strong>AbstractConfigurationBeanTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 03/02/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public abstract class AbstractConfigurationEntityTest {

  /**
   * Instance to test.
   */
  protected BaseConfigurationEntityInterface instance;

  /**
   * Common logger to know what is happening as the tests are being executed.
   */
  protected Logger logger = Logger.getLogger(this.getClass().getName());

  /**
   * Tester for getter and setters.
   *
   * @param setMethodName method to test as setter
   * @param getMethodName method to test as getter
   * @param methodParameter data to set and afterward to get
   */
  public void testGettersAndSetters(String setMethodName, String getMethodName, Object methodParameter) {
    Arrays.stream(instance.getClass().getMethods())
      .filter(s -> s.getName().startsWith(setMethodName))
      .forEach(
        x -> {
          try {
            x.invoke(instance, methodParameter);
            assertNotNull(instance);
            logger.info("Method " + x.getName() + " called properly");
          } catch (IllegalAccessException
            | IllegalArgumentException
            | InvocationTargetException e) {
            final String message = "Exception raised with no control at all: " + e.getMessage();
            logger.severe(message);
            fail(message);
          }
        }
      );

    Arrays.stream(instance.getClass().getMethods())
      .filter(s -> s.getName().startsWith(getMethodName))
      .forEach(
        x -> {
          try {
            Object retval = x.invoke(instance);
            assertNotNull(retval);
            assertEquals(retval, methodParameter);
            logger.info("Method " + x.getName() + " called properly");
            logger.info(
              "Parameter/s passed, "
                + methodParameter
                + ", and obtained value/s, "
                + retval
                + " are as expected");
          } catch (IllegalAccessException
            | IllegalArgumentException
            | InvocationTargetException e) {
            final String message = "Exception raised with no control at all: " + e.getMessage();
            logger.severe(message);
            fail(message);
          }
        }
      );
  }

  /**
   * Setup method.
   */
  public void setUp() {
    logger.info(
      String.format(GlobalContent.ACTIVATING_TEST_TEXT, this.getClass().getSimpleName()));
  }

  /**
   * Teardown method.
   */
  @AfterMethod()
  public void tearDown() {
    logger.info(
      String.format(GlobalContent.CLEANING_STATUS_TEXT, this.getClass().getSimpleName()));
    instance = null;
  }
}
