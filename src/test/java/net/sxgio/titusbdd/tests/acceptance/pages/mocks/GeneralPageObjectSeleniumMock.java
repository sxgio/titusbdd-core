package net.sxgio.titusbdd.tests.acceptance.pages.mocks;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.pages.AbstractPageObjectSelenium;

/**
 * <strong>GeneralPageObjectSeleniumMock.java</strong><br>
 * <strong>GeneralPageObjectSeleniumMock class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class GeneralPageObjectSeleniumMock extends AbstractPageObjectSelenium {

    /**
     * Constructor by default.
     *
     * @throws DriverException Just in case a selenium webdriver error happened
     */
    public GeneralPageObjectSeleniumMock(BrowserDriver browserDriver) throws DriverException {
        super(browserDriver);
    }
}
