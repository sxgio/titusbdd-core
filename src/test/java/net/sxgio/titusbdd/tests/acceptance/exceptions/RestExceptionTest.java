package net.sxgio.titusbdd.tests.acceptance.exceptions;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

/**
 * <strong>RestExceptionTest.java</strong><br>
 * <strong>RestExceptionTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RestExceptionTest {

  /** Instance to test. */
  private RestException instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(RestExceptionTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, RestException.class.getSimpleName()));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, RestException.class.getSimpleName()));
    instance = null;
  }

  @Test()
  public void testGettersAndSetters() {
    final String message = "test";
    instance = new RestException(message);
    assertEquals(instance.getMessage(), message);
    instance = null;
    instance = new RestException(message, new Exception(message + message));
    assertEquals(instance.getMessage(), message);
  }
}
