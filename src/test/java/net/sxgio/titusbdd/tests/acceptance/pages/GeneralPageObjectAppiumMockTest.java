package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.pages.mocks.BrowserDriver;
import net.sxgio.titusbdd.tests.acceptance.pages.mocks.GeneralPageObjectAppiumMock;
import org.mockito.Mock;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.testng.Assert.*;

/**
 * <strong>GeneralPageObjectAppium.java</strong><br>
 * <strong>GeneralPageObjectAppium class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class GeneralPageObjectAppiumMockTest {

    /** Common logger to know what is happening as the tests are being executed. */
    public static final Logger logger = Logger.getLogger(GeneralPageObjectAppiumMockTest.class.getName());

    /** Instance to test. **/
    private GeneralPageObjectAppiumMock instance;

    @Mock private BrowserDriver browserDriverMock;

    /** Teardown method. */
    @AfterMethod()
    public void tearDown() {
        logger.info(String.format(CLEANING_STATUS_TEXT, GeneralPageObjectAppiumMock.class.getSimpleName()));
        instance = null;
    }

    /** Setup method. */
    @BeforeMethod()
    public void setUp() throws DriverException {
        HashSet<Class<?>> classSet = new HashSet<>();
        classSet.add(BrowserDriver.class);
        RunMainTest.getFeeder().initFactory(classSet);
        logger.info(String.format(ACTIVATING_TEST_TEXT, GeneralPageObjectAppiumMock.class.getSimpleName()));
        RunMainTest.getFeeder().getFactory().addClass(BrowserDriver.class);
        instance = new GeneralPageObjectAppiumMock();
    }

    @Test()
    public void getDriverTest() {
        assertSame(
            instance.getDriver(),
            RunMainTest.getFeeder().getFactory().getInstance(BrowserDriver.class).getWebDriver()
        );
    }

    @Test()
    public void setDriverTest() {
        instance.setDriver(RunMainTest.getFeeder().getFactory().getInstance(BrowserDriver.class).getWebDriver());
        assertNotNull(instance.getDriver());
        assertSame(
            instance.getDriver(),
            RunMainTest.getFeeder().getFactory().getInstance(BrowserDriver.class).getWebDriver()
        );
    }

    @Test()
    public void findTheElementTest() {
        assertNotNull(instance.findTheElement("", instance.getDriver()));
    }

    @Test()
    public void findTheElementsTest() {
        assertNotNull(instance.findTheElements("", instance.getDriver()));
    }

    @Test()
    public void clearTheElementTest() {
        instance.clearTheElement("");
    }

    @Test(expectedExceptions = ClassCastException.class)
    public void getCurrentContextHandleTest() {
        // TODO: Check it carefully to verify how to inject RemoteWebDriver
        assertNotNull(instance.getCurrentContextHandle());
    }

    @Test(expectedExceptions = ClassCastException.class)
    public void getContextHandlesTest() {
        // TODO: Check it carefully to verify how to inject RemoteWebDriver
        assertNotNull(instance.getContextHandles());
    }

    @Test(expectedExceptions = TimeoutException.class)
    public void goToFrameByIndexTest() {
        instance.goToFrame(0);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void goToFrameByTextTest() {
        // Check WebDriver.TargetLocator to inject a WebDriver properly formatted
        instance.goToFrame("index");
    }
}
