package net.sxgio.titusbdd.tests.acceptance.entities;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * <strong>OAuth2TokenBeanTest.java</strong><br>
 * <strong>OAuth2TokenBeanTest class</strong><br>
 *
 * @author Daniel Salgado daniel.salgado.p@gmail.com
 * * @version Release: 0.1.0 07/02/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RestDataEntityTest extends AbstractConfigurationEntityTest {

    /**
     * Provider for getter and setters methods
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] getSettersAndGettersProvider() {
        // Params defined
        return new Object[][]{
            // Right params
            new Object[]{"setActive", "isActive", true},
            new Object[]{"setName", "getName", GlobalContent.DUMMY_STRING},
            new Object[]{"setClientId", "getClientId", GlobalContent.DUMMY_STRING},
            new Object[]{"setClientSecret", "getClientSecret", GlobalContent.DUMMY_STRING},
            new Object[]{"setGrantType", "getGrantType", GlobalContent.DUMMY_STRING},
            new Object[]{"setResources", "getResources", Map.of("key1", "value1")},
            new Object[]{"setScope", "getScope", GlobalContent.DUMMY_STRING},
            new Object[]{"setUrl", "getUrl", GlobalContent.DUMMY_STRING},

        };
    }

    /**
     * Setup method.
     */
    @Override
    @BeforeMethod()
    public void setUp() {
        super.setUp();
        instance = new RestDataEntity();
    }

    /**
     * Tester for getter and setters.
     *
     * @param setMethodName method to test as setter
     * @param getMethodName method to test as getter
     * @param methodParameter data to set and afterward to get
     */
    @Override
    @Test(dataProvider = "getSettersAndGettersProvider")
    public void testGettersAndSetters(String setMethodName, String getMethodName, Object methodParameter) {
        super.testGettersAndSetters(setMethodName,getMethodName, methodParameter);
    }
}
