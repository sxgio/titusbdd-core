package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.FillableImpl;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.WebDriverImpl;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * <strong>FillableTest.java</strong><br>
 * <strong>FillableTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class FillableTest {

  /** Instance to test. */
  private FillableImpl instance;

  /** Webdriver Mock. */
  @Mock WebDriverImpl webDriver;

  /** WebElement Mock. */
  @Mock WebElement webElement;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(FillableTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, FillableTest.class.getSimpleName()));
    instance = new FillableImpl();
    webDriver = Mockito.mock(WebDriverImpl.class);
    webElement = Mockito.mock(WebElement.class);
    when(webElement.getTagName()).thenReturn("select");
    when(webElement.isEnabled()).thenReturn(true);
    when(webElement.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
    when(webElement.findElements(any())).thenReturn(Mockito.mock(List.class));
    doNothing().when(webElement).sendKeys(any());
    when(webDriver.findElement(any())).thenReturn(webElement);
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, FillableTest.class.getSimpleName()));
    instance = null;
  }

  /** Tester for fillAutocompleteField method. */
  @Test()
  public void testFillAutocompleteField() {
    instance.fillAutocompleteField(Mockito.mock(WebElement.class), "//test %s", webDriver, "test");
  }

  /** Tester for fillAutocompleteField method. */
  @Test()
  public void testFillAutocompleteField2() {
    instance.fillAutocompleteField("test", "blah,blah", webDriver, "");
  }

  /** Tester for fillTextField method.
   *
   * @throws DriverException just in case of an error
   **/
  @Test()
  public void testFillTextField() throws DriverException {
    instance.fillTextField("test", "blah,blah", webDriver);
  }

  /** Tester for fillTextField method.
   *
   * @throws DriverException just in case of an error
   * */
  @Test()
  public void testFillTextField2() throws DriverException {
    instance.fillTextField("test", "blah,blah", null, webDriver);
  }

  /** Tester for fillTextField method.
   *
   * @throws DriverException just in case of an error
   * */
  @Test(expectedExceptions = DriverException.class)
  public void testFillTextFieldWithException() throws DriverException {
    instance.fillTextField("test", "blah,blah", "", webDriver);
  }

  /** Tester for fillTextField method.
   *
   * @throws DriverException just in case of an error
   * */
  @Test()
  public void testFillTextFieldWithCallback() throws DriverException {
    instance.fillTextField("test", "blah,blah", "callbackMethod", webDriver);
  }

  /** Tester for fillTextField method.
   *
   * @throws DriverException just in case of an error
   * */
  @Test()
  public void testFillTextField3() throws DriverException {
    instance.fillTextField(webElement, "blah,blah", null);
  }

  /** Tester for fillSelectField method. */
  @Test(expectedExceptions = NullPointerException.class)
  public void testFillSelectField2() {
    instance.fillSelectField("test", "blah,blah", webDriver);
  }

  /** Tester for fillSelectFieldByValue method. */
  @Test(expectedExceptions = NullPointerException.class)
  public void testFillSelectFieldByValue() {
    instance.fillSelectFieldByValue("test", "blah,blah", webDriver);
  }
}
