package net.sxgio.titusbdd.tests.acceptance.pages.mocks;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.pages.AbstractPageObjectAppium;

/**
 * <strong>GeneralPageObjectAppiumMock.java</strong><br>
 * <strong>GeneralPageObjectAppiumMock class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class GeneralPageObjectAppiumMock extends AbstractPageObjectAppium {

    /**
     * Constructor by default.
     *
     * @throws DriverException WebDriver not suitable to proceed with the tests
     */
    public GeneralPageObjectAppiumMock() throws DriverException {
        super();
    }
}
