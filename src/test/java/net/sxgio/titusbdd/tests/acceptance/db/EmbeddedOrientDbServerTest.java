package net.sxgio.titusbdd.tests.acceptance.db;

import com.orientechnologies.orient.server.OServer;
import net.sxgio.titusbdd.tests.acceptance.exceptions.OrientDbException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.logging.Logger;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * <strong>EmbeddedOrientDbServerTest.java</strong><br>
 * <strong>EmbeddedOrientDbServerTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class EmbeddedOrientDbServerTest 
{
    /**
     * Instance to test.
     */
    private EmbeddedOrientDbServer instance;

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(EmbeddedOrientDbServerTest.class.getName());

    /**
     * OrientDb Exception text
     */
    public static final String ORIENTDB_EXCEPTION_RAISED_TEXT = "OrientDb exception raised in %s";

    /**
     * OrientDb Server Mock
     */
    @Mock    
    OServer serverMock;

    /**
     * Setup method
     */
    @BeforeTest()
    public void setUp() {
        // Initialize mocking system
        MockitoAnnotations.initMocks(this);                
    }

    /**
     * Tester for shutdown methods
     */
    @Test(dependsOnMethods = {"testStart"})
    public void testShutdown() {
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
        instance.shutdown();
        assertNull(instance.getServer());
    }

    /**
     * Provider with info to read some elements stored in the app.
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] startProvider() {        
        // Getting driver file
        final String OrientDbbaseUrl = "target/test/not-available";
        final String configFile = "src/test/resources/config/db.config";
        final String orientdbHome = new File(OrientDbbaseUrl).getAbsolutePath();
        
        // Setting ORIENTDB_HOME
        System.setProperty("ORIENTDB_HOME", orientdbHome);
        
        // Mocking functions
        when(serverMock.isActive()).thenReturn(false);
        when(serverMock.shutdown()).thenReturn(true);    
        try {            
            when(serverMock.startup()).thenReturn(this.serverMock);
            when(serverMock.activate()).thenReturn(this.serverMock);        
        } catch (Exception e) {
            // mock should be null 
            serverMock = null;
        }

        return new Object[][] {
            // Wrong arguments
            new Object[] {null, configFile, false},
            new Object[] {null, null, false},
            new Object[] { serverMock, configFile+".old", false},
            // Right arguments            
            new Object[] { serverMock, configFile, true}
        };
    }

    /**
     * Tester for start method
     *
     * @param serverMock        mocking for server entrance
     * @param configFile        file with the configuration
     * @param serverFinalStatus server final status
     *
     */
    @Test(dataProvider = "startProvider")
    public void testStart(OServer serverMock, String configFile, boolean serverFinalStatus) {
        try {                     
            instance = new EmbeddedOrientDbServer(serverMock, configFile);
            logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
            instance.start();    
            when(serverMock.isActive()).thenReturn(serverFinalStatus);        
            if (serverFinalStatus) {
                assertTrue(instance.getServer().isActive());
                OServer firstInstance = instance.getServer();
                // Server is already started. Starting again should do nothing.
                instance.start();
                assertTrue(instance.getServer().isActive());
                assertSame(firstInstance, instance.getServer());
            } else {
                assertFalse(instance.getServer().isActive());
            } 
        } catch (OrientDbException e) {
            logger.warning(
                String.format(
                    ORIENTDB_EXCEPTION_RAISED_TEXT,
                    EmbeddedOrientDbServerTest.class.getSimpleName()
                )
            );
            if (serverMock == null) {
                assertNull(instance.getServer());                            
            } else {
                assertFalse(instance.getServer().isActive());
            }            
        } catch (Exception e) {
            logger.warning(EmbeddedOrientDbServer.ORIENTDB_UNKNOWN_ERROR_TEXT);
            assertNull(instance.getServer());                        
        }         
    }

    /**
     * Teardown method
     */
    @AfterTest()
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
        instance = null;
    }
}