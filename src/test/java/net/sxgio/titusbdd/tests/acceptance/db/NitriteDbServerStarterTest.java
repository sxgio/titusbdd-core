package net.sxgio.titusbdd.tests.acceptance.db;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.NitriteCollection;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.dizitart.no2.filters.Filters.and;
import static org.dizitart.no2.filters.Filters.eq;
import static org.testng.Assert.*;

/**
 * <strong>NitriteDbServerStarterTest.java</strong><br>
 * <strong>NitriteDbServerStarterTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 08/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class NitriteDbServerStarterTest {
    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(NitriteDbServerStarterTest.class.getName());

    /**
     * Tester for main method
     */
    @Test()
    public void mainTest() {
        System.setProperty("nitriteDb.data.file", "target/nitrite-test-2.db");
        System.setProperty("testNitrite.db", "test");
        String[] args = {"admin", "admin"};
        NitriteDbServerStarter.main(args);
        assertNotNull(NitriteDbServerStarter.getEmbeddedNitriteDbServer());
        NitriteDbServerStarter.getEmbeddedNitriteDbServer().shutdown();
        assertTrue(NitriteDbServerStarter.getEmbeddedNitriteDbServer().getServer().isClosed());
    }

    /**
     * Setup method
     */
    @BeforeTest
    public void setUp() {
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, NitriteDbServerStarter.class.getSimpleName()));
    }

    /**
     * Teardown method
     */
    @AfterTest
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, NitriteDbServerStarter.class.getSimpleName()));
    }

    /**
     * Tester for main method with wrong parameters
     */
    @Test(dependsOnMethods = {"mainTest"})
    public void wrongMainTest() {
        String[] args = null;
        System.setProperty("nitriteDb.data.file", "target/nitrite-test-2.db");
        System.setProperty("testNitrite.db", "");
        NitriteDbServerStarter.main(args);
        assertTrue(NitriteDbServerStarter.getEmbeddedNitriteDbServer().getServer().isClosed());
    }

    /**
     * Tester an actualization in Nitrite
     */
    @Test(dependsOnMethods = {"wrongMainTest"})
    public void actualizeNitriteTest() {
        Map<String, Map<String, String>> map = new HashMap<>() {{
            put(
                "src/test/resources/nitrite/nitrite-example-1.json",
                new HashMap<>() {{put("name", "gherkin-id"); put("value", "first-gherkin-id");}}
            );
        }};
        String[] args = {"admin", "admin"};
        NitriteDbServerStarter.main(args);
        NitriteCollection collection = NitriteDbServerStarter.actualizeNitrite(map, "test");
        // Check data previously inserted.
        Cursor cursor = collection.find(and(eq("active", true)));
        for(Document document: cursor) {
            logger.log(Level.INFO, "Gherkin Identifier found {0}", document.get("gherkin-id"));
            assertEquals(document.get("gherkin-id"), "first-gherkin-id");
        }
    }

    /**
     * Tester an actualization in Nitrite
     */
    @Test(dependsOnMethods = {"actualizeNitriteTest"}, expectedExceptions = {NullPointerException.class})
    public void actualizeNitriteWithExceptionTest() {
        Map<String, Map<String, String>> map = new HashMap<>() {{
            put(
                "src/test/resources/nitrite/nitrite-example-2.json",
                new HashMap<>() {{put("name", "gherkin-id"); put("value", "first-gherkin-id");}}
            );
        }};
        String[] args = {"admin", "admin"};
        NitriteDbServerStarter.main(args);
        NitriteCollection collection = NitriteDbServerStarter.actualizeNitrite(map, "test");
        assertNull(collection);
    }
}
