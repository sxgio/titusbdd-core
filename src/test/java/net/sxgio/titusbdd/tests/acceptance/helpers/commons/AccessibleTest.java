package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Logger;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.AccessibleImpl;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.WebDriverImpl;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.PageObjectInterface;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * <strong>AccessibleTest.java</strong><br>
 * <strong>AccessibleTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class AccessibleTest {

  /** Instance to test. */
  private AccessibleImpl instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(AccessibleTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    instance = new AccessibleImpl();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, AccessibleTest.class.getSimpleName()));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, AccessibleTest.class.getSimpleName()));
    instance = null;
  }

  /** Tester for resizeImage method. **/
  @Test()
  public void testResizeImage() {
    BufferedImage bufferedImage = Mockito.mock(BufferedImage.class);
    Image image = Mockito.mock(Image.class);
    when(image.getScaledInstance(anyInt(), anyInt(), anyInt())).thenReturn(image);
    assertNotNull(instance.resizeImage(bufferedImage, 500, 500));
  }

  /** Tester for takeScreenshot method.
   *
   * @throws IOException just in case on an error.
   * **/
  @Test(expectedExceptions = {NullPointerException.class})
  public void testTakeScreenshot() throws IOException {
    PageObjectInterface pageObject = Mockito.mock(PageObjectInterface.class);
    when(pageObject.getDriver()).thenReturn(new WebDriverImpl());
    instance.takeScreenshot(null, pageObject);
  }
}
