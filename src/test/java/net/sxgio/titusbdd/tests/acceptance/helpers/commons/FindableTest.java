package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

import java.util.List;
import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.FindableImpl;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.WebDriverImpl;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * <strong>FindableTest.java</strong><br>
 * <strong>FindableTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class FindableTest {

  /** Instance to test. */
  private FindableImpl instance;

  /** Webdriver Mock. */
  @Mock WebDriverImpl webDriver;

  /** WebElement Mock. */
  @Mock WebElement webElement;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(FindableTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, FindableTest.class.getSimpleName()));
    instance = new FindableImpl();
    webDriver = Mockito.mock(WebDriverImpl.class);
    when(webDriver.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
    webElement = Mockito.mock(WebElement.class);
    when(webElement.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
    when(webElement.findElements(any())).thenReturn(Mockito.mock(List.class));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, FillableTest.class.getSimpleName()));
    instance = null;
  }

  /** Tester for findMyChild method. */
  @Test()
  public void testFindMyChild() {
    assertNotNull(instance.findMyChild(webElement, "test"));
  }

  /** Tester for findMyChilds method. */
  @Test()
  public void testFindMyChilds() {
    assertNotNull(instance.findMyChilds(webElement, "test"));
  }

  /** Tester for findTheElement method. */
  public void testFindTheElement() {
    assertNotNull(instance.findTheElement("test", webDriver));
  }

  /** Tester for findTheElements method. */
  public void testFindTheElements() {
    assertNotNull(instance.findTheElements("test", webDriver));
  }

  /** Tester for formatAndFindTheElement method. */
  public void testFormatAndFindTheElement() {
    assertNotNull(
        instance.formatAndFindTheElement(webDriver, "//%s['id'='%s']", "input", "input0"));
  }
}
