package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * <strong>ChromeBrowserconfigurationTest.java</strong><br>
 * <strong>ChromeBrowserconfigurationTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class ChromeBrowserConfigurationTest {

  /** Using with Mockito openMock */
  AutoCloseable closeable;

  /** Instance to test. */
  private ChromeBrowserConfiguration instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger =
      Logger.getLogger(ChromeBrowserConfigurationTest.class.getName());

  /** Chrome Driver. */
  @Mock ChromeDriver chromeDriver;

  /** QaProperties. */
  @Mock
  GlobalConfigurationEntity globalProperties;

  /** Web Data Entity. */
  @Mock
  WebDataEntity webDataEntity;


  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    instance = new ChromeBrowserConfiguration();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    // Initialize mocking system
    closeable = MockitoAnnotations.openMocks(this);
    when(webDataEntity.getBrowserCapabilities()).thenReturn("{\"foo\": \"true\"}");
    when(globalProperties.getWebData()).thenReturn(webDataEntity);
    instance.setGlobalProperties(globalProperties);
  }

  /** Teardown method.
   *
   * @throws Exception just in case something happened with AutoCloseable
   *
   */
  @AfterMethod()
  public void tearDown() throws Exception {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
    instance = null;
    closeable.close();
  }

  /** Tester for getCapabilities method. */
  @Test()
  public void testGetCapabilities() {
    assertNull(instance.getCapabilities());
    instance.init();
    assertNotNull(instance.getCapabilities());
  }

  /** Tester for getDriver method. */
  @Test()
  public void testGetDriver() {
    System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
    instance.setChromeDriverInstance(chromeDriver);
    ChromeDriver test = (ChromeDriver) instance.getDriver();
    assertNotNull(test);
    test.close();
  }

  /** Tester for getDriver method */
  @Test(expectedExceptions = {IllegalStateException.class}, dependsOnMethods = "testInit")
  public void testGetDriverWithNullChromeInstance() {
    instance.setChromeDriverInstance(null);
    ChromeDriver test = (ChromeDriver) instance.getDriver();
  }

  /**
   * Tester for getAppiumDriver method.
   *
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  @Test()
  public void testGetAppiumDriver() throws DriverException {
    AppiumDriver test = instance.getAppiumDriver();
    assertNull(test);
  }

  /** Tester for init method. */
  @Test()
  public void testInit() {
    instance.init();
    assertSame(instance.getOptions(), instance.getCapabilities());
  }

  /** Tester for init method */
  @Test()
  public void testInitWithWrongJSONCapabilities() {
    when(globalProperties.getWebData().getBrowserCapabilities())
            .thenReturn("{\"foo\": \"true\", \"foo1: \"false\"}");
    when(globalProperties.getWebData().isHeadless()).thenReturn(true);
    instance.setGlobalProperties(globalProperties);
    instance.init();
    assertSame(instance.getOptions(), instance.getCapabilities());
  }

  /** Tester for get and set chromeDriverInstance property. */
  @Test()
  public void testSetAndGetChromeDriverInstance() {
    instance.setChromeDriverInstance(null);
    assertNull(instance.getChromeDriverInstance());
  }

  /** Tester for set and get options property. */
  @Test()
  public void testSetAndGetOptions() {
    instance.setOptions(null);
    assertNull(instance.getOptions());
  }
}
