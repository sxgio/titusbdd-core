package net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks;

import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Fillable;

/**
 * <strong>FillableImpl.java</strong><br>
 * <strong>FillableImpl class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class FillableImpl implements Fillable {

  /**
   * Method to test callback dependent methods.
   *
   * @param fieldValue any string possible.
   * @return String echo value
   */
  public String callbackMethod(String fieldValue) {
    return fieldValue;
  }
}
