package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.pages.mocks.BrowserDriver;
import net.sxgio.titusbdd.tests.acceptance.pages.mocks.GeneralPageObjectSeleniumMock;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

/**
 * <strong>GeneralPageObjectSeleniumMockTest.java</strong><br>
 * <strong>GeneralPageObjectSeleniumMockTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release date: 4/2/24
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since File available since 4/2/24
 */
@Test(groups = {"units"})
public class GeneralPageObjectSeleniumMockTest {
    /** Common logger to know what is happening as the tests are being executed. */
    public static final Logger logger = Logger.getLogger(GeneralPageObjectSeleniumMockTest.class.getName());

    /** Instance to test. **/
    private GeneralPageObjectSeleniumMock instance;

    @Mock
    private BrowserDriver browserDriverMock;

    @Mock
    private WebDriver webdriverMock;

    @Mock
    private WebDriver.TargetLocator targetLocatorMock;

    /** Teardown method. */
    @AfterMethod()
    public void tearDown() {
        logger.info(String.format(CLEANING_STATUS_TEXT, GeneralPageObjectSeleniumMock.class.getSimpleName()));
        instance = null;
    }

    /** Setup method. */
    @BeforeMethod()
    public void setUp() throws DriverException {
        logger.info(String.format(ACTIVATING_TEST_TEXT, GeneralPageObjectSeleniumMock.class.getSimpleName()));
        browserDriverMock = Mockito.mock(BrowserDriver.class);
        webdriverMock = Mockito.mock(WebDriver.class);
        targetLocatorMock = Mockito.mock(WebDriver.TargetLocator.class);
        when(targetLocatorMock.defaultContent()).thenReturn(webdriverMock);
        when(targetLocatorMock.frame(anyString())).thenReturn(webdriverMock);
        when(targetLocatorMock.frame(anyInt())).thenReturn(webdriverMock);
        when(webdriverMock.switchTo()).thenReturn(targetLocatorMock);
        when(webdriverMock.getWindowHandle()).thenReturn("test");
        doNothing().when(webdriverMock).close();
        when(browserDriverMock.getCurrentDriver()).thenReturn(webdriverMock);

        instance = new GeneralPageObjectSeleniumMock(browserDriverMock);
    }

    @Test
    public void initElementsTest() {
        instance.initElements();
        assertSame(instance.getDriver(), webdriverMock);
    }

    @Test
    public void goToFrameWithIdentifierTest() {
        instance.goToFrame("");
        Mockito.verify(instance.getDriver(),Mockito.times(2)).switchTo();
        Mockito.verify(targetLocatorMock,Mockito.times(1)).defaultContent();
        Mockito.verify(targetLocatorMock,Mockito.times(1)).frame("");
        assertSame(instance.getDriver(), webdriverMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void goToFrameWithIndexTest() {
        instance.goToFrame(1);
        Mockito.verify(instance.getDriver(),Mockito.times(1)).switchTo();
        Mockito.verify(targetLocatorMock,Mockito.times(1)).defaultContent();
        assertSame(instance.getDriver(), webdriverMock);
    }

    @Test
    public void goToWindowTest() {
        instance.goToWindow("");
        Mockito.verify(instance.getDriver(),Mockito.times(1)).switchTo();
        Mockito.verify(targetLocatorMock,Mockito.times(1)).window("");
        assertSame(instance.getDriver(), webdriverMock);
    }

    @Test
    public void goToWindowAndCloseCurrentTest() {
        instance.goToWindowAndCloseCurrent("test");
        Mockito.verify(instance.getDriver(),Mockito.times(1)).getWindowHandle();
        assertSame(instance.getDriver(), webdriverMock);
    }

    @Test
    public void goToWindowAndCloseCurrentDifferentTest() {
        instance.goToWindowAndCloseCurrent("not-equal");
        Mockito.verify(instance.getDriver(),Mockito.times(1)).getWindowHandle();
        Mockito.verify(instance.getDriver(),Mockito.times(1)).close();
        assertSame(instance.getDriver(), webdriverMock);
    }


    @Test
    public void loadPageTest() {
        instance.loadPage("test");
        Mockito.verify(instance.getDriver(),Mockito.times(1)).get("test");
        assertSame(instance.getDriver(), webdriverMock);
    }

}
