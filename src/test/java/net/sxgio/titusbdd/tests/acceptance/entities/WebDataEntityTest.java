package net.sxgio.titusbdd.tests.acceptance.entities;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * <strong>GlobalConfigurationBeanTest.java</strong><br>
 * <strong>GlobalConfigurationBeanTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 03/02/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class WebDataEntityTest extends AbstractConfigurationEntityTest {

     /**
     * Provider for getter and setters methods.
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] getSettersAndGettersProvider() {
        // Params defined
        return new Object[][]{
            // Right params
            new Object[]{"setAndroidDevice", "getAndroidDevice", GlobalContent.DUMMY_STRING},
            new Object[]{"setAndroidVersion", "getAndroidVersion", GlobalContent.DUMMY_STRING},
            new Object[]{"setApplicationFile", "getApplicationFile", GlobalContent.DUMMY_STRING},
            new Object[]{"setAppiumTest", "isAppiumTest", Boolean.FALSE},
            new Object[]{"setBaseKeyStore", "getBaseKeyStore", GlobalContent.DUMMY_STRING},
            new Object[]{"setBaseKeyStorePassword", "getBaseKeyStorePassword", GlobalContent.DUMMY_STRING},
            new Object[]{"setBaseUrl", "getBaseUrl", GlobalContent.DUMMY_STRING},
            new Object[]{"setBrowserCapabilities", "getBrowserCapabilities", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverClass", "getDriverClass", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverFile", "getDriverFile", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverName", "getDriverName", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverType", "getDriverType", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverUrl", "getDriverUrl", GlobalContent.DUMMY_STRING},
            new Object[]{"setDriverRemote", "getDriverRemote", GlobalContent.DUMMY_STRING},
            new Object[]{"setHeadless", "isHeadless", Boolean.FALSE},
            new Object[]{"setIosDevice", "getIosDevice", GlobalContent.DUMMY_STRING},
            new Object[]{"setIosVersion", "getIosVersion", GlobalContent.DUMMY_STRING},
            new Object[]{"setLanguagePatternFile", "getLanguagePatternFile", GlobalContent.DUMMY_STRING},
            new Object[]{"setName", "getName", GlobalContent.DUMMY_STRING},
            new Object[]{"setOrientDbConfiguration", "getOrientDbConfiguration", new OrientDbConfigurationEntity()},
            new Object[]{"setNitriteConfiguration", "getNitriteConfiguration", new NitriteConfigurationEntity()},
            new Object[]{"setSuiteXmlFile", "getSuiteXmlFile", "suiteXmlFile"}
        };
    }

    /**
     * Setup method.
     */
    @Override
    @BeforeMethod()
    public void setUp() {
        super.setUp();
        instance = new WebDataEntity();
    }

    /**
     * Tester for getter and setters.
     *
     * @param setMethodName method to test as setter
     * @param getMethodName method to test as getter
     * @param methodParameter data to set and afterward to get
     */
    @Override
    @Test(dataProvider = "getSettersAndGettersProvider")
    public void testGettersAndSetters(String setMethodName, String getMethodName, Object methodParameter) {
        super.testGettersAndSetters(setMethodName,getMethodName, methodParameter);
    }
}

