package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * <strong>IosAppiumConfigurationTest.java</strong><br>
 * <strong>IosAppiumConfigurationTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class IOSAppiumConfigurationTest {
  /** Instance to test. */
  private IOSAppiumConfiguration instance;

  /** Common logger to know what is happening as the tests are being executed */
  public static final Logger logger = Logger.getLogger(IOSAppiumConfigurationTest.class.getName());

  /** Appium Driver */
  @Mock AppiumDriver appiumDriver;

  /** Using with Mockito openMock */
  AutoCloseable closeable;

  /** Qa properties. */
  GlobalConfigurationEntity globalProperties;

  /** Setup method */
  @BeforeMethod()
  public void setUp() {
    instance = new IOSAppiumConfiguration();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    // Initialize mocking system
    closeable = MockitoAnnotations.openMocks(this);
    WebDataEntity webData = new WebDataEntity();
    globalProperties = new GlobalConfigurationEntity();
    globalProperties.setWebData(webData);
  }

  /** Teardown method.
   *
   * @throws Exception just in case something happened with AutoCloseable
   * */
  @AfterMethod()
  public void tearDown() throws Exception {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
    instance = null;
    globalProperties = null;
    closeable.close();
  }

  /** Tester for getCapabilities method */
  @Test()
  public void testGetCapabilities() {
    Capabilities capabilities = instance.getCapabilities();
    assertNotNull(capabilities);
  }

  /**
   * Tester for getDriver method
   *
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  @Test()
  public void testGetDriver() throws DriverException {
    instance.setAppiumDriverInstance(appiumDriver);
    AppiumDriver test = (AppiumDriver) instance.getDriver();
    assertNotNull(test);
    test.close();
  }

  /**
   * Tester for getAppiumDriver method
   *
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  @Test()
  public void testGetAppiumDriver() throws DriverException {
    instance.setAppiumDriverInstance(appiumDriver);
    AppiumDriver test = instance.getAppiumDriver();
    assertNotNull(test);
    test.close();
  }

  /**
   * Tester for getAppiumDriver method
   *
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  @Test(expectedExceptions = DriverException.class)
  public void testGetAppiumDriverWithWrongURL() throws DriverException {
    instance.setAppiumDriverInstance(null);
    globalProperties.getWebData().setDriverUrl("hukf://wrongurl");
    globalProperties.getWebData().setDriverRemote(
        "net.sxgio.titusbdd.tests.acceptance.drivers.AndroidAppiumConfiguration");
    globalProperties.getWebData().setApplicationFile("true");
    instance.setGlobalProperties(globalProperties);
    AppiumDriver test = instance.getAppiumDriver();
    assertNotNull(test);
    test.close();
  }

  /**
   * Tester for getAppiumDriver method
   *
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  @Test(expectedExceptions = DriverException.class)
  public void testGetAppiumDriverWithWrongGridSetUp() throws DriverException {
    instance.setAppiumDriverInstance(null);
    globalProperties.getWebData().setDriverUrl("hukf://wrongurl");
    globalProperties.getWebData().setDriverRemote("");
    globalProperties.getWebData().setApplicationFile("true");
    instance.setGlobalProperties(globalProperties);
    assertNull(instance.getAppiumDriverInstance());
    AppiumDriver test = instance.getAppiumDriver();
    assertNotNull(test);
    test.close();
  }

  /** Tester for init method */
  @Test()
  public void testInit() {
    globalProperties.getWebData().setApplicationFile("true");
    instance.setGlobalProperties(globalProperties);
    instance.init();
    assertSame(instance.getOptions(), instance.getCapabilities());
    globalProperties.getWebData().setApplicationFile("false");
    instance.setGlobalProperties(globalProperties);
    instance.init();
    assertSame(instance.getOptions(), instance.getCapabilities());
    assertEquals(instance.getOptions().getBrowserName(), "Safari");
  }

  /** Tester for get and set appiumDriverInstance property */
  @Test()
  public void testSetAndGetAppiumDriverInstance() {
    instance.setAppiumDriverInstance(null);
    assertNull(instance.getAppiumDriverInstance());
  }

  /** Tester for set and get options property */
  @Test()
  public void testSetAndGetOptions() {
    instance.setOptions(null);
    assertNull(instance.getOptions());
  }
}
