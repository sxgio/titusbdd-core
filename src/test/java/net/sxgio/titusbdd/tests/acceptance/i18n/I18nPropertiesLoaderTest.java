package net.sxgio.titusbdd.tests.acceptance.i18n;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

/**
 * <strong>I18nPropertiesLoaderTest.java</strong><br>
 * <strong>I18nPropertiesLoaderTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class I18nPropertiesLoaderTest {
  /** Instance to test. */
  private I18nPropertiesLoader instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(I18nPropertiesLoaderTest.class.getName());

  /**
   * Setup method.
   *
   * @throws IOException Never required
   */
  @BeforeMethod()
  public void setUp() throws IOException {
    System.setProperty("language.properties.file.pattern", "i18n/i18n-%s.properties");
    instance = new I18nPropertiesLoader();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
    instance = null;
  }

  /**
   * Provider for getter and setter of fileName property.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] fileNameProvider() {
    return new Object[][] {
      // Right arguments
      new Object[] {"filename"}, new Object[] {null}
    };
  }

  /**
   * Tester for getter and setter methods from fileName property.
   *
   * @param fileName a possible name of the file containing properties
   */
  @Test(dataProvider = "fileNameProvider")
  public void testSetAndGetFileName(String fileName) {
    instance.setFileName(fileName);
    assertSame(instance.getFileName(), fileName);
  }

  /**
   * Provider for getter and setter of internalProperties property.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] internalPropertiesProvider() {
    Properties properties = new Properties();
    properties.setProperty("index", "value");
    properties.setProperty("index-2", "value-2");
    return new Object[][] {
      // Right arguments
      new Object[] {properties}, new Object[] {null}
    };
  }

  /**
   * Tester for getter and setter methods from internalProperties property.
   *
   * @param properties Properties to be tested
   */
  @Test(dataProvider = "internalPropertiesProvider")
  public void testSetAndGetInternalProperties(Properties properties) {
    instance.setInternalProperties(properties);
    assertSame(instance.getInternalProperties(), properties);
  }

  /**
   * Provider for getter and setter of internalProperties property.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] internalPropertiesProviderNotNull() {
    Properties properties = new Properties();
    properties.setProperty("index", "value");
    properties.setProperty("index-2", "value-2");
    return new Object[][] {
      // Right arguments
      new Object[] {properties}
    };
  }

  /**
   * Provider for getter and setter of internalProperties property in null cases.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] internalPropertiesProviderNull() {
    return new Object[][] {
      // Right arguments
      new Object[] {null}
    };
  }

  /**
   * Tester for getProperty just in case of no exception at all.
   *
   * @param properties Properties to get the one we are looking for
   */
  @Test(dataProvider = "internalPropertiesProviderNotNull")
  public void testGetProperty(Properties properties) {
    instance.setInternalProperties(properties);
    if (properties != null) {
      assertEquals(instance.getProperty("index"), "value");
      assertEquals(instance.getProperty("index-2"), "value-2");
    }
  }

  /**
   * Tester for getProperty just in case of an exception.
   *
   * @param properties Properties to get the one we are looking for
   */
  @Test(
      dataProvider = "internalPropertiesProviderNull",
      expectedExceptions = NullPointerException.class)
  public void testGetPropertyWithException(Properties properties) {
    instance.setInternalProperties(properties);
    // A nullPointerException is expected
    assertEquals(instance.getProperty("index"), "value");
  }

  /**
   * Tester for setProperty method.
   *
   * @param properties Properties to get the one we are looking for
   */
  @Test(dataProvider = "internalPropertiesProviderNotNull")
  public void testSetProperty(Properties properties) {
    instance.setInternalProperties(properties);
    assertEquals(instance.getProperty("index"), "value");
    instance.setProperty("index", "newValue");
    assertEquals(instance.getProperty("index"), "newValue");
  }

  /**
   * Tester for setProperty just in case of an exception.
   *
   * @param properties Properties to get the one we are looking for
   */
  @Test(
      dataProvider = "internalPropertiesProviderNull",
      expectedExceptions = NullPointerException.class)
  public void testSetPropertyWithException(Properties properties) {
    instance.setInternalProperties(properties);
    instance.setProperty("index", "newValue");
    assertEquals(instance.getProperty("index"), "newValue");
  }
}
