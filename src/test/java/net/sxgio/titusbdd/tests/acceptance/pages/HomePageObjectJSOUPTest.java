package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.utils.SharedContent;
import org.apache.commons.lang.NotImplementedException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.ACTIVATING_TEST_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.test.GlobalContent.CLEANING_STATUS_TEXT;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * <strong>HomePageObjectJSOUPTest.java</strong><br>
 * <strong>HomePageObjectJSOUPTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 09/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class HomePageObjectJSOUPTest {
    /** Common logger to know what is happening as the tests are being executed. */
    public static final Logger logger = Logger.getLogger(HomePageObjectJSOUPTest.class.getName());

    /** Instance to test. **/
    private HomePageObjectJSOUP instance;

    /** Setup method. */
    @BeforeMethod()
    public void setUp() throws DriverException {
        logger.info(String.format(ACTIVATING_TEST_TEXT, HomePageObjectJSOUP.class.getSimpleName()));
        instance = new HomePageObjectJSOUP();
    }

    /** Teardown method. */
    @AfterMethod()
    public void tearDown() {
        logger.info(String.format(CLEANING_STATUS_TEXT, HomePageObjectJSOUP.class.getSimpleName()));
        instance = null;
    }

    /** Tester for getDriver method. */
    @Test(expectedExceptions = NotImplementedException.class)
    public void getDriverTest() {
        instance.getDriver();
    }

    /**
     * Tester for loadHomePage method.
     *
     */
    @Test()
    public void loadHomePageTest() {
        GlobalConfigurationEntity globalProperties = Mockito.mock(GlobalConfigurationEntity.class);
        WebDataEntity webDataEntity = Mockito.mock(WebDataEntity.class);
        SharedContent sharedContent = Mockito.mock(SharedContent.class);
        when(webDataEntity.getBaseKeyStorePassword()).thenReturn("");
        when(webDataEntity.getBaseKeyStore()).thenReturn("");
        when(globalProperties.getWebData()).thenReturn(webDataEntity);
        RunMainTest.setGlobalProperties(globalProperties);
        RunMainTest.setSharedContent(sharedContent);
        // Correct URL to access.
        assertTrue(instance.loadHomePage("http://www.sxgio.net"));
        // URL Empty
        assertFalse(instance.loadHomePage(""));
        // Status Exception
        assertFalse(instance.loadHomePage("http://www.sxgio.net/notfound"));
    }

    /**
     * Tester for searchForThisText method.
     */
    @Test()
    public void searchForThisTextTest() {
        Document document = Mockito.mock(Document.class);
        Element element = Mockito.mock(Element.class);
        Elements elements = Mockito.mock(Elements.class);
        // Both isEmpty false
        when(elements.isEmpty()).thenReturn(false);
        when(element.select(anyString())).thenReturn(elements);
        when(document.body()).thenReturn(element);
        when(document.head()).thenReturn(element);
        HomePageObjectJSOUP.setWebDocument(document);
        assertTrue(instance.searchForThisText("test"));
        // Just only the last of elements.isEmpty() is false
        Element elementBody = Mockito.mock(Element.class);
        Elements elementsBody = Mockito.mock(Elements.class);
        when(elementsBody.isEmpty()).thenReturn(true);
        when(elementBody.select(anyString())).thenReturn(elementsBody);
        when(document.body()).thenReturn(elementBody);
        when(document.head()).thenReturn(element);
        HomePageObjectJSOUP.setWebDocument(document);
        assertTrue(instance.searchForThisText("test"));
    }
}
