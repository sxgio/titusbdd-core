package net.sxgio.titusbdd.tests.acceptance.drivers;

import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.EXCEPTION_CAUGHT_AS_EXPECTED_TEXT;
import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.parseContent;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

import io.restassured.RestAssured;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.auth.AuthOAuth2;
import net.sxgio.titusbdd.tests.acceptance.drivers.mocks.RestAssuredMockServer;
import net.sxgio.titusbdd.tests.acceptance.exceptions.RestException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.apache.http.HttpStatus;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockserver.integration.ClientAndServer;
import org.testng.annotations.*;

/**
 * <strong>RestDriverTest.java</strong><br>
 * <strong>RestDriverTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RestDriverTest {

  /** Port value. */
  private static final int PORT = 8888;

  /** Server FQDN. */
  private static final String SERVER = "127.0.0.1";

  /** Server URL. */
  private static final String SERVER_URL = "http://" + SERVER;

  /** Rest-assured Headers mock. */
  @Mock Cookies cookies;

  /** Mockserver. */
  private ClientAndServer mockServer;

  /** Instance to test. */
  private RestDriver instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(RestDriverTest.class.getName());

  /**
   * Process authentication mock.
   *
   * @return AuthOauth2 authentication object
   */
  protected AuthOAuth2 processAuthenticationMock() {
    AuthOAuth2 oauth2 = Mockito.mock(AuthOAuth2.class);
    when(oauth2.auth(any())).thenReturn(Mockito.mock(RequestSpecification.class));
    return oauth2;
  }

  @BeforeClass
  public void setUpBeforeClass() {
    mockServer = ClientAndServer.startClientAndServer(PORT);
    RestAssuredMockServer mockExpectationsForRestClient = new RestAssuredMockServer(SERVER, PORT);
    mockExpectationsForRestClient.createExpectationForDeleteRequest();
    mockExpectationsForRestClient.createExpectationsForGetRequest();
    mockExpectationsForRestClient.createExpectationForOptionsRequest();
    mockExpectationsForRestClient.createExpectationForPostRequest();
    mockExpectationsForRestClient.createExpectationForPutRequest();
    mockExpectationsForRestClient.createExpectationForPatchRequest();
  }

  /** Setup method. */
  @BeforeTest()
  public void setUp() {
    instance = new RestDriver(SERVER_URL, PORT);
    // Initialize mocking system
    MockitoAnnotations.openMocks(this);
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, RestDriverTest.class.getSimpleName()));
  }

  /** Teardown method. */
  @AfterTest()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, RestDriverTest.class.getSimpleName()));
    instance = null;
    RestAssured.baseURI = RestAssured.DEFAULT_URI;
    RestAssured.port = RestAssured.DEFAULT_PORT;
  }

  /** Teardown class. */
  @AfterClass()
  public void tearDownAfterClass() {
    mockServer.stop();
  }
  /**
   * Provider for getter and setters methods
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] getSettersAndGettersProvider() {
    // Params defined
    return new Object[][] {
      // Right params
      new Object[] {"setAuthentication", "getAuthentication", new AuthOAuth2()},
      new Object[] {
        "setAuthenticationParams",
        "getAuthenticationParams",
        new HashMap<String, Object>() {
          {
          }
        }
      },
      new Object[] {"setCookies", "getCookies", new Cookies()},
      new Object[] {"setDeleteStatusCode", "getDeleteStatusCode", 1},
      new Object[] {"setGetStatusCode", "getGetStatusCode", 1},
      new Object[] {"setFollowRedirect", "isFollowRedirect", true},
      new Object[] {"setOptionsStatusCode", "getOptionsStatusCode", 1},
      new Object[] {"setPatchStatusCode", "getPatchStatusCode", 1},
      new Object[] {"setPostStatusCode", "getPostStatusCode", 1},
      new Object[] {"setPutStatusCode", "getPutStatusCode", 1}
    };
  }

  @Test(dataProvider = "getSettersAndGettersProvider")
  public void testGettersAndSetters(
      String setMethodName, String getMethodName, Object methodParameter) {
    Arrays.stream(instance.getClass().getMethods())
        .filter(s -> s.getName().equals(setMethodName))
        .forEach(
            x -> {
              try {
                x.invoke(instance, methodParameter);
                assertNotNull(instance);
                logger.info("Method " + x.getName() + " called properly");
              } catch (IllegalAccessException
                  | IllegalArgumentException
                  | InvocationTargetException e) {
                final String message = "Exception raised with no control at all: " + e.getMessage();
                logger.severe(message);
                fail(message);
              }
            });

    Arrays.stream(instance.getClass().getMethods())
        .filter(s -> s.getName().equals(getMethodName))
        .forEach(
            x -> {
              try {
                Object retval = x.invoke(instance);
                assertNotNull(retval);
                assertEquals(retval, methodParameter);
                logger.info("Method " + x.getName() + " called properly");
                logger.info(
                    "Parameter/s passed, "
                        + methodParameter
                        + ", and obtained value/s, "
                        + retval
                        + " are as expected");
              } catch (IllegalAccessException
                  | IllegalArgumentException
                  | InvocationTargetException e) {
                final String message = "Exception raised with no control at all: " + e.getMessage();
                logger.severe(message);
                fail(message);
              }
            });
  }

  /**
   * Provider for connect method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] resourceOnlyTestProvider() {
    return new Object[][] {
      // Right resources
      new Object[] {"/"},
    };
  }

  /**
   * Provider for connect method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] wrongResourceOnlyTestProvider() {
    return new Object[][] {
      // Wrong arguments
      new Object[] {null}, new Object[] {"/not-available-for-sure"}
    };
  }

  /** Tester for delete method. */
  @Test()
  public void authenticateTest() {
    instance.setAuthentication(processAuthenticationMock());
    assertNotNull(instance.authenticate(new HashMap<>()));
  }

  /** Tester for delete method. */
  @Test()
  public void deleteTest() {
    instance.setDeleteStatusCode(HttpStatus.SC_ACCEPTED);
    Response response = instance.delete("delete-resource");
    assertTrue(response.getBody().asString().contains("deleted"));
  }

  /**
   * Tester for connect method.
   *
   * @param resource       Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(
      expectedExceptions = {RestException.class},
      dataProvider = "resourceOnlyTestProvider")
  public void connectTest(String resource) throws RestException {
    instance.connect(resource);
  }

  /**
   * Provider for get method..
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] getTestProvider() {
    // Params
    HashMap<String, Object> params = new HashMap<>();
    params.put("foo", "bar");
    params.put("useless", "sure");
    // Setting up headers
    Headers headers = new Headers();
    return new Object[][] {
      // Right resources
      new Object[] {"/get-resource", headers, params}
    };
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   */
  @Test(dataProvider = "resourceOnlyTestProvider")
  public void getWithOnlyResourceTest(String resource) {
    instance.setGetStatusCode(HttpStatus.SC_OK);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(resource);
    assertNotNull(response);
    assertEquals(response.getStatusCode(), HttpStatus.SC_OK);
    assertTrue(response.getBody().asString().contains("foo"));
    assertTrue(response.getBody().asString().contains("selected"));
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   */
  @Test(dataProvider = "wrongResourceOnlyTestProvider")
  public void getWrongTest(String resource) {
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = null;
    if (Objects.isNull(resource)) {
      instance.setGetStatusCode(HttpStatus.SC_OK);
      try {
        response = instance.get(resource);
      } catch (IllegalArgumentException e) {
        logger.info(parseContent(EXCEPTION_CAUGHT_AS_EXPECTED_TEXT, e.getMessage()));
      } finally {
        // Exception expected here and the following line should never be read
        assertNull(response);
      }
    } else {
      instance.setGetStatusCode(HttpStatus.SC_NOT_FOUND);
      response = instance.get(resource);
      assertEquals(response.getStatusCode(), instance.getStatusCode);
    }
  }

  /** Tester for get method. */
  @Test()
  public void getWithHeadersTest() {
    instance.setGetStatusCode(HttpStatus.SC_OK);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response =
        instance.get(
            "/get-resource", Headers.headers(new Header("Content-Type", "application/json")));
    assertTrue(response.getBody().asString().contains("selected"));
    assertTrue(response.getBody().asString().contains("foo"));
  }

  /** Tester for get method. */
  @Test()
  public void getWithParametersTest() {
    instance.setGetStatusCode(HttpStatus.SC_OK);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response =
        instance.get(
            "/get-resource",
            new HashMap<>() {
              {
                put("foo", "bar");
                put("useless", "sure");
              }
            });
    assertTrue(response.getBody().asString().contains("selected"));
    assertTrue(response.getBody().asString().contains("foo"));
  }

  /**
   * Tester for get method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers URL headers if needed
   * @param params URL params if needed
   */
  @Test(dataProvider = "getTestProvider")
  public void getWithHeadersAndParametersTest(
      String resource, Headers headers, Map<String, ?> params) {
    instance.setGetStatusCode(HttpStatus.SC_OK);
    instance.setCookies(cookies);
    instance.setAuthentication(null);
    Response response = instance.get(resource, headers, params);
    assertTrue(response.getBody().asString().contains("selected"));
    assertTrue(response.getBody().asString().contains("foo"));
  }

  /**
   * Tester for head method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(expectedExceptions = RestException.class, dataProvider = "resourceOnlyTestProvider")
  public void headTest(String resource) throws RestException {
    instance.head(resource);
  }

  /**
   * Tester for options method.
   *
   * @param resource Web resource to be linked to the baseURI
   */
  @Test(dataProvider = "resourceOnlyTestProvider")
  public void optionsTest(String resource) {
    instance.options(resource);
    assertEquals(instance.optionsStatusCode, HttpStatus.SC_OK);
    instance.options();
    assertEquals(instance.optionsStatusCode, HttpStatus.SC_OK);
    instance.options();
  }

  /** Tester for patch method. */
  @Test()
  public void patchWithResourceTest() {
    Response response = instance.patch("/patch-resource");
    assertTrue(response.getBody().asString().contains("patched"));
  }

  /** Tester for patch method. */
  @Test()
  public void patchWithResourceAndPayloadTest() {
    Response response = instance.patch("/patch-resource", "{'username': 'foo', 'password': 'bar'}");
    assertTrue(response.getBody().asString().contains("patched"));
  }

  /** Tester for patch method. */
  @Test()
  public void patchWithHeadersAndResourceTest() {
    Response response =
        instance.patch(
            "/patch-resource", Headers.headers(new Header("Content-type", "application/json")));
    assertTrue(response.getBody().asString().contains("patched"));
  }

  /** Tester for patch method- */
  @Test()
  public void patchWithHeadersAndResourceAndPayloadTest() {
    Response response =
        instance.patch(
            "/patch-resource",
            Headers.headers(new Header("accept", "application/json")),
            "{'username': 'foo', 'password': 'bar'}");
    assertTrue(response.getBody().asString().contains("patched"));
  }

  /** Tester for post method. */
  @Test()
  public void postWithResourceTest() {
    Response response = instance.post("/post-resource");
    assertTrue(response.getBody().asString().contains("created"));
  }

  /** Tester for post method. */
  @Test()
  public void postWithHeadersAndResourceTest() {
    Response response =
        instance.post(
            "/post-resource", Headers.headers(new Header("Content-Type", "application/json")));
    assertTrue(response.getBody().asString().contains("created"));
  }

  /** Tester for post method. */
  @Test()
  public void postWithResourceAndParamsTest() {
    Response response =
        instance.post(
            "/post-resource",
            new HashMap<>() {
              {
                put("username", "foo");
                put("password", "bar");
              }
            });
    assertTrue(response.getBody().asString().contains("created"));
  }

  /** Tester for post method. */
  @Test()
  public void postWithHeadersAndResourceAndParamsTest() {
    Response response =
        instance.post(
            "/post-resource",
            Headers.headers(new Header("Content-type", "application/json")),
            new HashMap<>() {
              {
                put("username", "foo");
                put("password", "bar");
              }
            });
    assertTrue(response.getBody().asString().contains("created"));
  }

  /** Tester for post method. */
  @Test()
  public void postWithHeadersAndResourceAndPayloadTest() {
    Response response =
        instance.post(
            "/post-resource",
            Headers.headers(new Header("Content-type", "application/json")),
            "{'username': 'foo', 'password': 'bar'}");
    assertTrue(response.getBody().asString().contains("created"));
  }

  /** Tester for put method. */
  @Test()
  public void putWithResourceTest() {
    Response response = instance.put("/put-resource");
    assertTrue(response.getBody().asString().contains("modified"));
  }

  /** Tester for put method. */
  @Test()
  public void putWithResourceAndPayloadTest() {
    Response response = instance.put("/put-resource", "{'username': 'foo', 'password': 'bar'}");
    assertTrue(response.getBody().asString().contains("modified"));
  }

  /** Tester for put method. */
  @Test()
  public void putWithHeadersAndResourceTest() {
    Response response =
        instance.put(
            "/put-resource", Headers.headers(new Header("Content-type", "application/json")));
    assertTrue(response.getBody().asString().contains("modified"));
  }

  /** Tester for put method- */
  @Test()
  public void putWithHeadersAndResourceAndPayloadTest() {
    Response response =
        instance.put(
            "/put-resource",
            Headers.headers(new Header("accept", "application/json")),
            "{'username': 'foo', 'password': 'bar'}");
    assertTrue(response.getBody().asString().contains("modified"));
  }

  /**
   * Tester for trace method.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException An error ocurred while connecting
   */
  @Test(expectedExceptions = RestException.class, dataProvider = "resourceOnlyTestProvider")
  public void traceTest(String resource) throws RestException {
    instance.trace(resource);
  }
}
