package net.sxgio.titusbdd.tests.acceptance.auth;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertNotNull;

/**
 * <strong>AuthDigestTest.java</strong><br>
 * <strong>AuthDigestTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class AuthDigestTest {
  /** Instance to test. */
  private AuthDigest instance;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(AuthDigestTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    instance = new AuthDigest();
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, AuthDigestTest.class.getSimpleName()));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, AuthDigestTest.class.getSimpleName()));
    instance = null;
  }

  /**
   * language Provider for init method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] authProvider() {
    return new Object[][] {
      // Right arguments
      new Object[] {
        Stream.of(
                new Object[][] {
                  {"userName", "test"}, {"password", "test"},
                })
            .collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]))
      }
    };
  }

  /**
   * language Provider for init method.
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] authWrongDataProvider() {
    return new Object[][] {
      // Wrong arguments
      new Object[] {
        Stream.of(
                new Object[][] {
                  {"username", "test"}, {"password", "test"},
                })
            .collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]))
      }
    };
  }

  /**
   * Tester for auth method.
   *
   * @param params HashMap with specific params to the auth method
   */
  @Test(dataProvider = "authProvider")
  public void authTest(HashMap<String, Object> params) {
    assertNotNull(instance.auth(params));
  }

  /**
   * Tester for auth method with wrong data.
   *
   * @param params HashMap with specific params to the auth method
   */
  @Test(dataProvider = "authWrongDataProvider", expectedExceptions = IllegalArgumentException.class)
  public void authWrongInputTest(HashMap<String, Object> params) {
    assertNotNull(instance.auth(params));
  }
}
