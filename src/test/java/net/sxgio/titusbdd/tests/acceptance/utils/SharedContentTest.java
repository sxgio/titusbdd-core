package net.sxgio.titusbdd.tests.acceptance.utils;

import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

/**
 * <strong>SharedContentTest.java</strong><br>
 * <strong>SharedContentTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class SharedContentTest
{
    /**
     * Instance to test.
     */
    private SharedContent instance;

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(SharedContentTest.class.getName());

    /**
     * Setup method
     */
    @BeforeTest
    public void setUp() {
        instance = new SharedContent();
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
    }

    /**
     * Teardown method
     */
    @AfterTest
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
        // Shutting down instance
        instance = null;
    }

    /**
     * Provider for sharedContent getter and setter
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] sharedContentProvider() {
        return new Object[][] {
                // Right arguments
                new Object[] {1},
                new Object[] {4},
                new Object[] {-1234}
        };
    }

    /**
     * Tester for sharedContent setter and getter methods
     *
     * @param value value to set or get either
     */
    @Test(dataProvider = "sharedContentProvider")
    public void testSetAndGetSharedContent(int value) {
        instance.setStatusCode(value);
        assertEquals(instance.getStatusCode(), value);
    }

    /**
     * Provider for sharedContent getter and setter
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] sharedDynamicContentProvider() {
        return new Object[][] {
                // Right arguments
                new Object[] {"key1", "test1"},
                new Object[] {"key2", new Integer(12)},
                new Object[] {"key3", new Object()}
        };
    }

    /**
     * Tester for dynamicContent setter and getter methods
     *
     * @param key key to set or get either
     * @param value value to set or get either
     *
     */
    @Test(dataProvider = "sharedDynamicContentProvider")
    public void testSetAndGetDynamicContent(String key, Object value) {
        HashMap<String, Object> dynamicContent = new HashMap<>();
        dynamicContent.put(key, value);
        instance.setDynamicContent(dynamicContent);
        assertEquals(instance.getDynamicContent(), dynamicContent);
    }
}
