package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.entities.WebDataEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.AssertJUnit.assertEquals;

/**
 * <strong>RemoteBrowserConfigurationTest.java</strong><br>
 * <strong>RemoteBrowserConfigurationTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class RemoteBrowserConfigurationTest {

  /** Instance to test. */
  private RemoteBrowserConfiguration instance;

  /** Common logger to know what is happening as the tests are being executed */
  public static final Logger logger =
      Logger.getLogger(RemoteBrowserConfigurationTest.class.getName());

  /** QaProperties Mock. */
  @Mock
  GlobalConfigurationEntity globalProperties;

  /** Web Data Entity. */
  @Mock
  WebDataEntity webDataEntity;

  /** Setup method */
  @BeforeMethod()
  public void setUp() {
    instance = new RemoteBrowserConfiguration();
    logger.info(
        String.format(
            GlobalContent.ACTIVATING_TEST_TEXT, RemoteBrowserConfiguration.class.getSimpleName()));
    webDataEntity  = new WebDataEntity();
    globalProperties = new GlobalConfigurationEntity();
    globalProperties.setWebData(webDataEntity);
  }

  /** Teardown method */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(
            GlobalContent.CLEANING_STATUS_TEXT, RemoteBrowserConfiguration.class.getSimpleName()));
    instance = null;
    globalProperties = null;
  }

  /** Tester for getCapabilities method */
  @Test()
  public void testGetCapabilities() {
    globalProperties.getWebData().setDriverUrl("http://localhost:4444/wd/hub");
    globalProperties.getWebData().setDriverName(
        "net.sxgio.titusbdd.tests.acceptance.drivers.RemoteBrowserConfiguration");
    globalProperties.getWebData().setDriverRemote(
        "net.sxgio.titusbdd.tests.acceptance.drivers.ChromeBrowserConfiguration");
    globalProperties.getWebData().setAppiumTest(false);
    instance.setGlobalProperties(globalProperties);
    instance.init();
    assertNotNull(instance.getCapabilities());
  }

  /** Tester for getDriver method */
  @Test()
  public void testGetDriver() {
    instance.setGlobalProperties(globalProperties);
    RemoteWebDriver test = (RemoteWebDriver) instance.getDriver();
    assertNull(test);
  }

  /**
   * Tester for getAppiumDriver method
   *
   * @throws DriverException Just in case an appium error while connecting
   */
  @Test()
  public void testGetAppiumDriver() throws DriverException {
    globalProperties.getWebData().setDriverUrl("hukf://wrongurl");
    globalProperties.getWebData().setDriverRemote(
        "net.sxgio.titusbdd.tests.acceptance.drivers.ChromeBrowserConfiguration");
    globalProperties.getWebData().setAppiumTest(false);
    instance.setGlobalProperties(globalProperties);
    AppiumDriver test = null;
    test = instance.getAppiumDriver();
    assertNull(test);
  }

  /**
   * initProvider
   *
   * @return Object[][]
   */
  @DataProvider
  public Object[][] initProvider() {
    return new Object[][] {
      // Right arguments
      new Object[] {"net.sxgio.titusbdd.tests.acceptance.drivers.ChromeBrowserConfiguration"},
      // Exception raised but firefox browser configuration created by default
      new Object[] {"net.sxgio.titusbdd.tests.acceptance.drivers.mocks.WrongBrowserMock"},
    };
  }

  /**
   * Tester for init method
   *
   * @param driverRemote Class of the remote driver to be used
   */
  @Test(dataProvider = "initProvider")
  public void testInit(String driverRemote) {
    globalProperties.getWebData().setDriverUrl("http://localhost:4444/wd/hub");
    globalProperties.getWebData().setDriverName(
        "net.sxgio.titusbdd.tests.acceptance.drivers.RemoteBrowserConfiguration");
    globalProperties.getWebData().setDriverRemote(driverRemote);
    globalProperties.getWebData().setAppiumTest(false);
    globalProperties.getWebData().setBrowserCapabilities("{\"foo\":\"true\"}");
    instance.setGlobalProperties(globalProperties);
    instance.init();
    assertEquals(
        instance.getBrowserConfiguration().getCapabilities().toString(),
        instance.getCapabilities().toString());
  }

  /** Tester for get and set firefoxDriverInstance property */
  @Test()
  public void testSetAndGetBrowserConfiguration() {
    instance.setBrowserConfiguration(null);
    assertNull(instance.getBrowserConfiguration());
  }
}
