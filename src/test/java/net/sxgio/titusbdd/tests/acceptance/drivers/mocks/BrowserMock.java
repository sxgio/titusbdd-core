package net.sxgio.titusbdd.tests.acceptance.drivers.mocks;

import net.sxgio.titusbdd.tests.acceptance.drivers.BrowserInterface;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.mockito.Mockito;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

/**
 * <strong>BrowserMock.java</strong><br>
 * <strong>BrowserMock class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class BrowserMock implements BrowserInterface {

  /** Public constructor */
  public BrowserMock() {
    // do nothing.
  }

  @Override
  public Capabilities getCapabilities() {
    return null;
  }

  @Override
  public WebDriver getDriver() throws DriverException {
    return Mockito.mock(WebDriver.class);
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public void setGlobalProperties(GlobalConfigurationEntity globalProperties) {
    // Do nothing
  }
}
