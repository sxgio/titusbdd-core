package net.sxgio.titusbdd.tests.acceptance.helpers.appium.mocks;

import net.sxgio.titusbdd.tests.acceptance.helpers.appium.Swipeable;

/**
 * <strong>SwipeableTest.java</strong><br>
 * <strong>SwipeableTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class SwipeableImpl implements Swipeable {}
