package net.sxgio.titusbdd.tests.acceptance.features.mocks;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import org.testng.*;
import org.testng.xml.XmlTest;

/**
 * <strong>TestContextImpl.java</strong><br>
 * <strong>TestContextImpl class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public class TestContextImpl implements ITestContext {
  @Override
  public String getName() {
    return null;
  }

  @Override
  public Date getStartDate() {
    return null;
  }

  @Override
  public Date getEndDate() {
    return null;
  }

  @Override
  public IResultMap getPassedTests() {
    return null;
  }

  @Override
  public IResultMap getSkippedTests() {
    return null;
  }

  @Override
  public IResultMap getFailedButWithinSuccessPercentageTests() {
    return null;
  }

  @Override
  public IResultMap getFailedTests() {
    return null;
  }

  @Override
  public String[] getIncludedGroups() {
    return new String[0];
  }

  @Override
  public String[] getExcludedGroups() {
    return new String[0];
  }

  @Override
  public String getOutputDirectory() {
    return null;
  }

  @Override
  public ISuite getSuite() {
    return null;
  }

  @Override
  public ITestNGMethod[] getAllTestMethods() {
    return new ITestNGMethod[0];
  }

  @Override
  public String getHost() {
    return null;
  }

  @Override
  public Collection<ITestNGMethod> getExcludedMethods() {
    return null;
  }

  @Override
  public IResultMap getPassedConfigurations() {
    return null;
  }

  @Override
  public IResultMap getSkippedConfigurations() {
    return null;
  }

  @Override
  public IResultMap getFailedConfigurations() {
    return null;
  }

  @Override
  public XmlTest getCurrentXmlTest() {
    return new XmlTest();
  }

  @Override
  public Object getAttribute(String s) {
    return null;
  }

  @Override
  public void setAttribute(String s, Object o) {}

  @Override
  public Set<String> getAttributeNames() {
    return null;
  }

  @Override
  public Object removeAttribute(String s) {
    return null;
  }
}
