package net.sxgio.titusbdd.tests.acceptance.helpers.appium;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.logging.Logger;

import net.sxgio.titusbdd.tests.acceptance.helpers.appium.mocks.SwipeableImpl;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.AccessibleTest;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.mocks.WebDriverImpl;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * <strong>SwipeableTest.java</strong><br>
 * <strong>SwipeableTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups = {"units"})
public class SwipeableTest {

  /** Instance to test. */
  private SwipeableImpl instance;

  /** Webdriver Mock. */
  @Mock WebDriverImpl webDriver;

  /** Common logger to know what is happening as the tests are being executed. */
  public static final Logger logger = Logger.getLogger(SwipeableTest.class.getName());

  /** Setup method. */
  @BeforeMethod()
  public void setUp() {
    logger.info(
        String.format(GlobalContent.ACTIVATING_TEST_TEXT, AccessibleTest.class.getSimpleName()));
    instance = new SwipeableImpl();
    webDriver = Mockito.mock(WebDriverImpl.class);
    when(webDriver.findElement(any())).thenReturn(Mockito.mock(WebElement.class));
  }

  /** Teardown method. */
  @AfterMethod()
  public void tearDown() {
    logger.info(
        String.format(GlobalContent.CLEANING_STATUS_TEXT, AccessibleTest.class.getSimpleName()));
    instance = null;
    webDriver = null;
  }

  /** Tester for swipeNext method. * */
  @Test()
  public void testSwipeNext() {
    assertTrue(instance.swipeNext("test", webDriver));
    when(webDriver.executeScript(isA(String.class), isA(String.class))).thenReturn(true);
    assertFalse(instance.swipeNext("test", webDriver));
  }

  /** Tester for swipePrev method. * */
  @Test()
  public void testSwipePrev() {
    assertTrue(instance.swipePrev("test", webDriver));
    when(webDriver.executeScript(isA(String.class), isA(String.class))).thenReturn(true);
    assertFalse(instance.swipePrev("test", webDriver));
  }
}
