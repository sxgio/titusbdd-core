package net.sxgio.titusbdd.tests.acceptance.db;

import net.sxgio.titusbdd.tests.acceptance.exceptions.NitriteDbException;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.dizitart.no2.Nitrite;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Objects;
import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * <strong>EmbeddedNitriteDbServerTest.java</strong><br>
 * <strong>EmbeddedNitriteDbServerTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0 08/07/2023
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class EmbeddedNitriteDbServerTest {

    /**
     * Instance to test.
     */
    private EmbeddedNitriteDbServer instance;

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(EmbeddedNitriteDbServerTest.class.getName());

    /**
     * NitriteDB Exception text
     */
    public static final String NITRITEDB_EXCEPTION_RAISED_TEXT = "NitriteDb exception raised in %s";

    /**
     * NitriteDB Server Mock
     */
    @Mock
    Nitrite serverMock;

    /**
     * Setup method
     */
    @BeforeTest()
    public void setUp() {
        // Initialize mocking system
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Teardown method
     */
    @AfterTest()
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, instance.getClass().getSimpleName()));
        instance = null;
    }

    /**
     * Provider with info to read some elements stored in the app.
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] startProvider() {
        // Getting driver file
        final String configFile = "src/test/resources/config/nitrite.db";
        return new Object[][] {
            // Wrong arguments
            new Object[] {null, configFile, false},
            new Object[] {null, null, false},
            // Right arguments
            new Object[] { serverMock, configFile, true}
        };
    }

    /**
     * Provider with info to read some elements stored in the app.
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] constructorByParametersProvider() {
        // Getting driver file
        final String configFile = "target/nitrite-test-1.db";
        return new Object[][] {
            // Right arguments
            new Object[] {configFile, "admin", "admin"}
        };
    }

    /**
     * Tester for a constructor with parameters.
     *
     * @param configFile file with the configuration
     * @param user       User name
     * @param password   User's password
     *
     */
    @Test(dataProvider = "constructorByParametersProvider", dependsOnMethods = {"testStart"})
    public void testConstructorByParameters(String configFile, String user, String password) {
        if (!Objects.isNull(instance)) {
            instance.shutdown();
        }
        instance = new EmbeddedNitriteDbServer(configFile, user, password);
        assertFalse(instance.getServer().isClosed());
        instance.shutdown();
        assertTrue(instance.getServer().isClosed());
    }

    /**
     * Tester for start method
     *
     * @param serverMock        mocking for server entrance
     * @param configFile        file with the configuration
     * @param serverFinalStatus server final status
     *
     */
    @Test(dataProvider = "startProvider")
    public void testStart(Nitrite serverMock, String configFile, boolean serverFinalStatus) {
        try {
            instance = new EmbeddedNitriteDbServer(serverMock);
            logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
            instance.start(configFile, "admin", "admin");
            if (serverFinalStatus) {
                assertNotNull(instance.getServer());
            } else {
                assertNull(instance.getServer());
            }
        } catch (NitriteDbException e) {
            logger.warning(
                String.format(
                    NITRITEDB_EXCEPTION_RAISED_TEXT,
                    EmbeddedNitriteDbServerTest.class.getSimpleName()
                )
            );
            assertNull(instance);
        } catch (Exception e) {
            logger.warning(EmbeddedNitriteDbServer.NITRITE_UNKNOWN_ERROR_TEXT);
            assertNull(instance.getServer());
        }
    }

    /**
     * Tester for shutdown methods
     */
    @Test(dependsOnMethods = {"testStart"})
    public void testShutdown() {
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, instance.getClass().getSimpleName()));
        instance.shutdown();
        assertTrue(instance.getServer().isClosed());
    }
}
