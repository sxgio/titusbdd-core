package net.sxgio.titusbdd.tests.acceptance.di;

import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.test.GlobalContent;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * <strong>CustomPicoFactoryTest.java</strong><br>
 * <strong>CustomPicoFactoryTest class</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Test(groups={"units"})
public class CustomPicoFactoryTest
{
    /**
     * Adding class text
     */
    public static final String ADDING_CLASS_TEXT = "Adding class to the pico factory %s";

    /**
     * Instance to test.
     */
    private CustomPicoFactory instance;

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    public static final Logger logger = Logger.getLogger(CustomPicoFactoryTest.class.getName());

    /**
     * Verifying class text
     */
    public static final String VERIFYING_CLASS_TEXT = "Verifying class %s is into factory";

    /**
     * Provider with info to read some elements stored in the app. 
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] classProvider() {
        return new Object[][]{            
            // The next three are correct
            new Object[] {GlobalConfigurationEntity.class, true},
            //new Object[] {EmbeddedOrientDbServer.class, false},
            new Object[] {null, false},
        };
    }

    /**
     * Setup method
     */
    @BeforeTest
    public void setUp() {
        instance = new CustomPicoFactory();
        logger.info(String.format(GlobalContent.ACTIVATING_TEST_TEXT, CustomPicoFactory.class.getSimpleName()));
    }

    /**
     * Teardown method
     */
    @AfterTest
    public void tearDown() {
        logger.info(String.format(GlobalContent.CLEANING_STATUS_TEXT, CustomPicoFactory.class.getSimpleName()));
        instance = null;
    }

    /**
     * Add a class to the factory container for dependency injection
     * 
     * @param clazz    Class to get new instances from.
     * @param expected Value expected
     *      
     */
    @Test(dataProvider = "classProvider")
    public void testAddClass(Class<?> clazz, boolean expected) {
        if (clazz != null) {
            logger.info(String.format(ADDING_CLASS_TEXT, clazz.getClass()));
        }
        assertEquals(instance.addClass(clazz), expected);
        assertTrue(instance.getClasses().size()>0);
    }

    /**
     * Get a new object provided by the factory based on the 
     * class type.
     * 
     * @param clazz    Class to get new instances from.
     * @param expected Value expected
     */
    @Test(dataProvider = "classProvider")
    public void testGetInstance(Class<?> clazz, boolean expected) {
        instance.setClasses(new HashSet<>());
        if (clazz != null) {
            logger.info(String.format(ADDING_CLASS_TEXT, clazz.getSimpleName()));
        }
        assertEquals(instance.addClass(clazz), expected);
        if (clazz != null) {
            logger.info(String.format(VERIFYING_CLASS_TEXT, clazz.getSimpleName()));
        }
        instance.start();
        if (expected) {
            assertNotNull(instance.getInstance(clazz));
        }
        instance.stop();
    }

    /**
     * Tester for getter and setter of pico property
     */
    @Test()
    public void testSetAndGetPico() {
        instance.setPico(null);
        assertNull(instance.getPico());
    }
}
