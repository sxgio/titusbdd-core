package net.sxgio.titusbdd.tests.acceptance.utils;

import net.sxgio.titusbdd.tests.acceptance.di.CustomPicoFactory;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.logging.Logger;

/**
 * <strong>ClassFeeder.java</strong><br>
 * <strong>ClassFeeder class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class ClassFeeder {

    /**
     * Initializing DI text
     */
    public static final String INITIALIZING_DI_TEXT = "Initializing DI to access some classes not matter what";

    /**
     * Common logger to know what is happening along the test
     */
    private static final Logger logger = Logger.getLogger(ClassFeeder.class.getName());

    /**
     * Dependency Injection just in case of needed for some objects.
     */
    @Getter
    @Setter
    protected CustomPicoFactory factory = new CustomPicoFactory();

    /**
     * Add new classes to the factory
     *
     * @param classes Set of classes to add to the factory
     */
    public void addClasses(Set<Class<?>> classes) {
        // Check Factory is not empty
        if (!factory.getClasses().isEmpty()) {
            // Getting classes already added to the IOC
            classes.addAll(factory.getClasses());
        }
        initFactory(classes);
    }

    /**
     * InitFactory with a bunch of classes
     *
     * @param classes Set of classes to ramp up the factory
     */
    public void initFactory(Set<Class<?>> classes) {
        // Check Factory is not empty
        if (!factory.getClasses().isEmpty()) {
            // Clean up the factory
            factory.stop();
        }
        // Population of some classes as needed in most of the tests
        logger.info(INITIALIZING_DI_TEXT);
        // Factory init classes
        classes.forEach(x -> factory.addClass(x));
        factory.start();
    }

    /**
     * Pop some classes from factory
     *
     * @param classes Set of classes to be excluded from factory
     */
    public void popClasses(Set<Class<?>> classes) {
        Set<Class<?>> oldClasses = factory.getClasses();
        if (!oldClasses.isEmpty()) {
            oldClasses.removeAll(classes);
        }
        initFactory(oldClasses);
    }
}
