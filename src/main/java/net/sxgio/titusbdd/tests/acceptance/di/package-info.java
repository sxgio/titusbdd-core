/**
 * Provide the necessary objects to use a IOC pattern.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.di;
