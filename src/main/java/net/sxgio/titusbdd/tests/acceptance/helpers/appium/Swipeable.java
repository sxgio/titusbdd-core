package net.sxgio.titusbdd.tests.acceptance.helpers.appium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 * <strong>Swipeable.java</strong><br>
 * <strong>Swipeable class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public interface Swipeable {

    /**
     * This method swipes to the next element using JavascriptExecutor.
     *
     * @param swiperContainerId Swiper Container defined in the code
     * @param driver            Selenium WebDriver being used
     * @return boolean true if is able to swipe, false otherwise.
     */
    default boolean swipeNext(String swiperContainerId, WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        String scriptInit =
            "var mySwiper = document.querySelector('" + swiperContainerId + "').swiper ; ";
        String scriptNext = scriptInit + "mySwiper.slideNext();";
        String scriptIsEnd = scriptInit + "return mySwiper.isEnd;";

        Object isLastElemObject = jse.executeScript(scriptIsEnd, "");
        boolean isLastElem = Boolean.TRUE.equals(isLastElemObject);
        if (!isLastElem) {
            jse.executeScript(scriptNext, "");
        }
        return (!isLastElem);
    }

    /**
     * This method swipes to the prev element using JavascriptExecutor.
     *
     * @param swiperContainerId Swiper Container defined in the code
     * @param driver            Selenium WebDriver being used
     * @return boolean true if is able to swipe, false otherwise.
     */
    default boolean swipePrev(String swiperContainerId, WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        String scriptInit =
            "var mySwiper = document.querySelector('" + swiperContainerId + "').swiper ; ";
        String scriptPrev = scriptInit + "mySwiper.slidePrev();";
        String scriptIsBeginning = scriptInit + "return mySwiper.isBeginning;";

        Object isFirstElemObject = jse.executeScript(scriptIsBeginning, "");
        boolean isFirstElem = Boolean.TRUE.equals(isFirstElemObject);
        if (!isFirstElem) {
            jse.executeScript(scriptPrev, "");
        }
        return (!isFirstElem);
    }
}
