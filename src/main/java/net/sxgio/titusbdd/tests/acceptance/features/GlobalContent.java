package net.sxgio.titusbdd.tests.acceptance.features;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * <strong>GlobalContent.java</strong><br>
 * <strong>GlobalContent class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public final class GlobalContent {

  /** Exception caught as expected text. */
  public static final String EXCEPTION_CAUGHT_AS_EXPECTED_TEXT = "Exception raised but caught as expected. %s";

  /** Exception caught and not expected. */
  public static final String EXCEPTION_CAUGHT_AND_NOT_EXPECTED_TEXT = "Exception raised not expected. %s";

  /** General timeout in seconds. */
  public static final int EXTENDED_TIMEOUT_IN_SECONDS = 20;

  /** General timeout in seconds. */
  public static final int GENERAL_TIMEOUT_IN_SECONDS = 10;

  /** MIME Image by default choosen when screenshots are taken. */
  public static final String IMAGE_MIME_BY_DEFAULT_TEXT = "image/png";

  /** Format name from image by default choosen when screenshots are taken. */
  public static final String IMAGE_FORMAT_NAME_BY_DEFAULT_TEXT = "PNG";

  /** Error info just in case capabilities cannot be read from configuration file. */
  public static final String JSON_CAPABILITIES_ERROR_TEXT = "Capabilities cannot be read from json variable";

  /** Error marked as method currently not implemented. */
  public static final String NOT_IMPLEMENTED_YET_ERROR = "Not implemented yet";

  /** 404 path to test. */
  public static final String PATH_404 = "/error";

  /** Uncontrolled error happened with the browser. Text error. */
  public static final String UNKNOWN_BROWSER_ERROR = "Unknown error found in the code";

  /** Autocomplete field selector to access different fields in most of the tests. */
  public static final String XPATH_AUTOCOMPLETE_FIELD_DIV = "//div[@class=\"ui-menu-item-wrapper\" and text()=\"%s\"]";

  /** General XPATH for common buttons. */
  public static final String XPATH_BUTTON = "//button[@id=\"%s\" or @name=\"%s\" or contains(text(), \"%s\")]";

  /** General field selector to access different fields in most of the tests. */
  public static final String XPATH_GENERAL_FIELD_SELECTOR = "//%s[@name=\"%s\" or @id=\"%s\" or contains(text(), \"%s\")]";

  /** General field selector to access different fields in most of the tests. */
  public static final String XPATH_GENERAL_FIELD_SELECTOR_BY_TEXT = "//%s[contains(text(),\"%s\")]";

  /** General XPATH for iframes. */
  public static final String XPATH_IFRAME_IDENTIFIER = "//iframe";

  /** Constructor by default. Sonarqube forces us to define it. */
  private GlobalContent() {
    super();
  }

  /**
   * Parsing the content to get a clear message.
   *
   * @param toBeFormated String to be formatted
   * @param actors Object to be used to format the string
   * @return String Passed string correctly formatted
   */
  public static String parseContent(String toBeFormated, Object... actors) {
    return String.format(toBeFormated, actors);
  }

  /**
   * Stack Trace as String to fill up reports.
   *
   * @param exception Throwable to control what happened
   * @return String Stack to print wherever needed
   */
  public static String printFullStack(Throwable exception) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    exception.printStackTrace(pw);
    return sw.toString();
  }
}
