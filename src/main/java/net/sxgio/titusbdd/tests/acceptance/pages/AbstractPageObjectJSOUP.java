package net.sxgio.titusbdd.tests.acceptance.pages;

import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import org.jsoup.nodes.Document;

import java.io.File;
import java.util.logging.Logger;

/**
 * <strong>AbstractPageObjectJSOUP.java</strong><br>
 * <strong>AbstractPageObjectJSOUP class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public abstract class AbstractPageObjectJSOUP {
    /**
     * Web Document as requested by JSOUP
     */
    @Getter
    @Setter
    protected static Document webDocument;

    /**
     * Common logger to know what is happening along the test
     */
    protected final Logger logger = Logger.getLogger(AbstractPageObjectJSOUP.class.getName());

    /**
     * Checking SSL Certification
     */
    protected void checkSSLCertification() {
        if (RunMainTest.getGlobalProperties().getWebData().getBaseKeyStore() != null
            && (new File(RunMainTest.getGlobalProperties().getWebData().getBaseKeyStore())).isFile()) {
            // Get keystore for resource area
            ClassLoader classLoader = getClass().getClassLoader();
            // Setting Properties to access data through SSL.
            System.setProperty(
                "javax.net.ssl.keyStore",
                classLoader.getResource(RunMainTest.getGlobalProperties().getWebData().getBaseKeyStore()).getPath());
            System.setProperty(
                "javax.net.ssl.keyStorePassword",
                RunMainTest.getGlobalProperties().getWebData().getBaseKeyStorePassword());
        }
    }
}