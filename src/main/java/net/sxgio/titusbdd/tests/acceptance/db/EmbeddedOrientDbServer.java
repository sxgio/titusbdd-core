package net.sxgio.titusbdd.tests.acceptance.db;

import com.orientechnologies.orient.server.OServer;
import lombok.Getter;
import net.sxgio.titusbdd.tests.acceptance.exceptions.OrientDbException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <strong>EmbeddedOrientDbServer.java</strong><br>
 * <strong>EmbeddedOrientDbServer class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public final class EmbeddedOrientDbServer
{
    /**
     * Orientdb is starting. Text Message
     */
    public static final String INITIALIZING_ORIENTDB_TEXT = "Starting up Orientdb server";

    /**
     * Common logger to know what's happening for all the objects belong to this class
     */
    public static final Logger logger = Logger.getLogger(EmbeddedOrientDbServer.class.getName());

    /**
     * Orientdb server is already running. Text Message
     */
    public static final String ORIENTDB_ALREADY_RUNNING_TEXT = "Orientdb server is already running";

    /**
     * Orientdb is going to be shut down. Text Message
     */
    public static final String ORIENTDB_BEING_SHUTTING_DOWN_TEXT = "Shutting down Orientdb server";

    /**
     * Orientdb configuration error. Text Message
     */
    public static final String ORIENTDB_CONFIGURATION_ERROR_TEXT = "OrientDb found an exception, probably from the configuration";

    /**
     * Orientdb not running. Text Message
     */
    public static final String ORIENTDB_NOT_RUNNING_TEXT = "Orientdb server is not running";

     /**
     * Orientdb not running. Text Message
     */
    public static final String ORIENTDB_UNKNOWN_ERROR_TEXT = "Orientdb server error is unknown";

    /**
     * Config file of the database. No way to change the config file to
     * avoid misleading the server configuration.
     *
     */
    private String configFile;

    /**
     * OrientDb Server. Be careful as more than only one instance of the database
     * can be executed and serve to different set of tests
     *
     */
    @Getter
    private OServer server;

    /**
     * EmbeddedOrientDbServer constructor
     *
     * @param server     Server object to initialize.
     * @param configFile Config file to start the server
     */
    public EmbeddedOrientDbServer(OServer server, String configFile) {
        this.server = server;
        this.configFile = configFile;
    }

    /**
     * This method launches an OrientDb server
     *
     * @throws OrientDbException OrientDb general exception
     */
    public synchronized void start() throws OrientDbException
    {
        if (server == null) {
            // Log info
            logger.severe(ORIENTDB_UNKNOWN_ERROR_TEXT);
            throw new OrientDbException(ORIENTDB_UNKNOWN_ERROR_TEXT);
        }
        try {
            if (!server.isActive()) {
                // Log info
                logger.info(INITIALIZING_ORIENTDB_TEXT);
                // Startup server
                File configFileHandler = new File(configFile);
                if (configFileHandler.isFile()) {
                    server.startup(new File(configFileHandler.getAbsolutePath()));
                    // Activate server
                    server.activate();
                } else {
                    // Config file does not exist
                    throw new FileNotFoundException();
                }
            } else {
                // Log info
                logger.warning(ORIENTDB_ALREADY_RUNNING_TEXT);
            }
        } catch( InstantiationException | ClassNotFoundException
            | IllegalAccessException | FileNotFoundException e) {
            logger.log(Level.SEVERE, ORIENTDB_CONFIGURATION_ERROR_TEXT, e);
            throw new OrientDbException(ORIENTDB_CONFIGURATION_ERROR_TEXT);
        }
    }

    /**
     * This method stops the OrientDb server
     */
    public synchronized void shutdown() {
        if (server != null && server.isActive()) {
            logger.warning(ORIENTDB_BEING_SHUTTING_DOWN_TEXT);
            server.shutdown();
        }
        logger.info(ORIENTDB_NOT_RUNNING_TEXT);
        server = null;
    }
}
