package net.sxgio.titusbdd.tests.acceptance.exceptions;

/**
 * <strong>NitriteDbException.java</strong><br>
 * <strong>NitriteDbException class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class NitriteDbException extends Exception {

    /** Code to pass to the execution process. Something went wrong. */
    public static final int NITRITE_TERMINATION_ERROR_CODE = 2;

    /** Serial Version UID as needed for Exception child classes. */
    private static final long serialVersionUID = 2L;

    /**
     * Constructor with a message.
     *
     * @param message Message to write as the exception is raised
     */
    public NitriteDbException(String message) {
        super(message);
    }

    /**
     * Constructor with a message and a throwable exception.
     *
     * @param message Message to write as the exception is raised
     * @param exception Throwable exception
     */
    public NitriteDbException(String message, Throwable exception) {
        super(message, exception);
    }
}
