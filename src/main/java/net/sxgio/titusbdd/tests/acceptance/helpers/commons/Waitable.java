package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * <strong>Waitable.java</strong><br>
 * <strong>Waitable class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface Waitable {
  /**
   * This method waits some seconds.
   *
   * @param seconds Seconds to wait
   * @throws InterruptedException Just in case a timeout ocurred before finishing the time
   */
  default void waitForSeconds(int seconds) throws InterruptedException {
    TimeUnit.SECONDS.sleep(seconds);
  }

  /**
   * This method waits some seconds.
   *
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void wait(int seconds, WebDriver driver) {
    // Unexpected behaviour. Not recommended.
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
  }

  /**
   * This method waits some milliseconds.
   *
   * @param milliSeconds Seconds to wait
   * @throws InterruptedException Just in case a timeout ocurred before finishing the time
   */
  default void waitForMilliSeconds(long milliSeconds) throws InterruptedException {
    TimeUnit.MILLISECONDS.sleep(milliSeconds);
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param element Blocker
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilBlockElementDisappears(WebElement element, int seconds, WebDriver driver) {
    // Wait until the element disappears or time exhausted either.
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.invisibilityOf(element));
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param itemIdentifier Item Identifier
   * @param driver WebDriver
   */
  default void waitUntilElementIsVisible(String itemIdentifier, WebDriver driver) {
    waitUntilElementIsVisible(itemIdentifier, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS, driver);
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param element Web Element to wait for
   * @param driver WebDriver
   */
  default void waitUntilElementIsVisible(WebElement element, WebDriver driver) {
    waitUntilElementIsVisible(element, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS, driver);
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param itemIdentifier Item Identifier
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsVisible(String itemIdentifier, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(itemIdentifier)));
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param element Web element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsVisible(WebElement element, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOf(element));
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param locator used to find the element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsVisible(By locator, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
  }

  /**
   * This method waits until all WebElements in the List are visible. It uses default GENERAL
   * Timeout defined. The other parameters the method requires are:
   *
   * @param elemList The List of WebElement
   * @param driver WebDriver
   */
  default void waitUntilAllElementsAreVisible(List<WebElement> elemList, WebDriver driver) {
    WebDriverWait wait =
        new WebDriverWait(driver, Duration.ofSeconds(GlobalContent.GENERAL_TIMEOUT_IN_SECONDS));
    wait.until(ExpectedConditions.visibilityOfAllElements(elemList));
  }

  /**
   * This method waits until all WebElements in the List are visible. The other parameters the
   * method requires are:
   *
   * @param elemList The List of WebElement
   * @param seconds Seconds to Wait
   * @param driver WebDriver
   */
  default void waitUntilAllElementsAreVisible(
      List<WebElement> elemList, int seconds, WebDriver driver) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
    wait.until(ExpectedConditions.visibilityOfAllElements(elemList));
  }

  /**
   * This method waits until elements are visible.
   *
   * @param locator used to find the elements
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilAllElementsAreVisible(By locator, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOfElementLocated(locator));
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param locator used to find the elements
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementsAreVisibles(By locator, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
  }

  /**
   * This method waits until the block overlay disappears.
   *
   * @param element Web element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilOfElementsAreVisibles(WebElement element, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.visibilityOfAllElements(element));
  }

  /**
   * This method waits until the element is not visible.
   *
   * @param element Web element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsNotVisible(WebElement element, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.invisibilityOf(element));
  }

  /**
   * This method waits until the element is not visible.
   *
   * @param itemIdentifier Item Identifier
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsNotVisible(String itemIdentifier, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(itemIdentifier)));
  }

  /**
   * This method waits until elements are not visible.
   *
   * @param locator used to find the elements
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilAllElementsAreNotVisible(By locator, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.invisibilityOfElementLocated(locator));
  }

  /**
   * This method waits until the element is clickable.
   *
   * @param itemIdentifier Item Identifier
   * @param driver WebDriver
   */
  default void waitUntilElementIsClickable(String itemIdentifier, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(GlobalContent.GENERAL_TIMEOUT_IN_SECONDS))
        .until(ExpectedConditions.elementToBeClickable(By.xpath(itemIdentifier)));
  }

  /**
   * This method waits until the element is clickable.
   *
   * @param itemIdentifier Item Identifier
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsClickable(String itemIdentifier, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.elementToBeClickable(By.xpath(itemIdentifier)));
  }

  /**
   * This method waits until the element is clickable.
   *
   * @param locator used to find the element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsClickable(By locator, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.elementToBeClickable(locator));
  }

  /**
   * This method waits until the element is clickable.
   *
   * @param element Web element
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsClickable(WebElement element, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.elementToBeClickable(element));
  }

  /**
   * This method waits until alert is present.
   *
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilAlertIsPresent(int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.alertIsPresent());
  }

  /**
   * This method waits until the window expected tittle equals to expectedTittle.
   *
   * @param expectedTitle String tittle
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilTittleIs(String expectedTitle, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.titleIs(expectedTitle));
  }

  /**
   * This method waits until the window expected tittle contains expectedTittle.
   *
   * @param expectedTitle String tittle
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilTittleContains(String expectedTitle, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.titleContains(expectedTitle));
  }

  /**
   * This method waits until the url to be urlExpected.
   *
   * @param urlExpected String url
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilUrlToBe(String urlExpected, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.urlToBe(urlExpected));
  }

  /**
   * This method waits until invisibylity of element with text.
   *
   * @param expectedText String text
   * @param seconds Seconds to wait
   * @param driver WebDriver
   * @param locator used to find the element
   */
  default void waitUntilElementWithText(
      String expectedText, int seconds, WebDriver driver, By locator) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.invisibilityOfElementWithText(locator, expectedText));
  }

  /**
   * This method waits until the text matches expected pattern.
   *
   * @param pattern Pattern to use
   * @param seconds Seconds to wait
   * @param driver WebDriver
   * @param locator used to find the element
   */
  default void waitUntilTextMatches(Pattern pattern, int seconds, WebDriver driver, By locator) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.textMatches(locator, pattern));
  }

  /**
   * This method waits until the list has a specific number of elements.
   *
   * @param number used to locate a number from a list of elements of the same type
   * @param seconds Seconds to wait
   * @param driver WebDriver
   * @param locator used to find the element
   */
  default void waitUntilElementsToBe(int number, int seconds, WebDriver driver, By locator) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.numberOfElementsToBe(locator, number));
  }

  /**
   * This method waits until the list has less than a specific number of elements.
   *
   * @param number used to locate a number from a list of elements of the same type
   * @param seconds Seconds to wait
   * @param driver WebDriver
   * @param locator used to find the element
   */
  default void waitUntilNumberOfElementsToBeLessThan(
      int number, int seconds, WebDriver driver, By locator) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.numberOfElementsToBeLessThan(locator, number));
  }

  /**
   * This method waits until the list has more than a specific number of elements.
   *
   * @param number Number to expect
   * @param seconds Seconds to wait
   * @param driver WebDriver
   * @param locator used to find the element
   */
  default void waitUntilNumberOfElementsToBeMoreThan(
      int number, int seconds, WebDriver driver, By locator) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.numberOfElementsToBeMoreThan(locator, number));
  }

  /**
   * This method waits until an element is no longer attached to the DOM.
   *
   * @param element Web Element
   * @param seconds second to wait
   * @param driver WebDriver
   */
  default void waitUntilElementIsNoLongerAttachedToTheDom(
      WebElement element, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.stalenessOf(element));
  }

  /**
   * An expectation to check if js executable. Useful when you know that there should be a
   * Javascript value or something at the stage.
   *
   * @param javascriptCode string
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntilCheckIfJsExecutable(String javascriptCode, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.javaScriptThrowsNoExceptions(javascriptCode));
  }

  /**
   * Wrapper for a condition, which allows for elements to update by redrawing. This works around
   * the problem of conditions which have two parts: find an element and then check for some
   * condition on it. For these conditions it is possible that an element is located and then
   * subsequently it is redrawn on the client. When this happens a StaleElementReferenceException is
   * thrown when the second part of the condition is checked.
   *
   * @param conditions condition
   * @param seconds Seconds to wait
   * @param driver WebDriver
   */
  default void waitUntiRefresh(
      ExpectedCondition<WebDriver> conditions, int seconds, WebDriver driver) {
    new WebDriverWait(driver, Duration.ofSeconds(seconds))
        .until(ExpectedConditions.refreshed(conditions));
  }

  // PAGE LOAD METHODS

  /**
   * Wait for a page to load completely for up to TIMEOUT seconds.
   *
   * @param driver - the WebDriver instance
   */
  default void waitForPageLoadComplete(WebDriver driver) {
    waitForPageLoadComplete(driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a page to load completely for the specified number of seconds.
   *
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForPageLoadComplete(WebDriver driver, int specifiedTimeout) {
    Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    wait.until(
        driver1 ->
            String.valueOf(
                    ((JavascriptExecutor) driver1).executeScript("return document.readyState"))
                .equals("complete"));
  }

  // ELEMENT WAIT METHODS

  /**
   * Method that waits for the Selenium isDisplayed() method to return true. Hence, waits for an
   * element to be displayed. Will wait for up to TIMEOUT seconds.
   *
   * @param element - the WebElement to be displayed
   * @param driver - the WebDriver instance
   */
  default void waitForElementToBeDisplayed(WebElement element, WebDriver driver) {
    waitForElementToBeDisplayed(element, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the Selenium isDisplayed() method to return true. Hence, waits for an
   * element to be displayed. Will wait for up to the specified amount of seconds.
   *
   * @param element - the WebElement to be displayed
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementToBeDisplayed(
      WebElement element, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementIsDisplayed = arg0 -> element.isDisplayed();
    wait.until(elementIsDisplayed);
  }

  /**
   * Method that waits for the text on the element to equal an expected String. Compares the value
   * resulted from getText() on the element with the expected String. Will wait for up to the
   * TIMEOUT number of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value
   * @param expectedString - the expected value of the WebElement's text
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextEqualsString(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextEqualsString(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the text on the element to equal an expected String. Compares the value
   * resulted from getText() on the element with the expected String. Will wait for up to the
   * specifiedTimeout number of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value
   * @param expectedString - the expected value of the WebElement's text
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextEqualsString(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextEqualsString =
        arg0 -> element.getText().equals(expectedString);
    wait.until(elementTextEqualsString);
  }

  /**
   * Method that waits for the text on the element to equal an expected String but ignoring the case
   * of the two. Compares the value resulted from getText() on the element with the expected String,
   * but without taking into account the case of the two values. Therefore, for example 'tHis' and
   * 'This' will be equal when calling this method. Will wait for up to the TIMEOUT number of
   * seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value
   * @param expectedString - the expected value of the WebElement's text
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextEqualsStringIgnoreCase(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextEqualsStringIgnoreCase(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the text on the element to equal an expected String but ignoring the case
   * of the two. Compares the value resulted from getText() on the element with the expected String,
   * but without taking into account the case of the two values. Therefore, for example 'tHis' and
   * 'This' will be equal when calling this method. Will wait for up to the specifiedTimeout number
   * of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value
   * @param expectedString - the expected value of the WebElement's text
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextEqualsStringIgnoreCase(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextEqualsStringIgnoreCase =
        arg0 -> element.getText().equalsIgnoreCase(expectedString);
    wait.until(elementTextEqualsStringIgnoreCase);
  }

  /**
   * Method that waits for the text on the element (whose whitespaces are removed) to equal an
   * expected String (whose whitespaces are also removed). Basically, does a getText() on the
   * WebElement, removes all whitespaces from this resulting String, then compares this value to
   * another String that contains no whitespaces. Whitespaces include: space, new line, tab. Having
   * said that, only the non whitespace characters are compared. When calling the method, the
   * expectedString can contain whitespaces, as they are removed inside this method. Therefore,
   * 'this string here' will equal 'this string here'. Will wait for up to the TIMEOUT number of
   * seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value after removing
   *     the whitespaces on this text
   * @param expectedString - the expected value of the WebElement's text on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextEqualsStringIgnoreWhitespaces(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextEqualsStringIgnoreWhitespaces(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the text on the element (whose whitespaces are removed) to equal an
   * expected String (whose whitespaces are also removed). Basically, does a getText() on the
   * WebElement, removes all whitespaces from this resulting String, then compares this value to
   * another String that contains no whitespaces. Whitespaces include: space, new line, tab. When
   * calling the method, the expectedString can contain whitespaces, as they are removed inside this
   * method. Therefore, 'this string here' will equal 'this string here'. Will wait for up to the
   * specifiedTimeout number of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value after removing
   *     the whitespaces on this text
   * @param expectedString - the expected value of the WebElement's text on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextEqualsStringIgnoreWhitespaces(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextEqualsString =
        arg0 ->
            element.getText().replaceAll("\\s", "").equals(expectedString.replaceAll("\\s", ""));
    wait.until(elementTextEqualsString);
  }

  /**
   * Method that waits for the text on the element to contain an expected String. Checks whether the
   * value resulted from getText() on the element contains the expected String. Will wait for up to
   * the TIMEOUT number of seconds for the text on the element to contain the expected one.
   *
   * @param element - the WebElement whose text will be checked
   * @param expectedString - the value expected to be contained in the WebElement's text
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextContainsString(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextContainsString(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the text on the element to contain an expected String. Checks whether the
   * value resulted from getText() on the element contains the expected String. Will wait for up to
   * the specifiedTimeout number of seconds for the text on the element to contain the expected one.
   *
   * @param element - the WebElement whose text will be checked
   * @param expectedString - the value expected to be contained in the WebElement's text
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextContainsString(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextContainsString =
        arg0 -> element.getText().contains(expectedString);
    wait.until(elementTextContainsString);
  }

  /**
   * Method that waits for the text on the element to contain an expected String but ignoring the
   * case of the two. Checks whether the value resulted from getText() on the element contains the
   * expected String, but without taking into account the case of the two values. Therefore, for
   * example 'tHis' will contain 'his'. Will wait for up to the TIMEOUT number of seconds for the
   * text on the element to contain the expected one.
   *
   * @param element - the WebElement whose text will be checked
   * @param expectedString - the value expected to be part of the WebElement's text, ignoring the
   *     case
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextContainsStringIgnoreCase(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextContainsStringIgnoreCase(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that then waits for the text on the element to contain an expected String but ignoring
   * the case of the two. Checks whether the value resulted from getText() on the element contains
   * the expected String, but without taking into account the case of the two values. Therefore, for
   * example 'tHis' will contain 'his'. Will wait for up to the specifiedTimeout number of seconds
   * for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be checked
   * @param expectedString - the value expected to be part of the WebElement's text, ignoring the
   *     case
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextContainsStringIgnoreCase(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextContainsString =
        arg0 -> element.getText().toLowerCase().contains(expectedString.toLowerCase());
    wait.until(elementTextContainsString);
  }

  /**
   * Method that waits for the text on the element (whose whitespaces are removed) to contain an
   * expected String (whose whitespaces are also removed). Basically, does a getText() on the
   * WebElement, removes all whitespaces from this resulting String, then checks that this value
   * contains another String that has no whitespaces. Whitespaces include: space, new line, tab.
   * When calling the method, the expectedString can contain whitespaces, as they are removed inside
   * this method. Therefore, 'this string here' will contain 'str ing here'. Will wait for up to the
   * TIMEOUT number of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value after removing
   *     the whitespaces on this text
   * @param expectedString - the value expected to be part of the WebElement's text on which a
   *     whitespace removal is also done
   * @param driver - the WebDriver instance
   */
  default void waitForElementTextContainsStringIgnoreWhitespaces(
      WebElement element, String expectedString, WebDriver driver) {
    waitForElementTextContainsStringIgnoreWhitespaces(
        element, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for the text on the element (whose whitespaces are removed) to contain an
   * expected String (whose whitespaces are also removed). Basically, does a getText() on the
   * WebElement, removes all whitespaces from this resulting String, then checks that this value
   * contains another String that has no whitespaces. Whitespaces include: space, new line, tab.
   * When calling the method, the expectedString can contain whitespaces, as they are removed inside
   * this method. Therefore, 'this string here' will contain 'str ing here'. Will wait for up to the
   * specifiedTimeout number of seconds for the text on the element to be the expected one.
   *
   * @param element - the WebElement whose text will be compared to an expected value after removing
   *     the whitespaces on this text
   * @param expectedString - the value expected to be part of the WebElement's text on which a
   *     whitespace removal is also done
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementTextContainsStringIgnoreWhitespaces(
      WebElement element, String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementTextContainsString =
        arg0 ->
            element.getText().replaceAll("\\s", "").contains(expectedString.replaceAll("\\s", ""));
    wait.until(elementTextContainsString);
  }

  // Element attribute methods

  /**
   * Method that waits for an element's specified attribute's value to equal another specified
   * String. Compares the value resulted from getAttribute(nameOfAttribute) on the element with the
   * expected String. Will wait for up to the TIMEOUT number of seconds for an element's attribute
   * value to equal an expected String.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeEqualsString(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeEqualsString(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's specified attribute's value to equal another specified
   * String. Compares the value resulted from getAttribute(nameOfAttribute) on the element with the
   * expected String. Will wait for up to the specified number of seconds for an element's attribute
   * value to equal an expected String.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementAttributeEqualsString(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeEqualsString =
        arg0 -> element.getAttribute(attribute).equals(expectedString);
    wait.until(elementAttributeEqualsString);
  }

  /**
   * Method that waits for an element's specified attribute's value to equal another specified
   * String, no matter the case of the actual or expected value. Compares the value resulted from
   * getAttribute(nameOfAttribute) on the element with the expected String, ignoring the cases. Will
   * wait for up to the TIMEOUT number of seconds for an element's attribute value to equal an
   * expected String, ignoring the cases.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute, case insensitive
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeEqualsStringIgnoreCase(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeEqualsStringIgnoreCase(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's specified attribute's value to equal another specified
   * String, no matter the case of the actual or expected value. Compares the value resulted from
   * getAttribute(nameOfAttribute) on the element with the expected String, ignoring the cases. Will
   * wait for up to the TIMEOUT number of seconds for an element's attribute value to equal an
   * expected String.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute, case insensitive
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementAttributeEqualsStringIgnoreCase(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeEqualsStringIgnoreCase =
        arg0 -> element.getAttribute(attribute).equalsIgnoreCase(expectedString);
    wait.until(elementAttributeEqualsStringIgnoreCase);
  }

  /**
   * Method that waits for an element's attribute value (whose whitespaces are removed) to equal an
   * expected String (whose whitespaces are also removed). Basically, does a
   * getAttribute(nameOfAttribute) on the WebElement, removes all whitespaces from this resulting
   * String, then compares this value to another String that contains no whitespaces. When calling
   * the method, the expectedString can contain whitespaces, as they are removed inside this method.
   * Whitespaces include: space, new line, tab. Having said that, only the non whitespace characters
   * are compared. Therefore, 'this string here' will equal 'this string here'. Will wait for up to
   * the TIMEOUT number of seconds for expected condition to occur.
   *
   * @param element - the WebElement whose attribute will be verified
   * @param attribute - the attribute whose value will be verified
   * @param expectedString - the expected value of the WebElement's attribute on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeEqualsStringIgnoreWhitespaces(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeEqualsStringIgnoreWhitespaces(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's attribute value (whose whitespaces are removed) to equal an
   * expected String (whose whitespaces are also removed). Basically, does a
   * getAttribute(nameOfAttribute) on the WebElement, removes all whitespaces from this resulting
   * String, then compares this value to another String that contains no whitespaces. When calling
   * the method, the expectedString can contain whitespaces, as they are removed inside this method.
   * Whitespaces include: space, new line, tab. Having said that, only the non whitespace characters
   * are compared. Therefore, 'this string here' will equal 'this string here'. Will wait for up to
   * the specified timeout number of seconds for expected condition to occur.
   *
   * @param element - the WebElement whose attribute will be verified
   * @param attribute - the attribute whose value will be verified
   * @param expectedString - the expected value of the WebElement's attribute on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - the amount of seconds to wait for the condition to occur
   */
  default void waitForElementAttributeEqualsStringIgnoreWhitespaces(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeEqualsStringIw =
        arg0 ->
            element
                .getAttribute(attribute)
                .replaceAll("\\s", "")
                .equals(expectedString.replaceAll("\\s", ""));
    wait.until(elementAttributeEqualsStringIw);
  }

  /**
   * Method that waits for an element's specified attribute's value to contain another specified
   * String. Compares the value resulted from getAttribute(nameOfAttribute) on the element with the
   * expected String. Will wait for up to the TIMEOUT number of seconds for condition to occur.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeContainsString(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeContainsString(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's specified attribute's value to contain another specified
   * String. Compares the value resulted from getAttribute(nameOfAttribute) on the element with the
   * expected String. Will wait for up to the specified number of seconds for the condition to
   * occur.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementAttributeContainsString(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeContainsString =
        arg0 -> element.getAttribute(attribute).contains(expectedString);
    wait.until(elementAttributeContainsString);
  }

  /**
   * Method that waits for an element's specified attribute's value to equal another specified
   * String, no matter the case of the actual or expected value. Compares the value resulted from
   * getAttribute(nameOfAttribute) on the element with the expected String, ignoring the cases. Will
   * wait for up to the TIMEOUT number of seconds for an element's attribute value to equal an
   * expected String.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute, case insensitive
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeContainsStringIgnoreCase(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeContainsStringIgnoreCase(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's specified attribute's value to contain another specified
   * String, no matter the case of the actual or expected value. Compares the value resulted from
   * getAttribute(nameOfAttribute) on the element with the expected String, ignoring the cases. Will
   * wait for up to the specifiedTimeout number of seconds for the expected condition to occur.
   *
   * @param element - the WebElement whose attribute we are interested in
   * @param attribute - the attribute whose value needs to be compared to another value
   * @param expectedString - the expected value of the WebElement's attribute, case insensitive
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForElementAttributeContainsStringIgnoreCase(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeContainsStringIc =
        arg0 ->
            element.getAttribute(attribute).toLowerCase().contains(expectedString.toLowerCase());
    wait.until(elementAttributeContainsStringIc);
  }

  /**
   * Method that waits for an element's attribute value (whose whitespaces are removed) to equal an
   * expected String (whose whitespaces are also removed). Basically, does a
   * getAttribute(nameOfAttribute) on the WebElement, removes all whitespaces from this resulting
   * String, then compares this value to another String that contains no whitespaces. When calling
   * the method, the expectedString can contain whitespaces, as they are removed inside this method.
   * Whitespaces include: space, new line, tab. Having said that, only the non whitespace characters
   * are compared. Therefore, 'this string here' will equal 'this string here'. Will wait for up to
   * the TIMEOUT timeout number of seconds for expected condition to occur.
   *
   * @param element - the WebElement whose attribute will be verified
   * @param attribute - the attribute whose value will be verified
   * @param expectedString - the expected value of the WebElement's attribute on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   */
  default void waitForElementAttributeContainsStringIgnoreWhitespaces(
      WebElement element, String attribute, String expectedString, WebDriver driver) {
    waitForElementAttributeContainsStringIgnoreWhitespaces(
        element, attribute, expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Method that waits for an element's attribute value (whose whitespaces are removed) to contain
   * an expected String (whose whitespaces are also removed). Basically, does a
   * getAttribute(nameOfAttribute) on the WebElement, removes all whitespaces from this resulting
   * String, then compares this value to another String that contains no whitespaces. When calling
   * the method, the expectedString can contain whitespaces, as they are removed inside this method.
   * Whitespaces include: space, new line, tab. Having said that, only the non whitespace characters
   * are compared. Will wait for up to the specified timeout number of seconds for expected
   * condition to occur.
   *
   * @param element - the WebElement whose attribute will be verified
   * @param attribute - the attribute whose value will be verified
   * @param expectedString - the expected value of the WebElement's attribute on which a whitespace
   *     removal is also done
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - the amount of seconds to wait for the condition to occur
   */
  default void waitForElementAttributeContainsStringIgnoreWhitespaces(
      WebElement element,
      String attribute,
      String expectedString,
      WebDriver driver,
      int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> elementAttributeContainsStringIw =
        arg0 ->
            element
                .getAttribute(attribute)
                .replaceAll("\\s", "")
                .contains(expectedString.replaceAll("\\s", ""));
    wait.until(elementAttributeContainsStringIw);
  }

  /**
   * Wait for a URL to open in the browser and for the page to load completely. Wait for TIMEOUT
   * number of seconds.
   *
   * @param url - the URL to open
   * @param driver - the WebDriver instance
   */
  default void waitForUrl(String url, WebDriver driver) {
    waitForUrl(url, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a URL to open in the browser and for the page to load completely. Wait for the
   * specifiedTimeout number of seconds.
   *
   * @param url - the URL to open
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrl(String url, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect = arg0 -> driver.getCurrentUrl().equals(url);
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for a URL containing a specified String to open in the browser. The URL will not equal the
   * specified String. Just contain it. Wait for TIMEOUT number of seconds.
   *
   * @param expectedString - the String that needs to be included in the URL
   * @param driver - the WebDriver instance
   */
  default void waitForUrlContains(String expectedString, WebDriver driver) {
    waitForUrlContains(expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a URL containing a specified String to open in the browser. The URL will not equal the
   * specified String. Just contain it. Wait for the specifiedTimeout number of seconds.
   *
   * @param expectedString - the String that needs to be included in the URL
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlContains(String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect =
        arg0 -> driver.getCurrentUrl().contains(expectedString);
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for an expected URL to load in the browser, but ignore the case of the url. Compares a
   * lower case value of the actual url in the browser with the lower case value of the expected
   * url. Wait for the TIMEOUT number of seconds.
   *
   * @param url - the url expected to load in the browser, ignoring its case
   * @param driver - the WebDriver instance
   */
  default void waitForUrlIgnoreCase(String url, WebDriver driver) {
    waitForUrlIgnoreCase(url, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for an expected URL to load in the browser, but ignore the case of the url. Compares a
   * lower case value of the actual url in the browser with the lower case value of the expected
   * url. Wait for the specifiedTimeout number of seconds.
   *
   * @param url - the url expected to load in the browser, ignoring its case
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlIgnoreCase(String url, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect =
        arg0 -> driver.getCurrentUrl().equalsIgnoreCase(url.toLowerCase());
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for a URL containing a specified String to open in the browser, ignoring the case of the
   * url. Checks whether a lower case value of the actual URL contains a lower case value of the
   * expected String. Wait for the TIMEOUT number of seconds.
   *
   * @param expectedString - the String that needs to be included in the URL
   * @param driver - the WebDriver instance
   */
  default void waitForUrlContainsIgnoreCase(String expectedString, WebDriver driver) {
    waitForUrlContainsIgnoreCase(expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a URL containing a specified String to open in the browser, ignoring the case of the
   * url. Checks whether a lower case value of the actual URL contains a lower case value of the
   * expected String. Wait for the specifiedTimeout number of seconds.
   *
   * @param expectedString - the String that needs to be included in the URL
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlContainsIgnoreCase(
      String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect =
        arg0 -> driver.getCurrentUrl().toLowerCase().contains(expectedString.toLowerCase());
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for the URL in the browser to start with a specified String. Please see the startsWith
   * method from the String class for details on how this method determines whether a String starts
   * with another one. Wait for the TIMEOUT number of seconds.
   *
   * @param expectedString - the expected String to be found at the start of the URL loaded in the
   *     browser
   * @param driver - the WebDriver instance
   */
  default void waitForUrlStartsWith(String expectedString, WebDriver driver) {
    waitForUrlStartsWith(expectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for the URL in the browser to start with a specified String. Please see the startsWith
   * method from the String class for details on how this method determines whether a String starts
   * with another one. Wait for the specifiedTimeout number of seconds.
   *
   * @param expectedString - the expected String to be found at the start of the URL loaded in the
   *     browser
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlStartsWith(String expectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect =
        arg0 -> driver.getCurrentUrl().startsWith(expectedString);
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for a URL not to contain a specified String. The URL will not contain the specified
   * String. Wait for TIMEOUT number of seconds.
   *
   * @param unExpectedString - the String that shouldn't be included in the URL
   * @param driver - the WebDriver instance
   */
  default void waitForUrlNotContains(String unExpectedString, WebDriver driver) {
    waitForUrlNotContains(unExpectedString, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a URL not to contain a specified String. The URL will not contain the specified
   * String. Wait for the specifiedTimeout number of seconds.
   *
   * @param unExpectedString - the String that shouldn't be included in the URL
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlNotContains(
      String unExpectedString, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect =
        arg0 -> !driver.getCurrentUrl().contains(unExpectedString);
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }

  /**
   * Wait for a URL not to be equal to given url. The URL will not be equal to the specified String.
   * Wait for TIMEOUT number of seconds.
   *
   * @param url - the String url to check
   * @param driver - the WebDriver instance
   */
  default void waitForUrlNotEquals(String url, WebDriver driver) {
    waitForUrlNotEquals(url, driver, GlobalContent.GENERAL_TIMEOUT_IN_SECONDS);
  }

  /**
   * Wait for a URL not to be equal to given url. The URL will not be equal to the specified String.
   * Wait for TIMEOUT number of seconds.
   *
   * @param url - the String url to check
   * @param driver - the WebDriver instance
   * @param specifiedTimeout - amount of seconds you want to wait for
   */
  default void waitForUrlNotEquals(String url, WebDriver driver, int specifiedTimeout) {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(specifiedTimeout));
    ExpectedCondition<Boolean> urlIsCorrect = arg0 -> !driver.getCurrentUrl().equals(url);
    wait.until(urlIsCorrect);
    waitForPageLoadComplete(driver, specifiedTimeout);
  }
}
