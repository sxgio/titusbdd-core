package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.lang.reflect.Method;

/**
 * <strong>Fillable.java</strong><br>
 * <strong>Fillable class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public interface Fillable extends Findable {
    /**
     * This method fills up an autocomplete JQuery field.
     *
     * @param field      JSONObject with all the information to fill up the field
     * @param fieldValue Value of the field
     * @param driver     Webdriver
     * @param callback   Callback function to be executed and change data
     */
    default void fillAutocompleteField(
        WebElement field, String fieldValue, WebDriver driver, String callback) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(String.format(callback, field.getAttribute("id"), fieldValue));
        formatAndFindTheElement(driver, GlobalContent.XPATH_AUTOCOMPLETE_FIELD_DIV, fieldValue).click();
    }

    /**
     * This method fills up an autocomplete JQuery field.
     *
     * @param fieldName  Field Name
     * @param fieldValue Field Value
     * @param driver     Webdriver
     * @param callback   Callback function to be executed and change data
     */
    default void fillAutocompleteField(
        String fieldName, String fieldValue, WebDriver driver, String callback) {
        fillAutocompleteField(
            formatAndFindTheElement(
                driver, GlobalContent.XPATH_GENERAL_FIELD_SELECTOR, "*", fieldName, fieldName, fieldName),
            fieldValue,
            driver,
            callback);
    }

    /**
     * This method fills up a text field.
     *
     * @param fieldName  Field Name
     * @param fieldValue Field Value
     * @param driver     WebDriver
     *
     * @throws DriverException just in case of an error happened
     */
    default void fillTextField(String fieldName, String fieldValue, WebDriver driver)
        throws DriverException {
        fillTextField(fieldName, fieldValue, null, driver);
    }

    /**
     * This method fills up a text field.
     *
     * @param fieldName  Field Name
     * @param fieldValue Field Value
     * @param callback   Callback function to be executed and change data
     * @param driver     WebDriver
     *
     * @throws DriverException just in case of an error happened
     */
    default void fillTextField(String fieldName, String fieldValue, String callback, WebDriver driver)
        throws DriverException {
        fillTextField(
            formatAndFindTheElement(
                driver, GlobalContent.XPATH_GENERAL_FIELD_SELECTOR, "*", fieldName, fieldName, fieldName),
            fieldValue,
            callback);
    }

    /**
     * This method fills up a text field.
     *
     * @param field      JSONObject with all the information to fill up the field
     * @param fieldValue Value of the field
     * @param callback   Callback function to be executed and change data
     *
     * @throws DriverException Exception from Reflection process
     */
    default void fillTextField(WebElement field, String fieldValue, String callback)
        throws DriverException {
        try {
            if (callback != null) {
                Method method = this.getClass().getMethod(callback, String.class);
                fieldValue = (String) method.invoke(this, fieldValue);
            }
            field.sendKeys(fieldValue);
        } catch (Exception e) {
            throw new DriverException(e.getMessage());
        }
    }

    /**
     * This method fills up a select field.
     *
     * @param fieldName  Field Name
     * @param fieldValue Field Value
     * @param driver     Webdriver
     */
    default void fillSelectField(String fieldName, String fieldValue, WebDriver driver) {
        new Select(
            formatAndFindTheElement(
                driver, GlobalContent.XPATH_GENERAL_FIELD_SELECTOR, "select", fieldName, fieldName, fieldName))
            .selectByVisibleText(fieldValue);
    }

    /**
     * This method fills up a select field.
     *
     * @param fieldName  Field Name
     * @param fieldValue Field Value
     * @param driver     Webdriver
     */
    default void fillSelectFieldByValue(String fieldName, String fieldValue, WebDriver driver) {
        new Select(
            formatAndFindTheElement(
                driver, GlobalContent.XPATH_GENERAL_FIELD_SELECTOR, "select", fieldName, fieldName, fieldName))
            .selectByValue(fieldValue);
    }
}
