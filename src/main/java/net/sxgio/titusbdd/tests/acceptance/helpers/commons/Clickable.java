package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * <strong>Clickable.java</strong><br>
 * <strong>Clickable class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public interface Clickable extends Findable {
    /**
     * Click on a button based on the following REGEXP //button[@id=\"%s\" or @name=\"%s\" or
     * contains(text(), \"%s\")].
     *
     * @param buttonName Identifier, name or text of a button
     * @param driver     WebDriver
     */
    default void clickOnTheButton(String buttonName, WebDriver driver) {
        // format and click on the button.
        formatAndClickOnTheElement(
            driver, GlobalContent.XPATH_BUTTON, buttonName, buttonName, buttonName);
    }

    /**
     * Click on a button based on a WebElement.
     *
     * @param button Button WebElement
     */
    default void clickOnTheButton(WebElement button) {
        button.click();
    }

    /**
     * Format and click on an element. Varags method.
     *
     * @param itemIdentifier XPath Item Identifier
     * @param driver         WebDriver
     */
    default void clickOnTheElement(String itemIdentifier, WebDriver driver) {
        findTheElement(itemIdentifier, driver).click();
    }

    /**
     * Click on an element using a Javascript function.
     *
     * @param element element to click
     * @param driver  JavascriptExecutor
     */
    default void clickOnTheElement(WebElement element, JavascriptExecutor driver) {
        driver.executeScript("arguments[0].click();", element);
    }

    /**
     * This method clicks on a element, this is used when other procedures did not work.
     *
     * @param element WebElement to click on
     * @param actions got from net Actions(webdriver)
     */
    default void clickOnTheElement(WebElement element, Actions actions) {
        actions.click(element).perform();
    }

    /**
     * Format and click on an element. Varags method.
     *
     * @param driver          WebDriver
     * @param locatorToFormat XPath to be fullfilled with sprintf
     * @param items           Items to change on the locator
     */
    default void formatAndClickOnTheElement(
        WebDriver driver, String locatorToFormat, Object... items) {
        formatAndFindTheElement(driver, locatorToFormat, items).click();
    }
}
