package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.restassured.RestAssured;
import io.restassured.config.RedirectConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.auth.AuthInterface;
import net.sxgio.titusbdd.tests.acceptance.exceptions.RestException;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

/**
 * <strong>RestDriver.java</strong><br>
 * <strong>RestDriver class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class RestDriver implements RestInterface {

  /** Message just in case an error while connecting. */
  public static final String REST_EXCEPTION_MESSAGE = "Rest exception caught.";

  /** Message just in case a call requested for an unimplemented method. */
  public static final String REST_EXCEPTION_NOT_IMPLEMENTED_MESSAGE = "Not Implemented method.";

  /** HTTP 202 code. */
  public static final int STATUS_CODE_ACCEPTED = HttpStatus.SC_ACCEPTED;

  /** HTTP 201 code. */
  public static final int STATUS_CODE_CREATED = HttpStatus.SC_CREATED;

  /** HTTP 204 code. */
  public static final int STATUS_CODE_NO_CONTENT = HttpStatus.SC_NO_CONTENT;

  /** HTTP 200 code. */
  public static final int STATUS_CODE_OK = HttpStatus.SC_OK;

  /** Common logger to know what is happening for all objects belong to this class. */
  public static final Logger logger = Logger.getLogger(RestDriver.class.getName());

  /** Authentication object based on the definition. */
  @Getter @Setter protected AuthInterface authentication;

  /** Authentication Params. */
  @Getter @Setter protected HashMap<String, Object> authenticationParams;

  /** Cookies for rest assured. */
  @Getter @Setter protected Cookies cookies;

  /** Main delete status code. */
  @Getter @Setter protected int deleteStatusCode = STATUS_CODE_OK;

  /** Main get status code. */
  @Getter @Setter protected int getStatusCode = STATUS_CODE_OK;

  /** Just in case either we must follow redirects or not. */
  @Getter @Setter protected boolean followRedirect = true;

  /** Main options status code. */
  @Getter @Setter protected int optionsStatusCode = STATUS_CODE_OK;

  /** Main patch status code. */
  @Getter @Setter protected int patchStatusCode = STATUS_CODE_ACCEPTED;

  /** Main post status code. */
  @Getter @Setter protected int postStatusCode = STATUS_CODE_CREATED;

  /** Main put status code. */
  @Getter @Setter protected int putStatusCode = STATUS_CODE_ACCEPTED;

  /**
   * Constructor by default.
   *
   * @param baseUri Base URI as entry point
   * @param port Base Port to set up the socket
   */
  public RestDriver(String baseUri, int port) {
    RestAssured.baseURI = baseUri;
    RestAssured.port = port;
    cookies = new Cookies(new Cookie.Builder("foo", "fooValue").build());
  }

  /**
   * Authenticate method. Strategy pattern.
   *
   * @param params Parameters passed
   * @return RequestSpecification to keep pushing chain methods
   */
  public RequestSpecification authenticate(Map<String, Object> params) {
    if (authentication != null && params != null) {
      return authentication.auth((HashMap<String, Object>) params);
    } else {
      return given();
    }
  }

  /**
   * Connect HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException Not implemented yet.
   */
  @Override
  public void connect(String resource) throws RestException {
    throw new RestException(REST_EXCEPTION_NOT_IMPLEMENTED_MESSAGE);
  }

  /**
   * Delete HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response delete(String resource) {
    return authenticate(authenticationParams)
        .cookies(cookies)
        .when()
        .delete(resource)
        .then()
        .assertThat()
        .statusCode(deleteStatusCode)
        .extract()
        .response();
  }

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response get(String resource) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .when()
        .get(resource)
        .then()
        .assertThat()
        .statusCode(getStatusCode)
        .extract()
        .response();
  }

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response get(String resource, Headers headers) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .when()
        .get(resource)
        .then()
        .assertThat()
        .statusCode(getStatusCode)
        .extract()
        .response();
  }

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @param params Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response get(String resource, Headers headers, Map<String, ?> params) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .params(params)
        .when()
        .get(resource)
        .then()
        .assertThat()
        .statusCode(getStatusCode)
        .extract()
        .response();
  }

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param params Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response get(String resource, Map<String, ?> params) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .params(params)
        .when()
        .get(resource)
        .then()
        .assertThat()
        .statusCode(getStatusCode)
        .extract()
        .response();
  }

  /**
   * Head HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException Not implemented yet.
   */
  @Override
  public void head(String resource) throws RestException {
    throw new RestException(REST_EXCEPTION_NOT_IMPLEMENTED_MESSAGE);
  }

  /**
   * Options HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   */
  @Override
  public void options(String resource) {
    authenticate(authenticationParams)
        .cookies(cookies)
        .when()
        .options(resource)
        .then()
        .assertThat()
        .statusCode(optionsStatusCode);
  }

  /**
   * Options HTTP Verb in a REST interface. Use this method with the awaitility library to check
   * some time response.
   */
  @Override
  public void options() {
    authenticate(authenticationParams)
        .cookies(cookies)
        .when()
        .options(RestAssured.basePath)
        .then()
        .assertThat()
        .statusCode(optionsStatusCode)
        .extract()
        .response();
  }

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response patch(String resource) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .when()
        .patch(resource)
        .then()
        .assertThat()
        .statusCode(patchStatusCode)
        .extract()
        .response();
  }

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param payload Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response patch(String resource, String payload) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .body(payload)
        .when()
        .patch(resource)
        .then()
        .assertThat()
        .statusCode(patchStatusCode)
        .extract()
        .response();
  }

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response patch(String resource, Headers headers) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .when()
        .patch(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @param payload Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response patch(String resource, Headers headers, String payload) {
    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .body(payload)
        .when()
        .patch(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response post(String resource) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .when()
        .post(resource)
        .then()
        .assertThat()
        .statusCode(postStatusCode)
        .extract()
        .response();
  }

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param params Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response post(String resource, Map<String, ?> params) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .urlEncodingEnabled(true)
        .params(params)
        .when()
        .post(resource)
        .then()
        .assertThat()
        .statusCode(postStatusCode)
        .extract()
        .response();
  }

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response post(String resource, Headers headers) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .urlEncodingEnabled(true)
        .headers(headers)
        .when()
        .post(resource)
        .then()
        .assertThat()
        .statusCode(postStatusCode)
        .extract()
        .response();
  }

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @param params Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response post(String resource, Headers headers, Map<String, ?> params) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .urlEncodingEnabled(true)
        .headers(headers)
        .params(params)
        .when()
        .post(resource)
        .then()
        .assertThat()
        .statusCode(postStatusCode)
        .extract()
        .response();
  }

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @param payload If parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response post(String resource, Headers headers, String payload) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .body(payload)
        .when()
        .post(resource)
        .then()
        .assertThat()
        .statusCode(postStatusCode)
        .extract()
        .response();
  }

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response put(String resource) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .when()
        .put(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param payload Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response put(String resource, String payload) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .body(payload)
        .when()
        .put(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response put(String resource, Headers headers) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .when()
        .put(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers Headers to be defined in a request
   * @param payload Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  @Override
  public Response put(String resource, Headers headers, String payload) {

    return authenticate(authenticationParams)
        .config(
            RestAssuredConfig.newConfig()
                .redirect(RedirectConfig.redirectConfig().followRedirects(followRedirect)))
        .cookies(cookies)
        .headers(headers)
        .body(payload)
        .when()
        .put(resource)
        .then()
        .assertThat()
        .statusCode(putStatusCode)
        .extract()
        .response();
  }

  /**
   * Trace HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException Not implemented yet.
   */
  @Override
  public void trace(String resource) throws RestException {
    throw new RestException(REST_EXCEPTION_NOT_IMPLEMENTED_MESSAGE);
  }
}
