package net.sxgio.titusbdd.tests.acceptance.db;

import com.orientechnologies.orient.server.OServerMain;
import net.sxgio.titusbdd.tests.acceptance.exceptions.OrientDbException;
import lombok.Getter;

import java.io.File;
import java.util.logging.Logger;

/**
 * <strong>OrientDbServerStarter.java</strong><br>
 * <strong>OrientDbServerStarter class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class OrientDbServerStarter
{
    /**
     * Common logger to know what's happening for all the objects belong to this class
     */
    public static final Logger logger = Logger.getLogger(OrientDbServerStarter.class.getName());

    /**
     * Embedded Orientdb Server. To start or stop database server from code
     */
    @Getter
    private static EmbeddedOrientDbServer orientdb;

    /**
     * Just in case an error running the server. Text error
     */
    public static final String ORIENTDB_ERROR = "Starting up the OrientDb server raised the following error %s";

    /**
     * Orientdb trigger method
     *
     * @param args External args if needed
     */
    public static void main(final String[] args) {
        try {
            final String baseUrl = System.getProperty("orientdb.resources");
            final String configFile = baseUrl + System.getProperty("orientdb.baseUrl");
            File baseUrlHandler = new File(baseUrl);
            File configFileHandler = new File(configFile);
            if (baseUrlHandler.isDirectory()) {
                final String orientdbHome = baseUrlHandler.getAbsolutePath();
                System.setProperty("ORIENTDB_HOME", orientdbHome);
                orientdb = new EmbeddedOrientDbServer(
                    OServerMain.create(), configFileHandler.getAbsolutePath()
                );
                orientdb.start();
            } else {
                throw new OrientDbException("Not directory avalable with OrientDB data accesible");
            }
        } catch (final Exception e) {
            logger.severe(String.format(ORIENTDB_ERROR, e.getMessage()));
        }
    }
}
