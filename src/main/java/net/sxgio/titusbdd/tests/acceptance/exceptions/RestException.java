package net.sxgio.titusbdd.tests.acceptance.exceptions;

/**
 * <strong>RestException.java</strong><br>
 * <strong>RestException class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class RestException extends Exception {
    /**
     * Code to pass to the execution process. Something went wrong.
     */
    public static final int REST_TERMINATION_ERROR_CODE = 5;

    /**
     * Serial Version UID as needed for Exception child classes.
     */
    private static final long serialVersionUID = 5L;

    /**
     * Constructor with a message and a throwable exception
     *
     * @param message   Message to write as the exception is raised
     * @param exception Throwable exception
     *
     */
    public RestException(String message, Throwable exception) {
        super(message, exception);
    }

    /**
     * Constructor with a message
     *
     * @param message Message to write as the exception is raised
     *
     */
    public RestException(String message) {
        super(message);
    }
}