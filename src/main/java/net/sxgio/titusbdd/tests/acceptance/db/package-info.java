/**
 * Provide the necessary objects for starting and communicating with some databases.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.db;
