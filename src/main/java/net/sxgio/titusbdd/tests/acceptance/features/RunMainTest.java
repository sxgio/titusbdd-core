package net.sxgio.titusbdd.tests.acceptance.features;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.utils.ClassFeeder;
import net.sxgio.titusbdd.tests.acceptance.utils.SharedContent;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import java.util.logging.Logger;

/**
 * <strong>RunMainTest.java</strong><br>
 * <strong>RunMainTest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@CucumberOptions()
public class RunMainTest extends AbstractTestNGCucumberTests {

  /** Common logger to know what is happening along the test. */
  protected static final Logger logger = Logger.getLogger(RunMainTest.class.getName());

  /** Dependency Injection just in case of needed for some objects. */
  @Getter protected static final ClassFeeder feeder = new ClassFeeder();

  /** Shared Content is an object where to define all in common for different steps. */
  @Getter @Setter protected static SharedContent sharedContent;

  /** QAProperties. * */
  @Getter @Setter protected static GlobalConfigurationEntity globalProperties;

  /**
   * Data Provider for Scenarios.
   *
   * @return Object[][]
   */
  @Override
  @DataProvider()
  public Object[][] scenarios() {
    return super.scenarios();
  }

  /** Set up configuration before tests. Override as needed */
  @Override
  @BeforeClass(alwaysRun = true)
  public void setUpClass(ITestContext context) {
    // Execute parent class method
    super.setUpClass(context);
  }

  /** Tear down configuration after class.  Override as needed */
  @Override
  @AfterClass(alwaysRun = true)
  public void tearDownClass() {
    // Execute parent class method
    super.tearDownClass();
  }
}
