package net.sxgio.titusbdd.tests.acceptance.entities;

import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

/**
 * <strong>MongoConfigurationBean.java</strong><br>
 * <strong>MongoConfigurationBean class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Data
public class MongoConfigurationEntity implements BaseConfigurationEntityInterface {
    /** Whether database is active or not */
    private boolean active;

    /** Mongo dabatase. */
    private String mongoDatabase;

    /** Mongo user password. */
    private String mongoPassword;

    /** Mongo server port. */
    private int mongoPort;

    /** Mongo server FQDN. */
    private String mongoServer;

    /** Mongo user. */
    private String mongoUser;
}
