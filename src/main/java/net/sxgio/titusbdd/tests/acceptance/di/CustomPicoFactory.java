package net.sxgio.titusbdd.tests.acceptance.di;

import io.cucumber.core.backend.ObjectFactory;
import lombok.Getter;
import lombok.Setter;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoBuilder;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * <strong>CustomPicoFactory.java</strong><br>
 * <strong>CustomPicoFactory class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class CustomPicoFactory implements ObjectFactory
{
    /**
     * Common logger to know what is happening
     */
    public static final Logger logger = Logger.getLogger(CustomPicoFactory.class.getName());

    /**
     * Info text for adding a new class to the pico container
     */
    public static final String PICO_ADD_CLASS_TEXT = "Adding the following class to the DI ";

    /**
     * Info text shown while initializing the container
     */
    public static final String PICO_INIT_CONTAINER_TEXT = "Initializing classes inside the DI";

    /**
     * Info text shown while stopping the container
     */
    public static final String PICO_STOP_CONTAINER_TEXT = "Shutting down DI";

    /**
     * Classes stored to build new objects
     */
    @Getter
    @Setter
    protected Set<Class<?>> classes = new HashSet<>();

    /**
     * Mutable Pico Container. In general PicoBuilder
     */
    @Getter
    @Setter
    protected MutablePicoContainer pico;
     
    /**
     * Add a class to the factory container for dependency injection
     * 
     * @param clazz Class to get new instances from.
     * 
     * @return boolean
     */
    public boolean addClass(final Class<?> clazz) {
        boolean retval = false;
        if (clazz != null) {
            logger.info(PICO_ADD_CLASS_TEXT + clazz.getSimpleName());
            if (!clazz.isInterface() && classes.add(clazz)) {
                retval = addConstructorDependencies(clazz);
            }
        }
        return retval;
    }

    /**
     * Add a class to the factory container for dependency injection
     * 
     * @param clazz Class to get new instances from.
     * 
     * @return boolean
     */
    @SuppressWarnings("java:S3740")
    protected boolean addConstructorDependencies(final Class<?> clazz) {
        boolean retval = true;
        for (final Constructor constructor : clazz.getConstructors()) {
            for (final Class paramClazz : constructor.getParameterTypes()) {
                retval = addClass(paramClazz);
                if (!retval) {
                    break;
                }
            }
        }
        return retval;
    }    

    /**
     * Get a new object provided by the factory based on the class type.
     * 
     * @param type Class to get new instances from.
     * 
     * @return T
     */
    public <T> T getInstance(final Class<T> type) {
        return pico.getComponent(type);
    }
    
    /**
     * Start Method. Really to use in this way?
     */
    public void start() {
        logger.info(PICO_INIT_CONTAINER_TEXT);
        pico = new PicoBuilder().withCaching().withLifecycle().build();
        for (final Class<?> clazz : classes) {
            pico.addComponent(clazz);
        }
        pico.start();
    }

    /**
     * Stop Method. Really to use in this way?
     */
    public void stop() {
        logger.info(PICO_STOP_CONTAINER_TEXT);
        pico.stop();
        pico.dispose();
    } 
}
