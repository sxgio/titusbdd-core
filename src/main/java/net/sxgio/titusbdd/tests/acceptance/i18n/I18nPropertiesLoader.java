package net.sxgio.titusbdd.tests.acceptance.i18n;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * <strong>I18nPropertiesLoader.java</strong><br>
 * <strong>I18nPropertiesLoader class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class I18nPropertiesLoader
{
    /**
     * Enviromental property to define the pattern used to locate a language properties file
     */
    public static final String ENV_FILE_PATTERN_PROPERTY = "language.properties.file.pattern";

    /**
     * If no ENV_FILE_PATTERN_PROTERTY is defined use this instead
     */
    public static final String FILE_PATTERN_BY_DEFAULT = "i18n/i18n-%s.properties";

    /**
     * Language by default
     */
    public static final String LANGUAGE_BY_DEFAULT = "es";

    /**
     * Name of the file opened to get all the properties
     */
    @Getter
    @Setter
    private String fileName;

    /**
     * Object to store the properties obtained from the properties file
     */
    @Getter
    @Setter
    private Properties internalProperties;

    /**
     * Common logger to know what is happening
     */
    protected final Logger logger = Logger.getLogger(I18nPropertiesLoader.class.getName());

    /**
     * Constructor by default
     *
     * @throws IOException just in case file does not exist
     *
     */
    public I18nPropertiesLoader() throws IOException {
        new I18nPropertiesLoader(LANGUAGE_BY_DEFAULT);
    }

    /**
     * Constructor choosing the language
     *
     * @param language Language for I18n
     *
     * @throws IOException just in case file does not exist
     *
     */
    public I18nPropertiesLoader(String language) throws IOException {
        // Initalizing the properties
        internalProperties = new Properties();
        // FileName setup to load spanish version by default
        fileName = String.format(
            System.getProperty(ENV_FILE_PATTERN_PROPERTY, FILE_PATTERN_BY_DEFAULT),
            language
        );
        init();
    }

    /**
     * Get any property by its index
     *
     * @param index index or property name that has to be obtained
     *
     * @return Object
     */
    public Object getProperty(String index) {
        return internalProperties.get(index);
    }

    /**
     * Fill up the properties.
     *
     */
    private void init() throws IOException
    {
        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        internalProperties.load(inputStream);
    }

    /**
     * Get any property by its index
     *
     * @param index index or property name that has either to be obtained or added
     * @param value new value to be stored
     *
     */
    public void setProperty(String index, String value) {
        internalProperties.setProperty(index, value);
    }
}
