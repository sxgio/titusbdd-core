package net.sxgio.titusbdd.tests.acceptance.auth;

import io.restassured.authentication.OAuthSignature;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

/**
 * <strong>AuthOAuth2.java</strong><br>
 * <strong>AuthOAuth2 class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class AuthOAuth2 implements AuthInterface {

    /**
     * Method to get a RequestSpecification based on auth. Chained method
     *
     * @param params different parameters needed based on the auth type
     *
     * @return RequestSpecification
     */
    @Override
    public RequestSpecification auth(HashMap<String, Object> params) {
        if (params.containsKey("signature")) {
            return given().auth().oauth2(
                (String)params.get("accessToken"),
                (OAuthSignature)params.get("signature")
            );
        } else {
            return given().auth().oauth2(
                (String)params.get("accessToken")
            );
        }
    }
}