package net.sxgio.titusbdd.tests.acceptance.entities;

import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

/**
 * <strong>NitriteConfigurationBean.java</strong><br>
 * <strong>NitriteConfigurationBean class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Data
public class NitriteConfigurationEntity implements BaseConfigurationEntityInterface {

  /** Whether database is active or not. */
  private boolean active;

  /** Nitrite collection. */
  private String nitriteCollection;

  /** Nitrite user password. */
  private String nitritePassword;

  /** Nitrite file path. */
  private String nitritePath;

  /** Nitrite user. */
  private String nitriteUser;
}
