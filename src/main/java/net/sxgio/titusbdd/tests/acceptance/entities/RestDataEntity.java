package net.sxgio.titusbdd.tests.acceptance.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * <strong>OAuth2TokenBean.java</strong><br>
 * <strong>OAuth2TokenBean class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Data
public class RestDataEntity implements BaseConfigurationEntityInterface {

  /** Client name. */
  @NotNull(message = "Profile identifier cannot be null")
  @NotEmpty(message = "Profile identifier cannot be empty")
  @JsonProperty("name")
  protected String name;

  /** Client Identifier. */
  @NotNull(message = "Client identifier cannot be null")
  @NotEmpty(message = "Client identifier cannot be empty")
  @JsonProperty("clientId")
  protected String clientId;

  /** Client Secret. */
  @NotNull(message = "Client secret cannot be null")
  @NotEmpty(message = "Client secret cannot be empty")
  @JsonProperty("clientSecret")
  protected String clientSecret;

  /** Grant Type. */
  @NotNull(message = "grant type cannot be null")
  @NotEmpty(message = "Grant type cannot be empty")
  @JsonProperty("grantType")
  protected String grantType;

  /** Resources. */
  @NotNull(message = "Resources cannot be null")
  @NotEmpty(message = "Resources cannot be empty")
  @JsonProperty("resources")
  protected Map<String, Object> resources;

  /** Scope. */
  @NotNull(message = "Scope cannot be null")
  @NotEmpty(message = "Scope cannot be empty")
  @JsonProperty("scope")
  protected String scope;

  /** URL. */
  @NotNull(message = "URL cannot be null")
  @NotEmpty(message = "URL cannot be empty")
  @JsonProperty("url")
  protected String url;
}
