/**
 * Provide the necessary interfaces to make contracts to page object pattern
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.pages.contracts;
