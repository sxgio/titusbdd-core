package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.NotFoundPageObjectInterface;
import org.apache.commons.lang.NotImplementedException;
import org.apache.http.HttpStatus;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.openqa.selenium.WebDriver;

import java.util.logging.Level;

/**
 * <strong>NotFoundPageObjectJSOUP.java</strong><br>
 * <strong>NotFoundPageObjectJSOUP class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class NotFoundPageObjectJSOUP extends AbstractPageObjectJSOUP
    implements NotFoundPageObjectInterface {
  /** Unknown resource (URL). */
  public static final String UNKNOWNRESOURCE = "/error";

  /** Text for a status exception. */
  public static final String STATUSEXCEPTIONTEXT =
      "Response code different from OK. Error retrieved";

  /** Text for general exception. */
  public static final String GENERALEXCEPTIONTEXT = "Unknown error. Error retrieved ";

  /**
   * Get Driver just in case of needed.
   *
   * @return WebDriver Never reached
   */
  @Override
  public WebDriver getDriver() {
    // Uncomment the following line if you need to stop at this test
    throw new NotImplementedException();
  }

  /**
   * Load Unknown page. The error that comes from this page should be under control.
   *
   * @return boolean
   */
  @Override
  public boolean loadUnknownPage() {
    boolean retval = true;
    // Check if SSL is needed
    checkSSLCertification();
    // by default, if connection is not executed, an error will be raised
    final String errorPageUrl = RunMainTest.getGlobalProperties().getWebData().getBaseUrl() + UNKNOWNRESOURCE;
    // Process request
    try {
      // Obtain the response ignoring errors to get the body content
      final Connection.Response response =
          Jsoup.connect(errorPageUrl).ignoreHttpErrors(true).execute();
      // Obtain the body content ignoring errors
      webDocument = Jsoup.connect(errorPageUrl).ignoreHttpErrors(true).get();
      // Setting up status code
      RunMainTest.getSharedContent().setStatusCode(response.statusCode());
    } catch (final HttpStatusException statusException) {
      logger.log(
          Level.FINE,
          () -> GENERALEXCEPTIONTEXT + STATUSEXCEPTIONTEXT + statusException.getMessage());
      logger.log(Level.FINE, "loadUnknownPage exception", statusException);
      // Error found in the request.
      RunMainTest.getSharedContent().setStatusCode(statusException.getStatusCode());
      retval = false;
    } catch (Exception e) {
      logger.log(Level.FINE, () -> GENERALEXCEPTIONTEXT + e.getMessage());
      // Error found in the request.
      RunMainTest.getSharedContent().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      retval = false;
    }
    return retval;
  }
}
