package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.remote.AutomationName;
import org.openqa.selenium.remote.CapabilityType;

/**
 * <strong>IosAppiumConfiguration.java</strong><br>
 * <strong>IosAppiumConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class IOSAppiumConfiguration extends AbstractAppiumConfiguration {
  /** Info text shown while initializing the chrome driver. */
  public static final String IOS_APPIUM_STARTING_UP_TEXT = "Starting up a IOS-Appium driver";

  /** Constructor by default. */
  public IOSAppiumConfiguration() {
    super();
  }

  /** Initialize the IOS Appium Configuration. */
  @Override
  public void init() {
    logger.info(IOS_APPIUM_STARTING_UP_TEXT);
    options.setCapability("deviceName", globalProperties.getWebData().getIosDevice());
    options.setCapability("platformName", "iOS");
    options.setCapability("udid", globalProperties.getWebData().getIosDevice());
    options.setCapability("platformVersion", globalProperties.getWebData().getIosVersion());

    // If no application file provided means browser is going to be used
    if (globalProperties.getWebData().getApplicationFile().equals("false")) {
      options.setCapability(CapabilityType.BROWSER_NAME, "Safari");
    } else {
      options.setCapability("app", globalProperties.getWebData().getApplicationFile());
    }
    options.setCapability("automationName", AutomationName.IOS_XCUI_TEST);
  }
}
