/**
 * Provide the necessary functionality to interact with either selenium or appium.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.helpers;
