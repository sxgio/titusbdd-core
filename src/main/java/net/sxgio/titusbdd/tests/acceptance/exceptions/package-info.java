/**
 * Provide the necessary objects to handle exceptions.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.exceptions;
