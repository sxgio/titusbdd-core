/**
 * Provide the necessary objects to keep data from files and databases well organized.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.entities;
