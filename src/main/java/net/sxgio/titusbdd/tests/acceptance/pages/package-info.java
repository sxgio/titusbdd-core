/**
 * Provide the necessary objects to manipulate page object pattern
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.pages;
