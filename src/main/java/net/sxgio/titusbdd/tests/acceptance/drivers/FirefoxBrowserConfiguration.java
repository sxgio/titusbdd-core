package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.logging.Logger;

/**
 * <strong>FirefoxBrowserConfiguration.java</strong><br>
 * <strong>FirefoxBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class FirefoxBrowserConfiguration implements BrowserInterface {
  /** Info text shown while initializing the chrome driver. */
  public static final String FIREFOX_DRIVER_STARTING_UP_TEXT = "Starting up a Firefox driver";

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(FirefoxBrowserConfiguration.class.getName());

  /** Firefox driver. */
  @Getter @Setter protected FirefoxDriver firefoxDriverInstance;

  /** Firefox options. */
  @Getter @Setter protected FirefoxOptions options;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /** Initialize the Browser Configuration. */
  public void init() {
    logger.info(FIREFOX_DRIVER_STARTING_UP_TEXT);
    // Getting firefox profile
    FirefoxProfile profile = new FirefoxProfile();
    // Setting properties to avoid ssl and use the default profile by default
    profile.setPreference("intl.accept_languages", "no,en-us,en,es");
    profile.setPreference("security.default_personal_cert", "Select Automatically");
    try {
      if (globalProperties.getWebData().getBrowserCapabilities() != null) {
        JsonNode browserOptions = processBrowserOptions(globalProperties.getWebData().getBrowserCapabilities());
        browserOptions
            .fields()
            .forEachRemaining(s -> profile.setPreference(s.getKey(), s.getValue().textValue()));
      }
    } catch (JsonProcessingException e) {
      // Do nothing. Capabilites cannot be read from json object. Raise a warning
      logger.warning(GlobalContent.JSON_CAPABILITIES_ERROR_TEXT);
    }
    profile.setAcceptUntrustedCertificates(true);
    profile.setAssumeUntrustedCertificateIssuer(false);
    options = new FirefoxOptions().setProfile(profile);
    if (globalProperties.getWebData().isHeadless()) {
      options.addArguments("-headless");
    }
  }

  /**
   * This method provides the selenium driver based on configuration.
   *
   * @return WebDriver
   */
  public WebDriver getDriver() {
    if (firefoxDriverInstance == null) {
      init();
      // Instantiate FirefoxDriver
      firefoxDriverInstance = new FirefoxDriver(options);
    }
    return firefoxDriverInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return FirefoxOptions Current firefox capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    return options;
  }
}
