package net.sxgio.titusbdd.tests.acceptance.entities.contracts;

/**
 * <strong>BaseConfigurationBeanInterface.java</strong><br>
 * <strong>BaseConfigurationBeanInterface class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface BaseConfigurationEntityInterface {
}
