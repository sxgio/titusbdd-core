package net.sxgio.titusbdd.tests.acceptance.db;

import lombok.Getter;
import net.sxgio.titusbdd.tests.acceptance.exceptions.NitriteDbException;
import org.dizitart.no2.Nitrite;

import java.util.Objects;
import java.util.logging.Logger;

/**
 * <strong>EmbeddedNitriteDbServer.java</strong><br>
 * <strong>EmbeddedNitriteDbServer class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class EmbeddedNitriteDbServer {
    /**
     * Nitritedb is starting. Text Message
     */
    public static final String INITIALIZING_NITRITEDB_TEXT = "Starting up Nitritedb server";

    /**
     * Common logger to know what's happening for all the objects belong to this class
     */
    public static final Logger logger = Logger.getLogger(EmbeddedNitriteDbServer.class.getName());

    /**
     * Nitritedb server is already running. Text Message
     */
    public static final String NITRITEDB_ALREADY_RUNNING_TEXT = "Nitritedb server is already running";

    /**
     * Nitritedb is going to be shut down. Text Message
     */
    public static final String NITRITEDB_BEING_SHUTTING_DOWN_TEXT = "Shutting down Nitritedb server";

    /**
     * Nitritedb not running. Text Message
     */
    public static final String NITRITEDB_NOT_RUNNING_TEXT = "Nitritedb server is not running";

    /**
     * Nitritedb not running. Text Message
     */
    public static final String NITRITE_UNKNOWN_ERROR_TEXT = "Nitritedb server error is unknown";

    /**
     * NitriteDb Server. Be careful as more than only one instance of the database
     * can be executed and serve to different set of tests
     *
     */
    @Getter
    protected Nitrite server;

    /**
     * EmbeddedNitriteDbServer constructor
     *
     * @param server Server object to initialize.
     *
     * @throws NitriteDbException just in case an error connecting to the db file
     */
    public EmbeddedNitriteDbServer(Nitrite server) throws NitriteDbException {
        try {
            // Server should have been initialized previously
            if (!Objects.isNull(server) && !server.isClosed()) {
                this.server = server;
            } else {
                logger.severe(NITRITEDB_NOT_RUNNING_TEXT);
                throw new NitriteDbException(NITRITEDB_NOT_RUNNING_TEXT);
            }
        } catch (Exception e) {
            logger.severe(NITRITE_UNKNOWN_ERROR_TEXT);
            logger.severe(NITRITEDB_NOT_RUNNING_TEXT);
            throw new NitriteDbException(NITRITEDB_NOT_RUNNING_TEXT);
        }
    }

    /**
     * EmbeddedNitriteDbServer constructor
     *
     * @param configFile Config file where to store server data
     * @param user       Security defined with a user to access the file
     * @param password   Secutity defined with a password to acess the file
     */
    public EmbeddedNitriteDbServer(String configFile, String user, String password) {
        start(configFile, user, password);
    }

    /**
     * This method launches an NitriteDb server
     *
     * @param configFile Config file where to store server data
     * @param user       Security defined with a user to access the file
     * @param password   Secutity defined with a password to acess the file
     */
    public synchronized void start(String configFile, String user, String password) {
        // Try shutting down server before starting up.
        shutdown();
        logger.info(INITIALIZING_NITRITEDB_TEXT);
        server = Nitrite.builder()
            .compressed()
            .filePath(configFile)
            .openOrCreate(user, password);
    }

    /**
     * This method stops the NitriteDb server
     */
    public synchronized void shutdown() {
        if (!Objects.isNull(server) && !server.isClosed()) {
            logger.warning(NITRITEDB_ALREADY_RUNNING_TEXT);
            logger.warning(NITRITEDB_BEING_SHUTTING_DOWN_TEXT);
            server.close();
        }
        logger.info(NITRITEDB_NOT_RUNNING_TEXT);
    }
}
