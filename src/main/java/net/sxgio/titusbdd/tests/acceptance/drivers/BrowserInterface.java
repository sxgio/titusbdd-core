package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.appium.java_client.AppiumDriver;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

/**
 * <strong>BrowserInterface.java</strong><br>
 * <strong>BrowserInterface interface</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface BrowserInterface {
  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return Capabilities
   */
  Capabilities getCapabilities();

  /**
   * This method provides WebDriver based on file configuration.
   *
   * @return WebDriver
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  WebDriver getDriver() throws DriverException;

  /**
   * This method provides AppiumDriver just if exists.
   *
   * @return AppiumDriver
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  default AppiumDriver getAppiumDriver() throws DriverException {
    return null;
  }

  /** Initialize the Browser Configuration, mainly capabilities. */
  void init();

  /**
   * This method provides AppiumDriver just if exists.
   *
   * @param browserOptions Options for specific browsers in JSON format
   * @return LinkedTreeMap with browserOptions. Otherwise null
   * @throws JsonProcessingException just in case JSON data cannot be processed
   */
  default JsonNode processBrowserOptions(String browserOptions) throws JsonProcessingException {
    return processJsonStringData(browserOptions);
  }

  /**
   * This method provides AppiumDriver just if exists.
   *
   * @param jsonArgs Arguments for specific configuration in JSON format
   * @return LinkedTreeMap with browserOptions. Otherwise null
   * @throws JsonProcessingException just in case JSON data cannot be processed
   */
  default JsonNode processJsonStringData(String jsonArgs) throws JsonProcessingException {
    JsonNode retval = null;
    if (jsonArgs != null) {
      // Make the ObjectMapper
      ObjectMapper mapper = new ObjectMapper();
      // Get Tree Map
      retval = mapper.readTree(jsonArgs);
    }
    return retval;
  }

  /**
   * This method sets up globalProperties.
   *
   * @param globalProperties Global properties
   */
  void setGlobalProperties(GlobalConfigurationEntity globalProperties);
}
