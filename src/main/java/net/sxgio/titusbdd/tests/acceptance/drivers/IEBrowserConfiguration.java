package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.util.logging.Logger;

/**
 * <strong>IEBrowserConfiguration.java</strong><br>
 * <strong>IEBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class IEBrowserConfiguration implements BrowserInterface {
  /** Info text shown while initializing the chrome driver. */
  public static final String IEXPLORER_DRIVER_STARTING_UP_TEXT = "Starting up a IExplorer driver";

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(IEBrowserConfiguration.class.getName());

  /** IExplorer driver. */
  @Getter @Setter protected InternetExplorerDriver internetExplorerInstance;

  /** Options to be used by the InternetExplorerDriver. */
  @Getter @Setter protected InternetExplorerOptions options;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /** Initialize the Browser Configuration. */
  public void init() {
    logger.info(IEXPLORER_DRIVER_STARTING_UP_TEXT);
    options = new InternetExplorerOptions();
    options.setCapability(CapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANY);
    options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
    options.setCapability(
        InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
    try {
      if (globalProperties.getWebData().getBrowserCapabilities() != null) {
        JsonNode browserOptions = processBrowserOptions(globalProperties.getWebData().getBrowserCapabilities());
        browserOptions
            .fields()
            .forEachRemaining(s -> options.setCapability(s.getKey(), s.getValue().textValue()));
      }
    } catch (JsonProcessingException e) {
      // Do nothing. Capabilites cannot be read from json object. Raise a warning
      logger.warning(GlobalContent.JSON_CAPABILITIES_ERROR_TEXT);
    }
  }

  /**
   * This method provides the selenium driver based on configuration.
   *
   * @return WebDriver
   */
  public WebDriver getDriver() {
    if (internetExplorerInstance == null) {
      init();
      // Instantiate IExplorerDriver
      internetExplorerInstance = new InternetExplorerDriver(options);
      internetExplorerInstance
          .navigate()
          .to("javascript:document.getElementById('overridelink').click()");
    }
    return internetExplorerInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return InternetExplorerOptions Current iexplorer capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    return options;
  }
}
