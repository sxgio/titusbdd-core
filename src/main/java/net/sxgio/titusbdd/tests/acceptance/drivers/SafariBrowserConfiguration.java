package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.util.logging.Logger;

/**
 * <strong>SafariBrowserConfiguration.java</strong><br>
 * <strong>SafariBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class SafariBrowserConfiguration implements BrowserInterface {
  /** Info text shown while initializing the safari driver. */
  public static final String SAFARI_DRIVER_STARTING_UP_TEXT = "Starting up a Safari driver";

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(SafariBrowserConfiguration.class.getName());

  /** Chrome driver. */
  @Getter @Setter protected SafariDriver safariDriverInstance;

  /** Chrome capabilities. */
  @Getter @Setter protected SafariOptions options;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /** Initialize the Browser Configuration. */
  public void init() {
    logger.info(SAFARI_DRIVER_STARTING_UP_TEXT);
    options = new SafariOptions();
    options.setCapability(CapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANY);
    options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
    try {
      if (globalProperties.getWebData().getBrowserCapabilities() != null) {
        JsonNode browserOptions = processBrowserOptions(globalProperties.getWebData().getBrowserCapabilities());
        browserOptions
            .fields()
            .forEachRemaining(s -> options.setCapability(s.getKey(), s.getValue().textValue()));
      }
    } catch (JsonProcessingException e) {
      // Do nothing. Capabilites cannot be read from json object. Raise a warning
      logger.warning(GlobalContent.JSON_CAPABILITIES_ERROR_TEXT);
    }
  }

  /**
   * This method provides the selenium driver based on configuration.
   *
   * @return WebDriver
   */
  @Override
  public WebDriver getDriver() {
    if (safariDriverInstance == null) {
      init();
      safariDriverInstance = new SafariDriver(options);
    }
    return safariDriverInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return SafariOptions Current safari capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    return options;
  }
}
