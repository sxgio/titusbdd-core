package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import io.cucumber.java.Scenario;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.PageObjectInterface;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * <strong>Accessible.java</strong><br>
 * <strong>Accessible class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface Accessible {
  /**
   * Process executed after any step. It should be added to an afterStep method.
   *
   * @param scenario Current Scenario
   * @param pageObject Object to get driver from
   * @throws IOException Necessary to take screenshots
   */
  default void takeScreenshot(Scenario scenario, PageObjectInterface pageObject)
      throws IOException {
    TakesScreenshot camera = (TakesScreenshot) pageObject.getDriver();
    byte[] screenshot = camera.getScreenshotAs(OutputType.BYTES);
    ByteArrayInputStream bais = new ByteArrayInputStream(screenshot);
    BufferedImage bufferedImage;
    // Resize the screenshot at the same size of the browser
    bufferedImage =
        resizeImage(
            ImageIO.read(bais),
            pageObject.getDriver().manage().window().getSize().width,
            pageObject.getDriver().manage().window().getSize().height);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ImageIO.write(bufferedImage, GlobalContent.IMAGE_FORMAT_NAME_BY_DEFAULT_TEXT, baos);
    scenario.attach(baos.toByteArray(), GlobalContent.IMAGE_MIME_BY_DEFAULT_TEXT, "Image taken");
  }

  /**
   * Process to diminish image in size.
   *
   * @param img image to process
   * @param newWidth Object new width
   * @param newHeight Object new height
   *                  
   * @return BufferedImage Image with changed size
   */
  default BufferedImage resizeImage(BufferedImage img, int newWidth, int newHeight) {
    Image image = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
    BufferedImage retval = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
    retval.getGraphics().drawImage(image, 0, 0, null);
    return retval;
  }
}
