package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <strong>RemoteBrowserConfiguration.java</strong><br>
 * <strong>RemoteBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class RemoteBrowserConfiguration implements BrowserInterface {
  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(RemoteBrowserConfiguration.class.getName());

  /** Info text shown while initializing the chrome driver. */
  public static final String REMOTE_BROWSER_START_UP_TEXT =
      "Starting up a Remote driver. Grid initialization detected";

  /** Browser driver whatever would be. */
  @Getter @Setter protected BrowserInterface browserConfiguration;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return Capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    init();
    // Getting Driver based on file configuration
    return browserConfiguration.getCapabilities();
  }

  /**
   * This method provides the selenium driver based on the configuration.
   *
   * @return WebDriver
   */
  @Override
  public WebDriver getDriver() {
    try {
      // Remote web driver
      return new RemoteWebDriver(new URL(globalProperties.getWebData().getDriverUrl()), getCapabilities());
    } catch (MalformedURLException e) {
      return null;
    }
  }

  /**
   * This method provides AppiumDriver just if exists.
   *
   * @return AppiumDriver
   */
  @Override
  public AppiumDriver getAppiumDriver() throws DriverException {
    if (browserConfiguration == null) {
      init();
    }
    return browserConfiguration.getAppiumDriver();
  }

  /** Initialize the Browser Configuration. */
  @Override
  public void init() {
    try {
      logger.info(this.getClass().getSimpleName() + ":" + REMOTE_BROWSER_START_UP_TEXT);
      // Getting configuration name
      String typeName = globalProperties.getWebData().getDriverRemote();
      // Getting class Name
      Class<?> type = Class.forName(typeName);
      // Reflection of a class based on property name
      browserConfiguration = (BrowserInterface) type.getConstructor().newInstance();
      // QaProperties injection
      browserConfiguration.setGlobalProperties(globalProperties);
      // Initialize the browser.
      browserConfiguration.init();
    } catch (Exception e) {
      // Warning the error. Trying to set Firefox by default.
      logger.log(Level.WARNING, "Webdriver could not be created. Trying Firefox instead", e);
      // Make Firefox the browser by default
      browserConfiguration = new FirefoxBrowserConfiguration();
      // QaProperties injection
      browserConfiguration.setGlobalProperties(globalProperties);
      // Initialize the browser.
      browserConfiguration.init();
    }
  }
}
