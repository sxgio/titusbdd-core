package net.sxgio.titusbdd.tests.acceptance.entities;

import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

/**
 * <strong>GlobalConfigurationBean.java</strong><br>
 * <strong>GlobalConfigurationBean class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Data
public class GlobalConfigurationEntity implements BaseConfigurationEntityInterface {
  /** rest-assured configuration. * */
  RestDataEntity restData;

  /** jsoup, web and mobile configuration. * */
  WebDataEntity webData;
}
