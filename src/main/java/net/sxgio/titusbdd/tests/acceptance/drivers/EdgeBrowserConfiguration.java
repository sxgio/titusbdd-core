package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.util.logging.Logger;

/**
 * <strong>EdgeBrowserConfiguration.java</strong><br>
 * <strong>EdgeBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class EdgeBrowserConfiguration implements BrowserInterface {
  /** Info text shown while initializing the chrome driver. */
  public static final String EDGE_DRIVER_STARTING_UP_TEXT = "Starting up a Edge driver";

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(EdgeBrowserConfiguration.class.getName());

  /** Edge driver. */
  @Getter @Setter protected EdgeDriver edgeInstance;

  /** Options to be used by the InternetExplorerDriver. */
  @Getter @Setter protected EdgeOptions options;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /** Initialize the Browser Configuration. */
  public void init() {
    logger.info(EDGE_DRIVER_STARTING_UP_TEXT);
    options = new EdgeOptions();
    options.setCapability(CapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANY);
    options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
    try {
      if (globalProperties.getWebData().getBrowserCapabilities() != null) {
        JsonNode browserOptions = processBrowserOptions(globalProperties.getWebData().getBrowserCapabilities());
        browserOptions
            .fields()
            .forEachRemaining(s -> options.setCapability(s.getKey(), s.getValue().textValue()));
      }
    } catch (JsonProcessingException e) {
      // Do nothing. Capabilites cannot be read from json object. Raise a warning
      logger.warning(GlobalContent.JSON_CAPABILITIES_ERROR_TEXT);
    }
    if (globalProperties.getWebData().isHeadless()) {
      options.addArguments("--headless");
    }
  }

  /**
   * This method provides the selenium driver based on configuration.
   *
   * @return WebDriver
   */
  public WebDriver getDriver() {
    if (edgeInstance == null) {
      init();
      // Instantiate IExplorerDriver
      edgeInstance = new EdgeDriver(options);
    }
    return edgeInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return InternetExplorerOptions Current iexplorer capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    return options;
  }
}
