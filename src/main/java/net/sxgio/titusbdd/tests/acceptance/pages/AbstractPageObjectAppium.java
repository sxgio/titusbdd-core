package net.sxgio.titusbdd.tests.acceptance.pages;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.Getter;
import net.sxgio.titusbdd.tests.acceptance.drivers.BrowserDriver;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Fillable;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Waitable;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Clickable;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Findable;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.PageObjectInterface;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static org.openqa.selenium.remote.DriverCommand.GET_CONTEXT_HANDLES;
import static org.openqa.selenium.remote.DriverCommand.GET_CURRENT_CONTEXT_HANDLE;


/**
 * <strong>BasePageObject.java</strong><br>
 * <strong>BasePageObject class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public abstract class AbstractPageObjectAppium implements PageObjectInterface, Waitable, Findable, Clickable, Fillable {
    /**
     * Common logger to know what is happening along the test
     */
    protected final Logger logger = Logger.getLogger(AbstractPageObjectAppium.class.getName());

    /**
     * Appium Driver container
     */
    @Getter
    protected static ThreadLocal<WebDriver> mobileDriver = new ThreadLocal<>();

    /**
     * Constructor by default.
     *
     * @throws DriverException WebDriver not suitable to proceed with the tests
     */
    protected AbstractPageObjectAppium() throws DriverException {
        BrowserDriver browserDriver = RunMainTest.getFeeder().getFactory().getInstance(BrowserDriver.class);
        logger.info(browserDriver.getCurrentDriver().getClass().getName());
        setDriver(browserDriver.getCurrentDriver());
    }

    /**
     * Clear an element
     *
     * @param itemIdentifier Identifier of the element
     */
    protected void clearTheElement(String itemIdentifier) { findTheElement(itemIdentifier, getDriver()).clear();}

    /**
     * Get Driver
     *
     * @return WebDriver
     */
    @Override
    public WebDriver getDriver() {
        return mobileDriver.get();
    }

    /**
     * Get appium current context
     *
     * @return String
     */
    protected String getCurrentContextHandle() {
        RemoteExecuteMethod executeMethod = new RemoteExecuteMethod((RemoteWebDriver)getDriver());
        return (String) executeMethod.execute(GET_CURRENT_CONTEXT_HANDLE, null);
    }

    /**
     * Get appium current contexts
     *
     * @return List Current contexts
     */
    protected List<String> getContextHandles() {
        RemoteExecuteMethod executeMethod = new RemoteExecuteMethod((RemoteWebDriver)getDriver());
        return (List<String>) executeMethod.execute(GET_CONTEXT_HANDLES, null);
    }

    /**
     * Go to a specific frame by id
     *
     * @param frameIdentifier frame name or id
     */
    protected void goToFrame(String frameIdentifier)
    {
        // Get the default content
        getDriver().switchTo().defaultContent();
        // set the FRAME with the Menu
        getDriver().switchTo().frame(frameIdentifier);
    }

    /**
     * Go to a specific frame by index
     *
     * @param frameIndex frame index
     */
    protected void goToFrame(int frameIndex)
    {
        // Wait until iframes will be available in the current context. If not, error raised.
        waitUntilElementIsVisible(GlobalContent.XPATH_IFRAME_IDENTIFIER, getDriver());
        // Switch to the frame of choice.
        getDriver().switchTo().frame(frameIndex);
    }

    /**
     * InitElements for this object
     *
     */
    protected void initElements() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()), this);
    }

    /**
     * Checks if the element is displayed
     *
     * @param itemIdentifier Item Identifier
     *
     * @return boolean Whether element is displayed or not
     */
    protected boolean isElementDisplayed(String itemIdentifier) {
        return isElementDisplayed(findTheElement(itemIdentifier, getDriver()));
    }

    /**
     * Checks if the element is displayed
     *
     * @param item Item Identifier
     *
     * @return boolean Whether element is displayed or not
     */
    protected boolean isElementDisplayed(WebElement item) {
        return item.isDisplayed();
    }

    /**
     * Checks if the element is selected
     *
     * @param itemIdentifier Item identifier
     *
     * @return boolean Whether element is selected or not
     */
    protected boolean isElementSelected(String itemIdentifier) {
        return findTheElement(itemIdentifier, getDriver()).isSelected();
    }

    /**
     * This method loads a web page
     *
     * @param url Url to visit
     *
     */
    public void loadPage(String url) {
        getDriver().get(url);
    }

    /**
     * Set Driver
     *
     * @param driver Webdriver
     *
     * @return WebDriver
     */
    public WebDriver setDriver(WebDriver driver) {
        mobileDriver.set(driver);
        return getDriver();
    }

    /**
     * Switch to other appium context
     *
     * @param context Context to move to
     *
     */
    protected void switchToContext(String context) {
        RemoteExecuteMethod executeMethod = new RemoteExecuteMethod((RemoteWebDriver)getDriver());
        Map<String,String> params = new HashMap<>();
        params.put("name", context);
        executeMethod.execute(DriverCommand.SWITCH_TO_CONTEXT, params);
    }

}