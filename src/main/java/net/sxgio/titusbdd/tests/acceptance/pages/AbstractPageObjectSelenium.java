package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.drivers.BrowserDriver;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Accessible;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Clickable;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Fillable;
import net.sxgio.titusbdd.tests.acceptance.helpers.commons.Waitable;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.PageObjectInterface;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * <strong>AbstractPageObjectSelenium.java</strong><br>
 * <strong>AbstractPageObjectSelenium class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public abstract class AbstractPageObjectSelenium
    implements PageObjectInterface, Waitable, Clickable, Fillable, Accessible {

  /** Windows changed text for a comment in a logger. */
  protected static final String WINDOWS_CHANGED_TEXT = "Windows changed to %s";

  /** Common logger to know what is happening along the test. */
  protected final Logger logger = Logger.getLogger(this.getClass().getName());

  /** Selenium Driver container. */
  protected ThreadLocal<WebDriver> driver = new ThreadLocal<>();

  /**
   * Constructor by default.
   *
   * @throws DriverException Just in case a selenium webdriver error happened
   */
  protected AbstractPageObjectSelenium(BrowserDriver browserDriver) throws DriverException {
    browserDriver.setGlobalProperties(RunMainTest.getGlobalProperties());
    setDriver(browserDriver.getCurrentDriver());
  }

  /**
   * Get Driver.
   *
   * @return WebDriver
   */
  @Override
  public WebDriver getDriver() {
    return driver.get();
  }

  /**
   * Go to a specific frame by id.
   *
   * @param frameIdentifier frame name or id
   */
  protected void goToFrame(String frameIdentifier) {
    // Get the default content
    getDriver().switchTo().defaultContent();
    // set the FRAME with the Menu
    getDriver().switchTo().frame(frameIdentifier);
  }

  /**
   * Go to a specific frame by index.
   *
   * @param frameIndex frame index
   */
  protected void goToFrame(int frameIndex) {
    // Get the default content
    getDriver().switchTo().defaultContent();
    // Wait until iframes will be available in the current context. If not, error raised.
    waitUntilElementIsVisible(GlobalContent.XPATH_IFRAME_IDENTIFIER, getDriver());
    // Switch to the frame of choice.
    getDriver().switchTo().frame(frameIndex);
  }

  /**
   * Go to a specific window defined by its identifier.
   *
   * @param windowIdentifier window name or id
   */
  protected void goToWindow(String windowIdentifier) {
    // Log window indentifier
    logger.info(() -> GlobalContent.parseContent(WINDOWS_CHANGED_TEXT, windowIdentifier));
    // set the WINDOW with the Menu
    getDriver().switchTo().window(windowIdentifier);
  }

  /**
   * Go to a specific window defined by its identifier and close the current one.
   *
   * @param windowIdentifier window name or id
   */
  protected void goToWindowAndCloseCurrent(String windowIdentifier) {
    if (!getDriver().getWindowHandle().equalsIgnoreCase(windowIdentifier)) {
      getDriver().close();
    }
    goToWindow(windowIdentifier);
  }

  /** InitElements for this object. */
  public synchronized void initElements() {
    PageFactory.initElements(getDriver(), this);
  }

  /**
   * This method loads a web page.
   *
   * @param url Url to visit
   */
  public void loadPage(String url) {
    getDriver().get(url);
  }

  /**
   * Set Driver.
   *
   * @param driver WebDriver
   * @return WebDriver
   */
  private WebDriver setDriver(WebDriver driver) {
    this.driver.set(driver);
    return getDriver();
  }
}
