package net.sxgio.titusbdd.tests.acceptance.entities;

import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

/**
 * <strong>WebDataEntity.java</strong><br>
 * <strong>WebDataEntity class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release date: 3/2/24
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since File available since 3/2/24
 */
@Data
public class WebDataEntity implements BaseConfigurationEntityInterface {
    /** Android device to make the tests. */
    private String androidDevice;

    /** Android version installed on the android device. */
    private String androidVersion;

    /** Android or IOS application file. */
    private String applicationFile;

    /** If a Keystore is needed just in case a two-way ssl validation is mandatory. */
    private String baseKeyStore;

    /** If the Keystore needs a password. */
    private String baseKeyStorePassword;

    /** Main URL to test. */
    private String baseUrl;

    /** Browser capabilities and/or options to customize Configuration files. */
    private String browserCapabilities;

    /** Webdriver class. */
    private String driverClass;

    /** Webdriver file for local executions. */
    private String driverFile;

    /** Webdriver name as needed by Selenium. */
    private String driverName;

    /** Webdriver type as needed by Selenium. */
    private String driverType;

    /** Selenium url of the grid to send it the tests. */
    private String driverUrl;

    /**
     * If selected driver name was Selenium remote browser, driverRemote means what webdriver remote
     * browser would use to execute the test.
     */
    private String driverRemote;

    /** Browser headless if it should be activated. */
    private boolean headless;

    /** IOS Device to make the tests. */
    private String iosDevice;

    /** Version of ios installed on the Apple device. */
    private String iosVersion;

    /**
     * Activated if needed an Appium Test. It means to provide appiumDriver instead of webDriver and
     * use customed-specific mobile actions.
     */
    private boolean isAppiumTest;

    /** File path to locate page elements with internationalization. */
    private String languagePatternFile;

    /** Profile name. */
    private String name;

    /** OrientDb Configuration. */
    private OrientDbConfigurationEntity orientDbConfiguration;

    /** Nitrite Configuration. */
    private NitriteConfigurationEntity nitriteConfiguration;

    /** TestNG file to be loaded by default. */
    private String suiteXmlFile;
}
