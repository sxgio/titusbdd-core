package net.sxgio.titusbdd.tests.acceptance.pages.contracts;

/**
 * <strong>NotFoundPageObjectInterface.java</strong><br>
 * <strong>NotFoundPageObjectInterface interface</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface NotFoundPageObjectInterface extends PageObjectInterface
{
    /**
     * Load Unknown page. The error that comes from this page should
     * be under control.
     * 
     * @return boolean     
     */
    boolean loadUnknownPage();
}