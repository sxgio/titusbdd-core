package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.InvalidServerInstanceException;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <strong>AbstractAppiumConfiguration.java</strong><br>
 * <strong>AbstractAppiumConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public abstract class AbstractAppiumConfiguration implements BrowserInterface {
  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(AbstractAppiumConfiguration.class.getName());

  /** Misconfigured appium server text. */
  public static final String MISCONFIGURED_APPIUM_SERVER_TEXT =
      "Misconfigured Appium Server in GRID";

  /** Malformed appium url text. */
  public static final String MALFORMED_APPIUM_URL_TEXT = "Malformed Appium Server URL: %s";

  /** Appium driver. */
  @Getter @Setter protected AppiumDriver appiumDriverInstance;

  /** Appium capabilities. */
  @Getter @Setter protected DesiredCapabilities options;

  /** Spring properties bean. */
  @Setter @Getter protected GlobalConfigurationEntity globalProperties;

  /** Constructor by default. */
  protected AbstractAppiumConfiguration() {
    options = new DesiredCapabilities();
  }

  /**
   * This method provides the selenium driver based on the configuration.
   *
   * @return WebDriver
   */
  @Override
  public WebDriver getDriver() throws DriverException {
    return getAppiumDriver();
  }

  /**
   * This method provides AppiumDriver just if exists.
   *
   * @return AppiumDriver
   */
  @Override
  public AppiumDriver getAppiumDriver() throws DriverException {
    if (appiumDriverInstance == null) {
      init();
      if (!globalProperties
          .getWebData()
          .getDriverRemote()
          .contains("net.sxgio.titusbdd.tests.acceptance.drivers")) {
        try {
          appiumDriverInstance = new AppiumDriver(options);
        } catch (InvalidServerInstanceException | SessionNotCreatedException e) {
          logger.log(Level.SEVERE, MISCONFIGURED_APPIUM_SERVER_TEXT, e);
          throw new DriverException(MISCONFIGURED_APPIUM_SERVER_TEXT);
        }
      } else {
        try {
          appiumDriverInstance = new AppiumDriver(new URL(globalProperties.getWebData().getDriverUrl()), options);
        } catch (MalformedURLException e) {
          final String errorInfo =
              String.format(MALFORMED_APPIUM_URL_TEXT, globalProperties.getWebData().getDriverUrl());
          logger.log(Level.SEVERE, errorInfo, e);
          throw new DriverException(errorInfo);
        }
      }
    }
    return appiumDriverInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return Capabilities
   */
  public Capabilities getCapabilities() {
    return options;
  }
}
