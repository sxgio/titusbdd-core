/**
 * Provide the necessary functionality to interact with appium.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.helpers.appium;
