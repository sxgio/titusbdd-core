package net.sxgio.titusbdd.tests.acceptance.pages;

import net.sxgio.titusbdd.tests.acceptance.features.RunMainTest;
import net.sxgio.titusbdd.tests.acceptance.pages.contracts.HomePageObjectInterface;
import org.apache.commons.lang.NotImplementedException;
import org.apache.http.HttpStatus;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.openqa.selenium.WebDriver;

import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.parseContent;

/**
 * <strong>HomePageObjectJSOUP.java</strong><br>
 * <strong>HomePageObjectJSOUP class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class HomePageObjectJSOUP extends AbstractPageObjectJSOUP
    implements HomePageObjectInterface {
  /** Text for general exception. */
  public static final String GENERAL_EXCEPTION_TEXT = "Unknown error. Error retrieved %s";

  /** Text for SPA not found. */
  public static final String MAIN_PAGE_NOT_FOUND_TEXT = "Error accessing the main entry point";

  /** Text for a status exception. */
  public static final String STATUS_EXCEPTION_TEXT = "Response code different from OK. Error retrieved %s";

  /** CSS Expression for a text to find in a page. */
  public static final String TEXT_TO_FIND_IN_PAGE_EXPR = ":contains(%s)";

  /**
   * Get Driver just in case of needed.
   *
   * @return WebDriver Never reached
   */
  @Override
  public WebDriver getDriver() {
    // Uncomment the following line if you need to stop at this test
    throw new NotImplementedException();
  }

  /**
   * Loading Home Page.
   *
   * @param homePage home page URL
   * @return boolean
   */
  @Override
  public boolean loadHomePage(String homePage) {
    boolean retval = true;
    // Check if SSL is needed.
    checkSSLCertification();
    // Process request
    try {
      RunMainTest.getSharedContent().setStatusCode(Jsoup.connect(homePage).execute().statusCode());
      webDocument = Jsoup.connect(homePage).get();
    } catch (HttpStatusException statusException) {
      logger.severe(MAIN_PAGE_NOT_FOUND_TEXT);
      logger.severe(parseContent(STATUS_EXCEPTION_TEXT, statusException.getMessage()));
      // Error found in the request.
      RunMainTest.getSharedContent().setStatusCode(statusException.getStatusCode());
      retval = false;
    } catch (Exception e) {
      logger.severe(MAIN_PAGE_NOT_FOUND_TEXT);
      logger.severe(parseContent(GENERAL_EXCEPTION_TEXT, e.getMessage()));
      // Error found in the request.
      RunMainTest.getSharedContent().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      retval = false;
    }
    return retval;
  }

  /**
   * Search for this text into the body or into the head, either.
   *
   * @param textToFind Text to find in the body or the head either
   * @return boolean
   */
  @Override
  public boolean searchForThisText(String textToFind) {
    return !(webDocument.body()
            .select(parseContent(TEXT_TO_FIND_IN_PAGE_EXPR, textToFind))
            .isEmpty()
        && webDocument.head()
            .select(parseContent(TEXT_TO_FIND_IN_PAGE_EXPR, textToFind))
            .isEmpty());
  }
}
