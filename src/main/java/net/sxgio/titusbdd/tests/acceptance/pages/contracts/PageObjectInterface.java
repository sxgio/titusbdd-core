package net.sxgio.titusbdd.tests.acceptance.pages.contracts;

import org.openqa.selenium.WebDriver;

/**
 * <strong>PageObjectInterface.java</strong><br>
 * <strong>PageObjectInterface interface</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface PageObjectInterface
{
    /**
     * Get Driver
     *
     * @return WebDriver
     */
    WebDriver getDriver();
}
