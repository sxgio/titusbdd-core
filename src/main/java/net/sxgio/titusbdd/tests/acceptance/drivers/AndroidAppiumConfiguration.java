package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.remote.AutomationName;

import java.util.Map;
import java.util.logging.Logger;

/**
 * <strong>AndroidAppiumConfiguration.java</strong><br>
 * <strong>AndroidAppiumConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class AndroidAppiumConfiguration extends AbstractAppiumConfiguration {

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(AndroidAppiumConfiguration.class.getName());

  /** Info text shown while initializing the chrome driver. */
  public static final String ANDROID_DRIVER_STARTING_UP_TEXT =
      "Starting up a Android Appium driver";

  /** Constructor by default. */
  public AndroidAppiumConfiguration() {
    super();
  }

  /** Initialize the Android Appium Configuration. */
  @Override
  public void init() {
    logger.info(ANDROID_DRIVER_STARTING_UP_TEXT);
    options.setCapability("deviceName", globalProperties.getWebData().getAndroidDevice());
    options.setCapability("platformName", "Android");
    options.setCapability("udid", globalProperties.getWebData().getAndroidDevice());
    options.setCapability("platformVersion", globalProperties.getWebData().getAndroidVersion());
    options.setCapability("nativeWebScreenshot", true);
    // If no application file provided means browser is going to be used
    if (globalProperties.getWebData().getApplicationFile().equals("false")) {
      options.setCapability("browserName", "Chrome");
    } else {
      options.setCapability("app", globalProperties.getWebData().getApplicationFile());
    }

    options.setCapability("automationName", AutomationName.ANDROID_UIAUTOMATOR2);
    options.setCapability("forceMjsonwp", true);
    options.setCapability("appium:chromeOptions", Map.of("w3c", false));
  }
}
