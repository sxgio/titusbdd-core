package net.sxgio.titusbdd.tests.acceptance.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.dizitart.no2.Document;
import org.dizitart.no2.NitriteCollection;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Logger;

import static net.sxgio.titusbdd.tests.acceptance.features.GlobalContent.parseContent;
import static org.dizitart.no2.Document.createDocument;


/**
 * <strong>NitriteDbServerStarter.java</strong><br>
 * <strong>NitriteDbServerStarter class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class NitriteDbServerStarter {

    /**
     * Common logger to know what's happening for all the objects belong to this class
     */
    public static final Logger logger = Logger.getLogger(NitriteDbServerStarter.class.getName());

    /**
     * Embedded NitriteDb Server. To start or stop database server from code
     */
    @Getter
    private static EmbeddedNitriteDbServer embeddedNitriteDbServer;

    /**
     * Just in case an error running the server. Text error
     */
    public static final String NITRITEDB_ERROR = "Starting up the NitriteDb server raised the following error %s";

    /**
     * NitriteDb trigger method
     *
     * @param args External args if needed
     */
    public static void main(final String[] args) {
        try {
            final String configFile = (System.getProperty("nitriteDb.data.file")!=null)?System.getProperty("nitriteDb.data.file"): "testNitrite.db";
            embeddedNitriteDbServer = new EmbeddedNitriteDbServer(configFile, args[0], args[1]);
        } catch (final Exception e) {
            logger.severe(String.format(NITRITEDB_ERROR, e.getMessage()));
        }
    }

    /**
     * Method to upsert data.
     *
     * @param files Data to push into nitritedb
     * @param collectionName Name of the default collection
     *
     * @return NitriteCollection Collection to verify insertion
     */
    public static NitriteCollection actualizeNitrite(Map<String, Map<String, String>> files, String collectionName) {
        NitriteCollection collection = embeddedNitriteDbServer.getServer().getCollection(collectionName);
        ObjectMapper mapper = new ObjectMapper();
        files.keySet().forEach(x -> {
            Map<String, ?> map = null;
            try {
                map = mapper.readValue(Paths.get(x).toFile(), Map.class);
            } catch (IOException e) {
                logger.severe(() -> parseContent(GlobalContent.EXCEPTION_CAUGHT_AND_NOT_EXPECTED_TEXT, e.getMessage()));
            }
            Document doc = createDocument(files.get(x).get("name"), files.get(x).get("value"));
            doc.putAll(map);
            collection.insert(doc);
        });
        return collection;
    }
}
