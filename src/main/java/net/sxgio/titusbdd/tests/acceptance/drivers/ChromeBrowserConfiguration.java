package net.sxgio.titusbdd.tests.acceptance.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.features.GlobalContent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.util.Collections;
import java.util.logging.Logger;

/**
 * <strong>ChromeBrowserConfiguration.java</strong><br>
 * <strong>ChromeBrowserConfiguration class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class ChromeBrowserConfiguration implements BrowserInterface {
  /** Info text shown while initializing the chrome driver. */
  public static final String CHROME_DRIVER_STARTING_UP_TEXT = "Starting up a Chrome driver";

  /** Common logger to know what is happening. */
  public static final Logger logger = Logger.getLogger(ChromeBrowserConfiguration.class.getName());

  /** Chrome driver. */
  @Getter @Setter protected ChromeDriver chromeDriverInstance;

  /** Chrome capabilities. */
  @Getter @Setter protected ChromeOptions options;

  /** QaProperties. */
  @Getter @Setter protected GlobalConfigurationEntity globalProperties;

  /** Initialize the Browser Configuration. */
  public void init() {
    logger.info(CHROME_DRIVER_STARTING_UP_TEXT);
    options = new ChromeOptions();
    options.setCapability(CapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANY);
    options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
    options.setCapability(
        "chrome.switches", Collections.singletonList("--ignore-certificate-errors"));
    options.setAcceptInsecureCerts(true);
    try {
      if (globalProperties.getWebData().getBrowserCapabilities() != null) {
        JsonNode browserOptions = processBrowserOptions(globalProperties.getWebData().getBrowserCapabilities());
        browserOptions
            .fields()
            .forEachRemaining(s -> options.setCapability(s.getKey(), s.getValue().textValue()));
      }
    } catch (JsonProcessingException e) {
      // Do nothing. Capabilites cannot be read from json object. Raise a warning
      logger.warning(GlobalContent.JSON_CAPABILITIES_ERROR_TEXT);
    }
    if (globalProperties.getWebData().isHeadless()) {
      options.addArguments("--headless");
    }
  }

  /**
   * This method provides the selenium driver based on configuration.
   *
   * @return WebDriver
   */
  @Override
  public WebDriver getDriver() {
    if (chromeDriverInstance == null) {
      init();
      chromeDriverInstance = new ChromeDriver(options);
    }
    return chromeDriverInstance;
  }

  /**
   * This method provides the selenium capabilities to be used by the remoteDriver.
   *
   * @return ChromeOptions Current chrome capabilities
   */
  @Override
  public Capabilities getCapabilities() {
    return options;
  }
}
