/**
 * Provide the necessary objects to handle annotations at runtime.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.i18n;
