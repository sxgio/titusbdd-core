package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.appium.java_client.AppiumDriver;
import lombok.Getter;
import lombok.Setter;
import net.sxgio.titusbdd.tests.acceptance.entities.GlobalConfigurationEntity;
import net.sxgio.titusbdd.tests.acceptance.exceptions.DriverException;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/**
 * <strong>BrowserDriver.java</strong><br>
 * <strong>BrowserDriver class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class BrowserDriver {
  /** Browser is going to be closed. Text error */
  public static final String CLOSING_BROWSER_TEXT = "Closing the browser";

  /** Browser is going to be initialized. Text error */
  public static final String INIT_BROWSER_TEXT = "Initializing the browser";

  /** Common logger to know what is happening for all objects belong to this class. */
  public static final Logger logger = Logger.getLogger(BrowserDriver.class.getName());

  /** Uncontrolled error happened with the browser. Text error */
  public static final String UNKNOWN_BROWSER_ERROR_TEXT = "Unknown error found in the code %s";

  /** Driver class was not accessible. Text error */
  public static final String CONSTRUCTOR_NOT_AVAILABLE_TEXT =
      "No driver class accessible. Error is %s";

  /** Instance method was not accessible. Text error */
  public static final String ERROR_BUILDING_DRIVER =
      "Instance method is not available. Error is %s";

  /** This class simulates a thread to close the browser. */
  private class BrowserCleanup implements Runnable {
    public void run() {
      logger.info(CLOSING_BROWSER_TEXT);
      close();
    }
  }

  /** Spring properties bean. */
  @Getter @Setter private GlobalConfigurationEntity globalProperties;

  /** Selenium driver to make the test. */
  @Getter @Setter private WebDriver seleniumDriver;

  /** Selenium driver to make the test. */
  @Getter @Setter private AppiumDriver appiumDriver;

  /** This method destroys the selenium driver. */
  public void close() {
    try {
      logger.info(CLOSING_BROWSER_TEXT);
      // Try to shut down selenium gently
      getSeleniumDriver().quit();
    } catch (Exception e) {
      logger.severe(String.format(UNKNOWN_BROWSER_ERROR_TEXT, e.getMessage()));
    } finally {
      seleniumDriver = null;
    }
  }

  /**
   * This method provides the current driver used in the test.
   *
   * @return WebDriver
   * @throws DriverException WebDriver not suitable to proceed with the tests
   */
  public synchronized WebDriver getCurrentDriver() throws DriverException {
    if (seleniumDriver == null) {
      try {
        logger.info(INIT_BROWSER_TEXT);
        // Getting driver file
        File file = new File(globalProperties.getWebData().getDriverFile());
        // Setting real driver
        System.setProperty(globalProperties.getWebData().getDriverType(), file.getAbsolutePath());
        // Getting configuration name
        String typeName = globalProperties.getWebData().getDriverName();
        // Getting class Name
        Class<?> type = Class.forName(typeName);
        // Reflection of a class based on property name
        BrowserInterface browserConfiguration =
            (BrowserInterface) type.getConstructor().newInstance();
        // QaProperties injection
        browserConfiguration.setGlobalProperties(globalProperties);
        // Getting Driver based on file configuration
        seleniumDriver = browserConfiguration.getDriver();
        // Getting Appium Driver just in case of needed
        appiumDriver = browserConfiguration.getAppiumDriver();
      } catch (NoSuchMethodException e) {
        seleniumDriver = null;
        logger.severe(String.format(CONSTRUCTOR_NOT_AVAILABLE_TEXT, e.getMessage()));
      } catch (InvocationTargetException e) {
        seleniumDriver = null;
        logger.severe(String.format(ERROR_BUILDING_DRIVER, e.getMessage()));
      } catch (Exception e) {
        throw new DriverException(e.getMessage(), e);
      } finally {
        Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
      }
    }
    return seleniumDriver;
  }
}
