/**
 * Provide the necessary implementation for communicating with browsers and some mobile devices.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.drivers;
