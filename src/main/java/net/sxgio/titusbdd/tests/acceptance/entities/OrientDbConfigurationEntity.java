package net.sxgio.titusbdd.tests.acceptance.entities;

import lombok.Data;
import net.sxgio.titusbdd.tests.acceptance.entities.contracts.BaseConfigurationEntityInterface;

/**
 * <strong>OrientDbConfigurationBean.java</strong><br>
 * <strong>OrientDbConfigurationBean class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
@Data
public class OrientDbConfigurationEntity implements BaseConfigurationEntityInterface {
    /** Whether database is active or not. */
    private boolean active;

    /** orientDb dabatase. */
    private String orientdbDatabase;

    /** orientDb user password. */
    private String orientdbPassword;

    /** OrientDb server port. */
    private String orientdbPort;

    /** Orientdb User. */
    private String orientdbUser;
}
