/**
 * Provide the necessary objects to make a way to start testing. bootstrap or entry points.
 *
 * @since 1.0.0
 */
package net.sxgio.titusbdd.tests.acceptance.features;
