package net.sxgio.titusbdd.tests.acceptance.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

/**
 * <strong>SharedContent.java</strong><br>
 * <strong>SharedContent class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class SharedContent
{
    /**
     * Page status code
     */
    @Getter
    @Setter
    private int statusCode;

    /**
     * Random content to be pushed on the fly here
     */
    @Getter
    @Setter
    private HashMap<String, Object> dynamicContent;
}
