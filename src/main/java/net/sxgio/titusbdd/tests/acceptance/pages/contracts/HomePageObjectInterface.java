package net.sxgio.titusbdd.tests.acceptance.pages.contracts;

/**
 * <strong>HomePageObjectInterface.java</strong><br>
 * <strong>HomePageObjectInterface interface</strong><br>
 *
 * @author Sergio Martin, info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public interface HomePageObjectInterface extends PageObjectInterface {

  /**
   * Load home page.
   *
   * @param homePage Main home page URL.
   * @return boolean
   */
  boolean loadHomePage(String homePage);

  /**
   * Searching for this text in the page.
   *
   * @param textToFind text to find in the page
   * @return boolean
   */
  boolean searchForThisText(String textToFind);
}
