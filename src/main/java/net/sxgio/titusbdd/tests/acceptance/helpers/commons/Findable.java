package net.sxgio.titusbdd.tests.acceptance.helpers.commons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * <strong>Findable.java</strong><br>
 * <strong>Findable class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @version Release: 0.1.0
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 * @since Class available since Release 0.1.0
 */
public interface Findable {
    /**
     * Find one of my childs.
     *
     * @param webElement     WebElement as root for the searching context
     * @param itemIdentifier Identifier of the element
     * @return WebElement Element found.
     */
    default WebElement findMyChild(WebElement webElement, String itemIdentifier) {
        return webElement.findElement(By.xpath(itemIdentifier));
    }

    /**
     * Find my childs.
     *
     * @param webElement     WebElement as root for the searching context
     * @param itemIdentifier Identifier of the elements
     * @return List Elements found.
     */
    default List<WebElement> findMyChilds(WebElement webElement, String itemIdentifier) {
        return webElement.findElements(By.xpath(itemIdentifier));
    }

    /**
     * Find an element.
     *
     * @param itemIdentifier Identifier of the element
     * @param driver         Webdriver
     * @return WebElement Element found.
     */
    default WebElement findTheElement(String itemIdentifier, WebDriver driver) {
        return driver.findElement(By.xpath(itemIdentifier));
    }

    /**
     * Find some elements.
     *
     * @param itemIdentifier Identifier of the elements
     * @param driver         Webdriver
     * @return List Elements found.
     */
    default List<WebElement> findTheElements(String itemIdentifier, WebDriver driver) {
        return driver.findElements(By.xpath(itemIdentifier));
    }

    /**
     * Format and find an element. Varags method.
     *
     * @param driver          Driver
     * @param locatorToFormat Name of the MenuItem
     * @param items           Items to change based on the locator
     * @return WebElement Element found
     */
    default WebElement formatAndFindTheElement(
        WebDriver driver, String locatorToFormat, Object... items) {
        return driver.findElement(By.xpath(String.format(locatorToFormat, items)));
    }
}
