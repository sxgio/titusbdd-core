package net.sxgio.titusbdd.tests.acceptance.drivers;

import io.restassured.http.Headers;
import io.restassured.response.Response;
import net.sxgio.titusbdd.tests.acceptance.exceptions.RestException;

import java.util.Map;

/**
 * <strong>RestInterface.java</strong><br>
 * <strong>RestInterface interface</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
interface RestInterface {

  /**
   * Connect HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException If something wrong happened
   */
  void connect(String resource) throws RestException;

  /**
   * Delete HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  Response delete(String resource);

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  Response get(String resource);

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response get(String resource, Headers headers);

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @param params   Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response get(String resource, Headers headers, Map<String, ?> params);

  /**
   * Get HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param params   PArams to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response get(String resource, Map<String, ?> params);

  /**
   * Head HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException If something wrong happened
   */
  void head(String resource) throws RestException;

  /**
   * Options HTTP Verb in a REST interface. Use this method with the awaitility library to check
   * some time response.
   */
  void options();

  /**
   * Options HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   */
  void options(String resource);

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  Response patch(String resource);

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param payload  Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  Response patch(String resource, String payload);

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response patch(String resource, Headers headers);

  /**
   * Patch HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @param payload  Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  Response patch(String resource, Headers headers, String payload);

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  Response post(String resource);

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param params   Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response post(String resource, Map<String, ?> params);

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response post(String resource, Headers headers);

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @param params   Params to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response post(String resource, Headers headers, Map<String, ?> params);

  /**
   * Post HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @param payload  If parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  Response post(String resource, Headers headers, String payload);

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @return Response Rest-assured response object to be evaluated
   */
  Response put(String resource);

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param payload  Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  Response put(String resource, String payload);

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @return Response Rest-assured response object to be evaluated
   */
  Response put(String resource, Headers headers);

  /**
   * Put HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @param headers  Headers to be defined in a request
   * @param payload  Parameters are sent through the body
   * @return Response Rest-assured response object to be evaluated
   */
  Response put(String resource, Headers headers, String payload);

  /**
   * Trace HTTP Verb in a REST interface.
   *
   * @param resource Web resource to be linked to the baseURI
   * @throws RestException If something wrong happened
   */
  void trace(String resource) throws RestException;
}
