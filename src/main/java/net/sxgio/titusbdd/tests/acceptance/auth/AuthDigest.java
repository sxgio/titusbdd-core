package net.sxgio.titusbdd.tests.acceptance.auth;

import io.restassured.specification.RequestSpecification;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

/**
 * <strong>AuthDigest.java</strong><br>
 * <strong>AuthDigest class</strong><br>
 *
 * @author Sergio Martin info@sxgio.net
 * @see <a href="https://www.sxgio.net">SXGIO.NET</a>
 *
 * @version Release: 0.1.0
 * @since Class available since Release 0.1.0
 */
public class AuthDigest implements AuthInterface {

    /**
     * Method to get a RequestSpecification based on auth. Chained method
     *
     * @param params different parameters needed based on the auth type
     *
     * @return RequestSpecification
     */
    @Override
    public RequestSpecification auth(HashMap<String, Object> params) {
        return given().auth().digest((String)params.get("userName"), (String)params.get("password"));
    }
}